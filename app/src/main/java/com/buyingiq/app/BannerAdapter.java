package com.buyingiq.app;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.buyingiq.app.entities.Banner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by maxx on 17/11/14.
 */
public class BannerAdapter extends PagerAdapter {

    private ArrayList<Banner> mBannerList;
    private LayoutInflater mInflater;
    private DisplayImageOptions mOptions;

    BannerAdapter(Context context, ArrayList<Banner> bannerList, DisplayImageOptions options) {
        mBannerList = bannerList;
        mInflater = LayoutInflater.from(context);
        mOptions = options;
    }

    @Override
    public int getCount() {
        return mBannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view.equals(o);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View imageLayout = mInflater.inflate(R.layout.fragment_banner, container, false);
        assert imageLayout != null;
        ImageView imageView = (ImageView) imageLayout.findViewById(R.id.banner_image);
        ImageLoader.getInstance().displayImage(mBannerList.get(position).image, imageView, mOptions);
        container.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
