package com.buyingiq.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.buyingiq.app.resources.ImageLoader1;

public class BannerFragment extends Fragment {
    private static final String ARG_BANNER = "banner";
    private static final String ARG_POSITION = "position";
    private String mBanner;
    private int mPosition;
    private OnBannerSelectedListener mListener;

    public BannerFragment() {
    }

/*    public static BannerFragment newInstance(int bannerDrawable)
    {
        BannerFragment bf = new BannerFragment();
        Bundle b = new Bundle();
        if(bannerDrawable!=0)
            b.putInt(ARG_BANNER,bannerDrawable);
        bf.setArguments(b);
        return bf;
    }*/


    public static BannerFragment newInstance(String banner, int position) {
        BannerFragment bf = new BannerFragment();
        Bundle b = new Bundle();
        if (banner != null)
            b.putString(ARG_BANNER, banner);
        b.putInt(ARG_POSITION, position);
        bf.setArguments(b);
        return bf;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBanner = getArguments().getString(ARG_BANNER);
            mPosition = getArguments().getInt(ARG_POSITION);
            //    mDrawable = getResources().getDrawable(getArguments().getInt(ARG_BANNER));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_banner, container, false);
        if (mBanner != null)
            new ImageLoader1(getActivity(), R.drawable.iq_logo_grey).DisplayImage(mBanner, (ImageView) view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onBannerSelected(mPosition);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activity.invalidateOptionsMenu();
        try {
            mListener = (OnBannerSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBannerSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        getActivity().invalidateOptionsMenu();
        super.onDetach();
        mListener = null;
    }

    public interface OnBannerSelectedListener {
        public void onBannerSelected(int position);
    }
}
