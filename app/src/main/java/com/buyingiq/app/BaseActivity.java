package com.buyingiq.app;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.buyingiq.app.entities.Notif;
import com.buyingiq.app.resources.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public abstract class BaseActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    public static final String PREFS = "basePrefs";
    protected static final String ACTION_SPLASH = "com.buyingiq.action.SPLASH";
    public static int FLAG_HOME_ENABLED = 1;
    public static int FLAG_BACK_ENABLED = 1 << 1;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    protected NavigationDrawerFragment mNavigationDrawerFragment;
    protected int flags;
    DialogInterface.OnClickListener notificationClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    onUpdateRequested();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.cancel();
                    break;
                case DialogInterface.BUTTON_NEUTRAL:
                    dialog.dismiss();
                    break;
            }
        }
    };

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
//    protected CharSequence mTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ACTION_SPLASH.equals(getIntent().getAction())) {
            setFlags(BaseActivity.FLAG_BACK_ENABLED);
            checkUpdate(getIntent());
        }
        setContentView(R.layout.activity_base);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        installCache();
    }

    /**
     * Override this method according to your activity's requirements
     *
     * @param position position to be selected
     */
    @Override
    public abstract void onNavigationDrawerItemSelected(int position);

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(getTitle());
            actionBar.setLogo(R.drawable.ic_logo);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                onSearchRequested();
                return true;
            case android.R.id.home:
                if (!isTaskRoot()) {
                    finish();
                    overridePendingTransition(0, R.anim.slide_out_right);
                    return true;
                }
                return false;
            case R.id.action_feedback:
                onFeedbackRequested();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSearchRequested() {
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }

    public void onFeedbackRequested() {
        Intent i = new Intent(this, FeedbackActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        flushCache();
    }

    /**
     * Install the cache
     */

    public void installCache() {
        try {
            File httpCacheDir = new File(this.getCacheDir(), "http");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
            Log.i("MainActivity", "HTTP response cache installation failed:" + e);
        }
    }

    public void flushCache() {
        HttpResponseCache cache = HttpResponseCache.getInstalled();
        if (cache != null) {
            cache.flush();
        }
    }

    /*public MemoryCache getMemoryCache() {
        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public void checkUpdate(Intent intent) {
        if (intent.hasExtra(StringUtils.KEY_NOTIFICATION_LIST)) {
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .create();
            int currentDialog = -1;
            String message = "";
            String title = "";
            ArrayList<Notif> notifs = intent.getParcelableArrayListExtra(StringUtils.KEY_NOTIFICATION_LIST);
            for (Notif n : notifs) {
                switch (n.level) {
                    case Notif.UPDATE_CRITICAL:
                        currentDialog = Notif.UPDATE_CRITICAL;
                        title = "Update Required";
                        message = n.message;
//    getSharedPreferences(PREFS,MODE_PRIVATE).edit().putInt("LatestVersion",)
                        dialog.setMessage(n.message);
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Update", notificationClickListener);
                        dialog.setCancelable(false);
                        break;
                    case Notif.UPDATE:
                        if (currentDialog < Notif.UPDATE && currentDialog > -1)
                            break;
                        title = "Update Available";
                        message = n.message;
                        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Update", notificationClickListener);
                        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Later", notificationClickListener);
                        break;
                    default:
                        if (currentDialog < Notif.NOTIFICATION && currentDialog > -1)
                            break;
                        title = "Notification";
                        message = n.message;
                        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Ok", notificationClickListener);
                        break;
                }
            }
            dialog.setMessage(message);
            dialog.setTitle(title);
            dialog.show();
        }
    }

    private void onUpdateRequested() {
        final Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName() + "&referrer=utm_source%3Dandroid%26utm_medium%3Dupdate_notification");
        final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0) {
            startActivity(rateAppIntent);
            finish();
        }
    }
}
