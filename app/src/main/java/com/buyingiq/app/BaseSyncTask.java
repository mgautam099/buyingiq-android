package com.buyingiq.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Pair;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.buyingiq.app.resources.NetworkUtils;
import com.google.android.gms.analytics.Tracker;


import java.util.ArrayList;

/**
 *
 */
public abstract class BaseSyncTask<Progress> extends AsyncTask<Void, Progress, Integer> {

    public static int METHOD_POST = 1;
    public static int NETWORK_ERROR = -1;
    public static int OTHER_ERROR = -2;
    private final Tracker t;
    NetworkUtils networkUtils;
    String url;
    int flags;
    private ArrayList<Pair<String,String>> pairs;
    String json;
    Context mContext;
    ListView listView;
    private View progressBar;
    private String TAG = "BaseSyncTask";

    public BaseSyncTask(Activity context, String url, int flags) {
        this(context, url, flags, null);

    }

    public BaseSyncTask(Activity context, String url, int flags, ArrayList<Pair<String,String>> pairs) {
        super();
        this.url = url;
        this.flags = flags;
        this.pairs = pairs;
        mContext = context;
        t = ((BuyingIQApp) context.getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);

    }

    @Override
    protected void onPreExecute() {
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
        if (listView != null) {
            listView.getEmptyView().setVisibility(View.GONE);
        }
    }

    @Override
    protected Integer doInBackground(Void... params) {
        networkUtils = new NetworkUtils(t);
        if (!NetworkUtils.isNetworkConnected(mContext))
            return NETWORK_ERROR;
        if (pairs != null)
            json = networkUtils.makeServiceCall(url, ((flags & METHOD_POST) != 0) ? NetworkUtils.POST : NetworkUtils.GET, pairs);
        else
            json = networkUtils.makeServiceCall(url, NetworkUtils.GET);
        if (json == null)
            return OTHER_ERROR;
        if (json.startsWith("ERROR")) {
            if (!NetworkUtils.isNetworkConnected(mContext))
                return NETWORK_ERROR;
            else
                return OTHER_ERROR;
        }
        return 0;
    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if (result != 0) {
            if (result.equals(NETWORK_ERROR)) {
                showDialog("Please check network connection and try again!");
            } else
                showDialog("Problem loading results, please try after sometime.");
            if (listView != null) {
                setEmptyText("Touch to retry.");
            }
        } else {
            if (listView != null) {
                listView.getEmptyView().setVisibility(View.GONE);
            }
        }
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    public void showDialog(String alertMessage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(mContext).setMessage(alertMessage)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        dialog.show();
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }


    public void setProgressBar(View progressBar) {
        this.progressBar = progressBar;
    }

    public void setEmptyText(String emptyText) {
        ((TextView) listView.getEmptyView()).setText(emptyText);
        listView.getEmptyView().setVisibility(View.VISIBLE);
    }
}