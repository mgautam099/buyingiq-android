package com.buyingiq.app;

import android.app.Application;

import com.buyingiq.app.resources.MemoryCache;
import com.fasterxml.jackson.core.JsonFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import static com.buyingiq.app.resources.Categories.catIDs;
import static com.buyingiq.app.resources.Categories.catsToUse;

/**
 * Created by Mayank on 12/24/2014.
 */
public class BuyingIQApp extends Application {


    private static final String PROPERTY_ID = "UA-30334978-2";
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();
    /**
     * A single MemoryCache for images for whole app. Pass this as a parameter to ImageLoader instance.
     * Should always be in the main thread.
     */
    private MemoryCache memoryCache;
    //    public static HashMap<String, String> map;
//    public static HashMap<String, String> iMap;
    private HashMap<String, Integer> mCategories;
    private HashMap<Integer, Integer> mCategoryIDs;
    private JsonFactory factory;
    public BuyingIQApp() {
        super();
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t;
            switch (trackerId) {
                case APP_TRACKER:
                    t = analytics.newTracker(PROPERTY_ID);
                    break;
                case GLOBAL_TRACKER:
                    t = analytics.newTracker(R.xml.global_tracker);
                    break;
                default:
                    t = analytics.newTracker(PROPERTY_ID);
                    break;
            }
            mTrackers.put(trackerId, t);
        }
        return mTrackers.get(trackerId);
    }

    public synchronized MemoryCache getMemoryCache() {
        if (memoryCache == null)
            memoryCache = new MemoryCache();
        return memoryCache;
    }

    public synchronized void initCategories() {
        if (mCategories == null) {
            mCategories = new HashMap<>();
            for (int i = 0; i < catsToUse.length; i++) {
                mCategories.put((String) catsToUse[i][1], i);
            }
        }
    }

   /* public void createMap() {
        if (map != null && iMap != null)
            return;
        new AsyncTask<Void, Void, Void>(

        ) {
            @Override
            protected Void doInBackground(Void... voids) {
                map = new HashMap<String, String>();
                iMap = new HashMap<String, String>();
                for (Object[] c : catsToUse) {
                    map.put((String) c[0], (String) c[1]);
                    iMap.put((String) c[1], (String) c[0]);
                }
                return null;
            }
        }.execute();
    }*/

    public synchronized void initCategoryIDs() {
        if (mCategoryIDs == null) {
            mCategoryIDs = new HashMap<>();
            for (int i = 0; i < catIDs.length - 1; i++) {
                mCategoryIDs.put(catIDs[i], i);
            }
            //for all headphones.
            mCategoryIDs.put(catIDs[10], 2);
        }
    }

    /**
     * Get the index of the Category based on it's tag. This index
     * can then be used in {@link com.buyingiq.app.resources.Categories#catsToUse}
     * to get other info about this category.
     *
     * @return index of the category
     */
    public int queryInverseMap(String categoryTag) {
        initCategories();
        return mCategories.containsKey(categoryTag) ? mCategories.get(categoryTag) : -1;
    }

    public HashMap<String, Integer> getCategories() {
        initCategories();
        return mCategories;
    }

    public synchronized int findCategoryID(int id) {
        initCategoryIDs();
        return mCategoryIDs.containsKey(id) ? mCategoryIDs.get(id) : -1;
    }

    public enum TrackerName {
        APP_TRACKER,
        GLOBAL_TRACKER,
    }

    public synchronized JsonFactory getFactory(){
        if(factory ==null)
            factory = new JsonFactory();
        return factory;
    }

}
