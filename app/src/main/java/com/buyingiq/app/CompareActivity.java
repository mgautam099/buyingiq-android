package com.buyingiq.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.buyingiq.app.entities.CompareGroup;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;

public class CompareActivity extends FragmentActivity implements OnTaskCompleteCallback{

    private static final String TAG = "CompareActivity";
    private static final String FRAGMENT_TAG = "CompareFragment";
    //    private static final boolean D = true;
    ArrayList<Product> mProductList;
    ArrayList<CompareGroup> mCompareGroupList;
    CompareFragment compareFragment;
    String apiUrl = "products/compare/";
    int backAnimation;
    String defaultCompareUrl = "http://www.buyingiq.com/compare/";
    Tracker t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        handleIntent(getIntent(),savedInstanceState);
    }

    public void showDialog(String alertMessage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this).setMessage(alertMessage)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        dialog.show();
    }

    private void handleIntent(Intent intent, Bundle savedInstanceState) {
        if(Intent.ACTION_VIEW.equals(intent.getAction()))
        {
            backAnimation = R.anim.slide_out_right;
            String[] ids = getProductIdsFromURI(intent.getData());
            if(ids == null)
                showDialog("Invalid Compare Url!");
            else
            {
                new ProductDownloadTask(this).execute(ids[0]);
                new ProductDownloadTask(this).execute(ids[1]);
            }
        }
        else
        {
            mProductList = intent.getParcelableArrayListExtra(StringUtils.TAG_PRODUCT_LIST);
            backAnimation = intent.getIntExtra(StringUtils.KEY_BACK_ANIMATION, R.anim.scale_down_bottom_right);
            setContentView(R.layout.activity_compare);
            if(savedInstanceState!=null)
            {
                mCompareGroupList = savedInstanceState.getParcelableArrayList(StringUtils.KEY_COMPARE_LIST);
                compareFragment = (CompareFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
                compareFragment.updateViews(mCompareGroupList, null);
            }
            else
            {
                displayPage();
            }
            sendTracker();
        }
    }

    private String[] getProductIdsFromURI(Uri data){
        if(data.getPathSegments().get(1).contains("-vs-"))
        {
            String[] names = data.getPathSegments().get(1).split("-vs-");
            String[] ids = new String[2];
            for (int i=0; i<2; i++)
            {
                int j = names[i].lastIndexOf("-");
                ids[i] = names[i].substring(j+1);
            }
            return ids;
        }
        return null;
    }



    private void displayPage(){
        mCompareGroupList = new ArrayList<>();
        new LoadCompareTask(this, mProductList.get(0).mID, mProductList.get(1).mID).execute();
        compareFragment = CompareFragment.newInstance(mProductList);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, compareFragment, FRAGMENT_TAG)
                .commit();
    }

    private void sendTracker(){
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        t.setScreenName(TAG);
        t.send(new HitBuilders.EventBuilder("CompareProducts", "Compare")
                .setLabel(mProductList.get(0).category + ":" + mProductList.get(0).mID + "-vs-" + mProductList.get(1).mID)
                .build());
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.compare, menu);
        restoreActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.fade_in, backAnimation);
                return true;
            case R.id.action_search:
                onSearchRequested();
                return true;
            case R.id.action_share:
                onSharingRequested();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSharingRequested() {
        if(mProductList==null || mProductList.size()<2)
            Toast.makeText(this,"Wait for the page to load.",Toast.LENGTH_SHORT).show();
        else {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_SEND);
            i.putExtra(Intent.EXTRA_TEXT, defaultCompareUrl + mProductList.get(0).mID + "-vs-" + mProductList.get(1).mID + "/");
            i.setType("text/plain");
            startActivity(i);
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("COMPARE PRODUCTS");
            //    Utils.changeUpIcon(actionBar, this, R.drawable.ic_action_previous_item);
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_exit, backAnimation);
    }

    @Override
    public boolean onSearchRequested() {
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }

    @Override
    public void onComplete(Parcelable parcelable) {
        if(mProductList==null)
            mProductList = new ArrayList<>();
        mProductList.add((Product)parcelable);
        if(mProductList.size()==2) {
            setContentView(R.layout.activity_compare);
            displayPage();
            sendTracker();
        }
    }

    /*public MemoryCache getMemoryCache() {
        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/

    private class LoadCompareTask extends AsyncTask<Void, Void, Void> {

        Context mContext;
        String id1;
        String id2;
        ProgressDialog mProgressDialog;

        LoadCompareTask(Context context, String id1, String id2) {
            this.id1 = id1;
            this.id2 = id2;
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(mContext, null, "Please Wait", true, false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (NetworkUtils.isNetworkConnected(mContext)) {
                JsonFactory factory = ((BuyingIQApp) getApplication()).getFactory();
                NetworkUtils networkUtils = new NetworkUtils(t);
                ArrayList<Pair<String,String>> params = new ArrayList<>();
                params.add(new Pair<>("ids", id1));
                params.add(new Pair<>("ids", id2));
                String json = networkUtils.makeServiceCall(StringUtils.BASE_URL + apiUrl, NetworkUtils.GET, params);
                if (json != null && !json.contains("ERROR")) {
                    try {
                        JsonParser jp = factory.createParser(json);
                        jp.nextToken();
                        while (jp.nextToken() != JsonToken.END_OBJECT) {
                            String fieldName = jp.getCurrentName();
                            jp.nextToken();
                            if (StringUtils.TAG_COMPARISON.equals(fieldName))
                                populateCompareList(jp, id1, id2);
                            else if (StringUtils.TAG_PRODUCTS.equals(fieldName))
                                jp.skipChildren();
                        }
                    } catch (IOException e) {
                        NetworkUtils.networkCheck((CompareActivity) mContext);
                        Utils.Print.e(TAG, "IOException: " + e.getMessage());
//                        Utils.sendException(t, TAG, "IOException", "Compare: " + id1 + "-" + id2 + ":" + e.getMessage());
                        Utils.logException(e);
                    } catch (Exception e) {
                        Utils.Print.e(TAG, "Exception: " + e.getMessage());
                        Utils.logException(e);
//                        Utils.sendException(t, TAG, "Exception", "Compare: " + id1 + "-" + id2 + ":" + e.getMessage());
                    }
                }
            }
            return null;
        }

        private void populateCompareList(JsonParser jp, String id1, String id2) throws IOException {
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                mCompareGroupList.add(CompareGroup.createFromJSON(jp, id1, id2));
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (compareFragment != null)
                compareFragment.updateViews(mCompareGroupList, null);
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mProgressDialog.dismiss();
        }
    }
}
