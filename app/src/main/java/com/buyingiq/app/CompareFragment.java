package com.buyingiq.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.buyingiq.app.entities.CompareFeature;
import com.buyingiq.app.entities.CompareGroup;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.Categories;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.StringUtils;

import java.util.ArrayList;

public class CompareFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "CompareFragment";
    ArrayList<Product> mProductList;
    ArrayList<CompareGroup> mCompareGroupList;
    ImageLoader1 imageLoader1;

    //Required Empty Constructor
    public CompareFragment() {

    }


    public static CompareFragment newInstance(ArrayList<Product> mProductList, ArrayList<CompareGroup> mCompareGroupList) {
        CompareFragment f = new CompareFragment();
        Bundle b = new Bundle();
        b.putParcelableArrayList(StringUtils.TAG_PRODUCT_LIST, mProductList);
        if (mCompareGroupList != null)
            b.putParcelableArrayList(StringUtils.KEY_COMPARE_LIST, mCompareGroupList);
        f.setArguments(b);
        return f;
    }

    public static CompareFragment newInstance(ArrayList<Product> mProductList) {
        return newInstance(mProductList, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductList = getArguments().getParcelableArrayList(StringUtils.TAG_PRODUCT_LIST);
            if (getArguments().containsKey(StringUtils.KEY_COMPARE_LIST))
                mCompareGroupList = getArguments().getParcelableArrayList(StringUtils.KEY_COMPARE_LIST);
        }
        int i = ((BuyingIQApp) getActivity().getApplication()).findCategoryID(mProductList.get(0).categoryID);
        imageLoader1 = new ImageLoader1(getActivity(), Categories.catImgs[i]);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_compare, container, false);

        if (mProductList != null)
            setView(rootView);
        if (savedInstanceState != null) {
            mCompareGroupList = savedInstanceState.getParcelableArrayList(StringUtils.KEY_COMPARE_LIST);
            updateViews(mCompareGroupList, rootView);
        }
        ViewTreeObserver vto = rootView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int height = rootView.findViewById(R.id.main_layout).getMeasuredHeight();
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) (1 * getResources().getDisplayMetrics().density), height);
                lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
                rootView.findViewById(R.id.divider).setLayoutParams(lp);
            }
        });
        return rootView;
    }

    private void setView(View rootView) {
        int i = ((BuyingIQApp) getActivity().getApplication()).findCategoryID(mProductList.get(0).categoryID);
        imageLoader1.changeCurrentDefaultImage(Categories.catImgs[i]);
        ViewGroup v = (ViewGroup) rootView.findViewById(R.id.product_image_layout);
        if (i != -1) {
            ((ImageView) v.getChildAt(0)).setImageResource(Categories.catImgs[i]);
            ((ImageView) v.getChildAt(1)).setImageResource(Categories.catImgs[i]);
        }
        if (!TextUtils.isEmpty(mProductList.get(0).getOverviewImage()))
            imageLoader1.DisplayImage(mProductList.get(0).getOverviewImage(), (ImageView) v.getChildAt(0));
        if (!TextUtils.isEmpty(mProductList.get(1).getOverviewImage()))
            imageLoader1.DisplayImage(mProductList.get(1).getOverviewImage(), (ImageView) v.getChildAt(1));
        ViewGroup v1 = (ViewGroup) rootView.findViewById(R.id.product_name_layout);
        ((TextView) v1.getChildAt(0)).setText(mProductList.get(0).name);
        v.getChildAt(0).setTag(0);
        v.getChildAt(0).setOnClickListener(this);
        v.getChildAt(1).setTag(1);
        v.getChildAt(1).setOnClickListener(this);
        v1.getChildAt(0).setTag(0);
        v1.getChildAt(0).setOnClickListener(this);
        ((TextView) v1.getChildAt(3)).setText(mProductList.get(1).name);
        v1.getChildAt(3).setTag(1);
        v1.getChildAt(3).setOnClickListener(this);
        ViewGroup v2 = (ViewGroup) rootView.findViewById(R.id.product_price_layout);
        ((TextView) v2.getChildAt(0).findViewById(R.id.product_price)).setText(mProductList.get(0).price);
        ((TextView) v2.getChildAt(1).findViewById(R.id.product_price)).setText(mProductList.get(1).price);
        ViewGroup v3 = (ViewGroup) rootView.findViewById(R.id.product_rating_layout);
        ((TextView) v3.getChildAt(0).findViewById(R.id.product_rating)).setText(mProductList.get(0).rating);
        ((TextView) v3.getChildAt(1).findViewById(R.id.product_rating)).setText(mProductList.get(1).rating);
        ((TextView) v3.getChildAt(0).findViewById(R.id.product_votes)).setText(mProductList.get(0).votes);
        ((TextView) v3.getChildAt(1).findViewById(R.id.product_votes)).setText(mProductList.get(1).votes);

    }


    public void updateViews(ArrayList<CompareGroup> compareGroupsList, View rootView) {
        mCompareGroupList = compareGroupsList;
        LinearLayout compareLayout;
        if (getView() != null)
            compareLayout = (LinearLayout) getView().findViewById(R.id.compare_group_layout);
        else {
            if (rootView == null)
                return;
            else
                compareLayout = (LinearLayout) rootView.findViewById(R.id.compare_group_layout);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        for (CompareGroup cg : compareGroupsList) {
            View divider = inflater.inflate(R.layout.horizontal_divider, compareLayout, false);
            compareLayout.addView(divider);
            TextView groupHead = (TextView) inflater.inflate(R.layout.item_compare_group_head, compareLayout, false);
            groupHead.setText(cg.name);
            compareLayout.addView(groupHead);
            int childCount = 0;
            for (CompareFeature cf : cg.compareFeatures) {
                if (!TextUtils.isEmpty(cf.product1) || !TextUtils.isEmpty(cf.product2)) {
                    TextView featureHead = (TextView) inflater.inflate(R.layout.item_compare_head, compareLayout, false);
                    featureHead.setText(cf.name);
                    LinearLayout featureLayout = (LinearLayout) inflater.inflate(R.layout.item_compare_group, compareLayout, false);
                    ((TextView) featureLayout.getChildAt(0)).setText(TextUtils.isEmpty(cf.product1) ? "\u2014" : cf.product1);
                    ((TextView) featureLayout.getChildAt(2)).setText(TextUtils.isEmpty(cf.product2) ? "\u2014" : cf.product2);
                    compareLayout.addView(featureHead);
                    compareLayout.addView(featureLayout);
                    childCount++;
                }
            }
            if (childCount == 0) {
                compareLayout.removeView(groupHead);
                compareLayout.removeView(divider);
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), ProductActivity.class);
        intent.putExtra(StringUtils.TAG_PRODUCT, mProductList.get((int) v.getTag()));
        intent.putExtra(ProductDatabaseHelper.KEY_VISITED_FROM, TAG);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(StringUtils.KEY_COMPARE_LIST, mCompareGroupList);
    }
}
