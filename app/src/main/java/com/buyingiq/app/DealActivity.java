package com.buyingiq.app;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.buyingiq.app.entities.Deal;
import com.buyingiq.app.entities.Store;
import com.buyingiq.app.listadapters.DealListAdapter;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.StringUtils;

import java.util.ArrayList;

/**
 * Activity to display deals from a particular store. This Activity requires the
 * {@link com.buyingiq.app.entities.Product} name,  a {@link com.buyingiq.app.entities.Store} object,
 * and a {@link com.buyingiq.app.entities.Deal} list provided for the selected {@link
 * com.buyingiq.app.entities.StoreDeals} item through Intent Extras.
 */
public class DealActivity extends ListActivity {

    public static final String EXTRA_PRODUCT_NAME = "product_name";
    public static final String EXTRA_STORE = "store";
    public static final String EXTRA_DEAL_LIST = "deal_list";

    String mProductName;
    Store mStore;
    ArrayList<Deal> mDealList;
    String mTitle;
    AbsListView mListView;
    DealListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProductName = getIntent().getStringExtra(EXTRA_PRODUCT_NAME);
        mStore = getIntent().getParcelableExtra(EXTRA_STORE);
        mDealList = getIntent().getParcelableArrayListExtra(EXTRA_DEAL_LIST);
        //    mTitle = "AT "+mStore.name.toUpperCase(Locale.getDefault());
        mTitle = mProductName;
        setContentView(R.layout.activity_deal);
        TextView offerView = (TextView) findViewById(R.id.offer_label);
        offerView.setText(String.format(getString(R.string.text_offer_string), mDealList.size(), mStore.name));
        TextView storeReviewLabel = (TextView) findViewById(R.id.store_review_count_label);
        storeReviewLabel.setText(mStore.getReviewCount() + " Seller Reviews");
        ImageView storeImage = (ImageView) findViewById(R.id.store_image);
        RatingBar storeRating = (RatingBar) findViewById(R.id.store_rating_bar);
        storeRating.setRating(Float.parseFloat(mStore.getRatingOverall()));
        new ImageLoader1(this, 0).DisplayImage(mStore.getImageLarge(), storeImage);
        mListView = getListView();
        mAdapter = new DealListAdapter(this, mStore.name, mDealList);
        setListAdapter(mAdapter);
        System.out.println(getListView().getChildCount());
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setTitle(mTitle.toUpperCase());
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //    Utils.changeUpIcon(actionBar, this, R.drawable.ic_action_previous_item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.deal, menu);
        restoreActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                overridePendingTransition(0, R.anim.slide_out_right);
                return true;
            case R.id.action_search:
                onSearchRequested();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onSearchRequested() {
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_right);
    }

    public void onStoreReviewsClicked(View view) {
        Intent i = new Intent(this, StoreActivity.class);
        i.putExtra(StringUtils.ARG_STORE, mStore);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

/*    public MemoryCache getMemoryCache() {
        //    RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        //    mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/
}
