package com.buyingiq.app;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.buyingiq.app.entities.Facet;
import com.buyingiq.app.listadapters.FacetListAdapter;

import java.util.ArrayList;


/**
 * A fragment holding a facet list. It is currently being used in pager adapter for FolderFragment
 */
public class FacetFragment extends ListFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_name";
    private static final String ARG_FACET_LIST = "facet_list";
    private ArrayList<Facet> mFacets;
    private FacetListAdapter mListAdapter;

    public FacetFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FacetFragment newInstance(String sectionName, ArrayList<Facet> facets) {
        FacetFragment fragment = new FacetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, sectionName);
        args.putParcelableArrayList(ARG_FACET_LIST, facets);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFacets = getArguments().getParcelableArrayList(ARG_FACET_LIST);
        }
        mListAdapter = new FacetListAdapter(getActivity(), mFacets);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_facet, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(mListAdapter);
    }
}