package com.buyingiq.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

/**
 *
 */
public class FeedbackActivity extends NonNavigationActivity implements TextView.OnEditorActionListener {
    private static final String PREFS = "feedback_prefs";
    EditText message;
    EditText email;
    Button submit;
    ProgressDialog progressDialog;
    int submitted;
    Tracker t;
    TextWatcher messageWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0)
                submit.setEnabled(true);
            else
                submit.setEnabled(false);
        }
    };

    public static boolean isValidEmail(CharSequence target) {
        return target != null && (target.length() == 0 || android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches());

    }

    public static boolean isValidMessage(CharSequence target) {
        return target != null && (target.length() > 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.mTitle = "Feedback";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_feedback);
        message = (EditText) findViewById(R.id.feedback_message);
        email = (EditText) findViewById(R.id.feedback_email);
        submit = (Button) findViewById(R.id.feedback_submit);
        message.addTextChangedListener(messageWatcher);
        email.setOnEditorActionListener(this);
        SharedPreferences sp = getSharedPreferences(PREFS, MODE_PRIVATE);
        if (!sp.contains("email"))
            email.setText(sp.getString("email", ""));
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Submitting Feedback");
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    public void onSubmitButtonClicked(View view) {
        submitFeedback();
    }

    public void submitFeedback() {
        if (!isValidMessage(message.getText())) {
            showDialog("Message cannot be empty.", false);
            changeViewBackground(message, R.drawable.edittext_bg_red);
        } else {
            if (!isValidEmail(email.getText())) {
                showDialog("Please provide a proper email id", false);
                changeViewBackground(email, R.drawable.edittext_bg_red);
            } else {
                changeViewBackground(message, R.drawable.edittext_bg_grey);
                changeViewBackground(email, R.drawable.edittext_bg_grey);
                if (!email.getText().toString().isEmpty())
                    getSharedPreferences(PREFS, MODE_PRIVATE).edit().putString("email", email.getText().toString()).commit();
                new FeedbackTask(message.getText().toString(), email.getText().toString()).execute();
            }
        }

    }

    private void changeViewBackground(View view, int backgroundResource) {
        int pL = view.getPaddingLeft();
        int pT = view.getPaddingTop();
        int pR = view.getPaddingRight();
        int pB = view.getPaddingBottom();
        view.setBackgroundResource(backgroundResource);
        view.setPadding(pL, pT, pR, pB);

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null && event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        } else if (actionId == EditorInfo.IME_ACTION_SEND
                || event == null
                || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            submitFeedback();
            return true;
        }

        return false;
    }

    public void showDialog(String alertMessage, final boolean finish) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this).setMessage(alertMessage)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (finish)
                            finish();
                    }
                });
        dialog.show();
    }

    class FeedbackTask extends AsyncTask<Void, Void, String> {
        String message;
        String email;

        FeedbackTask(String message, String email) {
            this.message = message;
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... ps) {
            NetworkUtils utils = new NetworkUtils(t);
            String sub = "Device: " + Build.MODEL +
                    " Manufacturer: " + Build.MANUFACTURER +
                    " AndroidVersion: " + Build.VERSION.RELEASE +
                    " AppVersion: " + BuildConfig.VERSION_NAME;
            ArrayList<Pair<String,String>> params = new ArrayList<>();
            params.add(new Pair<>("subject", sub));
            params.add(new Pair<>("message", message));
            params.add(new Pair<>("email", email));
            return utils.makeServiceCall(StringUtils.BASE_URL + "feedback/", NetworkUtils.POST, params);
        }

        @Override
        protected void onPostExecute(String buff) {
            if (buff != null && !buff.contains("ERROR")) {
                showDialog("Thank you for the feedback.", true);
            } else {
                showDialog("Sorry we could not submit the feedback this time. Please check your connection and try again.", false);
            }
            progressDialog.dismiss();
        }
    }

}
