package com.buyingiq.app;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.buyingiq.app.entities.Folder;
import com.buyingiq.app.widget.SlidingTabLayout;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FolderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FolderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FolderFragment extends Fragment {
    private static final String ARG_FOLDER_LIST = "folderList";
    private static final String ARG_SELECTED_COUNT = "selectedCount";

    private OnFragmentInteractionListener mListener;

    private ArrayList<Folder> mFolders;
    private FacetPagerAdapter mPagerAdapter;
    private SlidingTabLayout mSlidingTabLayout;

    private int selectedCount;

    public FolderFragment() {
        // Required empty public constructor
    }

    public static FolderFragment newInstance(ArrayList<Folder> folders, int selectedCount) {
        FolderFragment folderFragment = new FolderFragment();
        Bundle b = new Bundle();
        b.putParcelableArrayList(ARG_FOLDER_LIST, folders);
        b.putInt(ARG_SELECTED_COUNT, selectedCount);
        folderFragment.setArguments(b);
        return folderFragment;
    }

    public void setUp(SlidingTabLayout slidingTabLayout, ArrayList<Folder> folders) {
        mFolders = folders;
        mSlidingTabLayout = slidingTabLayout;
        createPages(getView());
    }


    public void updateCount(int count) {
        ((TextView) getView().findViewById(R.id.cancel_facet)).setText("CLEAR ALL" + (count > 0 ? " (" + count + ")" : ""));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFolders = getArguments().getParcelableArrayList(ARG_FOLDER_LIST);
            selectedCount = getArguments().getInt(ARG_SELECTED_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_folder, container, false);
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        View view = rootView.findViewById(R.id.padding_view);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getActionMasked();
                switch (action) {
                    case MotionEvent.ACTION_UP:
                        view.setTag("FacetFragment");
                        mListener.onViewUpEvent(view);
                        return true;
                    default:
                        return true;
                }
            }
        });
        //    createPages(view);
    }

    private void createPages(View view) {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mPagerAdapter = new FacetPagerAdapter(getChildFragmentManager());

        // Set up the ViewPager with the sections adapter.
        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        if (mSlidingTabLayout != null) {
            //Set Up tabs
            mSlidingTabLayout.setCustomTabView(R.layout.item_tab, R.id.tab_title);
            mSlidingTabLayout.setViewPager(mViewPager);
            mSlidingTabLayout.setFlag(SlidingTabLayout.FLAG_FOLDER_HEADER);
            mSlidingTabLayout.setFolderList(mFolders);
            mSlidingTabLayout.setBackgroundColor(Color.parseColor("#f3f3f3"));
            mSlidingTabLayout.setDividerColors(getResources().getColor(R.color.tab_divider_grey));
            mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(android.R.color.transparent), getResources().getColor(android.R.color.transparent));
        }
        mPagerAdapter.notifyDataSetChanged();
        updateCount(selectedCount);
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        View view = getView();
        if (view != null) {
            if (hidden)
                exitAnimation(view);
            else
                enterAnimation(view);
        }
        super.onHiddenChanged(hidden);
    }


    /**
     * Sliding animation to be played when Fragment becomes Visible
     *
     * @param view Root view of the Fragment
     */
    public void enterAnimation(View view) {
        Animation facetAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
        Animation viewAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        view.findViewById(R.id.container).startAnimation(facetAnimation);
        view.findViewById(R.id.padding_view).startAnimation(viewAnimation);
    }


    /**
     * Sliding animation to be played when Fragment becomes hidden
     *
     * @param view Root view of the Fragment
     */
    public void exitAnimation(View view) {
        Animation facetAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_top);
        Animation viewAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        view.findViewById(R.id.container).startAnimation(facetAnimation);
        view.findViewById(R.id.padding_view).startAnimation(viewAnimation);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void notifyChanged() {
        if (mPagerAdapter != null)
            mPagerAdapter.notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onViewUpEvent(View view);
    }

    /**
     * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class FacetPagerAdapter extends FragmentStatePagerAdapter {

        public FacetPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return FacetFragment.newInstance(mFolders.get(position).toString(), mFolders.get(position).facets);
        }

        @Override
        public int getCount() {
            return mFolders.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFolders.get(position).toString();
        }

    }


}
