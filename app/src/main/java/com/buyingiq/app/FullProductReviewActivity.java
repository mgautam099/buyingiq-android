package com.buyingiq.app;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buyingiq.app.entities.ProductReview;
import com.buyingiq.app.entities.Rating;
import com.buyingiq.app.entities.User;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.widget.LevelTextView;
import com.buyingiq.app.widget.TextImageView;

public class FullProductReviewActivity extends NonNavigationActivity {

    ProductReview mProductReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProductReview = getIntent().getParcelableExtra(StringUtils.TAG_REVIEW);
        setContentView(R.layout.activity_full_review);
        //    ((TextView)findViewById(R.id.section_label)).setText(getIntent().getStringExtra("section_label"));
        setView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.full_review, menu);
        restoreActionBar();
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        TextImageView image = (TextImageView) findViewById(R.id.user_display_image);
        if (!TextUtils.isEmpty(mProductReview.getUser().getImage3())) {
            new ImageLoader1(this, 0).DisplayImage(mProductReview.getUser().getImage3(), image);
        } else
            image.setImageBitmap(null);
    }

    private void setView() {
        User u = mProductReview.getUser();
        TextImageView image = (TextImageView) findViewById(R.id.user_display_image);
        image.setText(u.name.substring(0, 1).toUpperCase());
        TypedArray imgs = getResources().obtainTypedArray(R.array.user_bg_colors);
        image.setBackgroundColor(imgs.getColor(u.getRand(), Color.GRAY));
        imgs.recycle();
        ((TextView) findViewById(R.id.user_name)).setText(mProductReview.getUser().name);
        ((LevelTextView) findViewById(R.id.user_level)).setLevel(u.getLevel());
        //    ((TextView)findViewById(R.id.review_text)).setText(Html.fromHtml(mReview.getReview()).toString().replaceAll("\\n\\n", "\n"));
        ((TextView) findViewById(R.id.review_text)).setText(mProductReview.getReview());
        ((TextView) findViewById(R.id.product_rating)).setText(mProductReview.getOverallRating());
        CharSequence views = getViewsString(mProductReview.getViews());
        if (views.length() > 0)
            ((TextView) findViewById(R.id.review_views)).setText(views);
        else
            findViewById(R.id.review_views).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.review_helpfuls)).setText(getHelpfulString(mProductReview.getUpvotes()));
        ((TextView) findViewById(R.id.review_date)).setText(mProductReview.getPublishedOnStr());
        LinearLayout leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        LinearLayout rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        boolean left = true;
        for (int i = 0; i < mProductReview.getRatings().size(); i++) {
            Rating r = mProductReview.getRatings().get(i);
            if (left)
                leftLayout.addView(getRatingView(r));
            else
                rightLayout.addView(getRatingView(r));
            left = !left;
        }
    }

    private CharSequence getHelpfulString(int upvotes) {
        String helpful = " Helpfuls";
        StringBuilder sb = new StringBuilder().append(upvotes).append(helpful);
        switch (upvotes) {
            case 0:
                return "";
            case 1:
                return sb.substring(0, sb.length() - 1);
            default:
                return sb.toString();
        }
    }


    private CharSequence getViewsString(int views) {
        String view = " Views";
        StringBuilder sb = new StringBuilder().append(views).append(view);
        switch (views) {
            case 0:
                return "";
            case 1:
                return sb.substring(0, sb.length() - 1);
            default:
                return sb.toString();
        }
    }

    private View getRatingView(Rating r) {
        LayoutInflater inflater = getLayoutInflater();
        View child = inflater.inflate(R.layout.item_review_user, null, false);
        ((TextView) child.findViewById(R.id.rating_label)).setText(r.head);
        ((TextView) child.findViewById(R.id.rating_text)).setText("" + r.score);
        return child;
    }

    /*public MemoryCache getMemoryCache() {
        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_share:
                onSharingRequested();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSharingRequested() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_TEXT, "http://www.buyingiq.com/reviews/" + mProductReview.getUri());
        i.setType("text/plain");
        startActivity(i);
    }

}
