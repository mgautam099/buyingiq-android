package com.buyingiq.app;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buyingiq.app.entities.Rating;
import com.buyingiq.app.entities.StoreReview;
import com.buyingiq.app.entities.User;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.widget.LevelTextView;
import com.buyingiq.app.widget.TextImageView;

public class FullStoreReviewActivity extends NonNavigationActivity {

    private static final String HELPFUL = " Helpfuls";
    private static final String VIEW = " Views";
    StoreReview mStoreReview;
//    private MemoryCache mMemoryCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStoreReview = getIntent().getParcelableExtra(StoreReviewsFragment.TAG_REVIEW);
        setContentView(R.layout.activity_full_store_review);
        setView();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        TextImageView image = (TextImageView) findViewById(R.id.user_display_image);
        if (!TextUtils.isEmpty(mStoreReview.getUser().getImage3())) {
            new ImageLoader1(this, 0).DisplayImage(mStoreReview.getUser().getImage3(), image);
        } else
            image.setImageBitmap(null);
    }

    private void setView() {
        User u = mStoreReview.getUser();
        TextImageView image = (TextImageView) findViewById(R.id.user_display_image);
        image.setText(u.name.substring(0, 1).toUpperCase());
        TypedArray imgs = getResources().obtainTypedArray(R.array.user_bg_colors);
        image.setBackgroundColor(imgs.getColor(u.getRand(), Color.GRAY));
        imgs.recycle();
        ((TextView) findViewById(R.id.user_name)).setText(mStoreReview.getUser().name);
        ((LevelTextView) findViewById(R.id.user_level)).setLevel(u.getLevel());
        ((TextView) findViewById(R.id.review_text)).setText(mStoreReview.getReview());
        ((TextView) findViewById(R.id.review_helpfuls)).setText(getHelpfulString(mStoreReview.getUpvotes()));
        ((TextView) findViewById(R.id.review_date)).setText(mStoreReview.getAddedOnStr());
        LinearLayout leftLayout = (LinearLayout) findViewById(R.id.left_layout);
        LinearLayout rightLayout = (LinearLayout) findViewById(R.id.right_layout);
        boolean left = true;
        for (int i = 0; i < mStoreReview.getRatings().size(); i++) {
            Rating r = mStoreReview.getRatings().get(i);
            if (left)
                leftLayout.addView(getRatingView(r));
            else
                rightLayout.addView(getRatingView(r));
            left = !left;
        }
    }

    private CharSequence getHelpfulString(int upvotes) {
        StringBuilder sb = new StringBuilder().append(upvotes).append(HELPFUL);
        switch (upvotes) {
            case 0:
                return "";
            case 1:
                return sb.substring(0, sb.length() - 1);
            default:
                return sb.toString();
        }
    }

    private CharSequence getViewsString(int views) {
        StringBuilder sb = new StringBuilder().append(views).append(VIEW);
        switch (views) {
            case 0:
                return "";
            case 1:
                return sb.substring(0, sb.length() - 1);
            default:
                return sb.toString();
        }
    }

    private View getRatingView(Rating r) {
        LayoutInflater inflater = getLayoutInflater();
        View child = inflater.inflate(R.layout.item_review_user, null, false);
        ((TextView) child.findViewById(R.id.rating_label)).setText(r.head);
        ((TextView) child.findViewById(R.id.rating_text)).setText("" + r.score);
        return child;
    }


    /*public MemoryCache getMemoryCache() {
        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/
}
