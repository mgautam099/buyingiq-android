package com.buyingiq.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buyingiq.app.listadapters.BannerPagerAdapter;
import com.buyingiq.app.resources.Utils;
import com.buyingiq.app.widget.TwoToneTextView;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

import static com.buyingiq.app.resources.Categories.catsToUse;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.buyingiq.app.HomeFragment.OnHomeFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private static final int BANNER_COUNT = 3;
    private static final double BANNER_RATIO = 0.625;
    private static final String TAG = "HomeFragment";
    private static final String TITLE = "HOME";
    private static final String TAG_BANNERS = "banners";
    Tracker t;
    Timer timer;
    int page = 0;
    DisplayImageOptions option;
    private boolean isTouched;
    private OnHomeFragmentInteractionListener mListener;
    View.OnTouchListener searchTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (!isTouched) {
                isTouched = true;
                view.requestFocus();
                if (mListener != null)
                    mListener.onHomeSearchSelected();
                return true;
            }
            return false;
        }
    };
    private BannerPagerAdapter mBannerAdapter;
    //    private BannerAdapter mBannerAdapter;
    private ViewPager mViewPager;
    private CirclePageIndicator mIndicator;
    private int direction = 1;
    private String[] bgColors = {"#56463d", "dc6829"};

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        option = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        t.setScreenName(TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        LinearLayout navLayout = (LinearLayout) view.findViewById(R.id.nav_layout);
        for (int i = 0; i < 10; i++) {
            TwoToneTextView t = (TwoToneTextView) navLayout.getChildAt(i);
            t.setText((String) catsToUse[i][0]);
            t.setTag(i + 1);
            t.setFirstBackground(Color.parseColor((String) catsToUse[i][4]));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                t.setCompoundDrawablesRelativeWithIntrinsicBounds((Integer) catsToUse[i][3], 0, R.drawable.ic_action_next_item, 0);
            else
                t.setCompoundDrawablesWithIntrinsicBounds((Integer) catsToUse[i][3], 0, R.drawable.ic_action_next_item, 0);
        /*
            try {
        //        t.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable((Integer) catsToUse[i][3]),null,null,null);
            } catch (Exception e)
            {
                Utils.debug(TAG,e.getMessage());
            }*/
            t.setOnClickListener(this);
        }
        view.findViewById(R.id.search_button).setOnTouchListener(searchTouchListener);
        int h = (int) (getResources().getDisplayMetrics().widthPixels * BANNER_RATIO);
        view.findViewById(R.id.banner_pager).setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, h));
        TextView footer = (TextView) view.findViewById(R.id.version);
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            footer.setText("Version: v" + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Utils.Print.e(TAG, e.getMessage());
        }

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home, menu);
        restoreActionBar();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mBannerAdapter = new BannerPagerAdapter(getChildFragmentManager(), ((ProductListingActivity) getActivity()).mBannerList);
//        mBannerAdapter = new BannerAdapter(getActivity(),((ProductListingActivity)getActivity()).mBannerList,option);
        mViewPager = (ViewPager) view.findViewById(R.id.banner_pager);
        mViewPager.setAdapter(mBannerAdapter);
        //    mViewPager.setOnPageChangeListener(this);
//        mViewPager.setOffscreenPageLimit(BANNER_COUNT);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        mIndicator.setViewPager(mViewPager);
        if (((ProductListingActivity) getActivity()).mBannerList.size() > 0)
            showBanners();
        //    pageSwitcher(5);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activity.invalidateOptionsMenu();
        try {
            mListener = (OnHomeFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHomeFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        getActivity().invalidateOptionsMenu();
        super.onDetach();
        mListener = null;
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View view) {
        if (mListener != null)
            mListener.onCategorySelected((Integer) view.getTag());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay in milliseconds
    }

    @Override
    public void onDestroy() {
//        timer.cancel();
        super.onDestroy();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(TITLE);
        getActivity().setTitle(TITLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isTouched = false;
        restoreActionBar();
    }

    public void refreshBanners() {
        mBannerAdapter.notifyDataSetChanged();
    }

    public void hideBanners() {
        mViewPager.setVisibility(View.GONE);
        mIndicator.setVisibility(View.GONE);
    }

    public void showBanners() {
        mViewPager.setVisibility(View.VISIBLE);
        mIndicator.setVisibility(View.VISIBLE);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHomeFragmentInteractionListener {
        void onCategorySelected(int position);

        void onHomeSearchSelected();
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a separate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if (direction == 1) {
                        page = mViewPager.getCurrentItem() + 1;
                        if (page > BANNER_COUNT - 1) {
                            page = BANNER_COUNT - 2;
                            direction = 0;
                        }
                        mViewPager.setCurrentItem(page);
                    } else {
                        page = mViewPager.getCurrentItem() - 1;
                        if (page < 0) {
                            page = 1;
                            direction = 1;
                        }
                        mViewPager.setCurrentItem(page);
                    }
                }
            });
        }
    }
}
