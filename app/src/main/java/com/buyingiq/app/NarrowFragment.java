package com.buyingiq.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by maxx on 14/7/14.
 */
public class NarrowFragment extends Fragment {


    private static final String KEY_LABELS = "labels";
    private static final String TAG = "NarrowFragment";
    AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Log.i(TAG, "itemSelected: " + i);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    private static final String KEY_TAGS = "tags";
    ArrayList<String> labels;
    ArrayList<String> tags;
    ArrayAdapter<String> mAdapter;
    ListView mListView;
    private OnNarrowFragmentInteractionListener mListener;
    AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if (mListener != null)
                mListener.onItemSelected(tags.get(i));
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            labels = savedInstanceState.getStringArrayList(KEY_LABELS);
            tags = savedInstanceState.getStringArrayList(KEY_TAGS);
        } else {
            labels = new ArrayList<>();
            tags = new ArrayList<>();
        }
        mAdapter = new ArrayAdapter<>(getActivity(), R.layout.item_narrow_text, R.id.label, labels);
    }

    public void setUp(ArrayList<String> list, ArrayList<String> tags) {
        mAdapter.clear();
        for (String s : list)
            mAdapter.add(s);
        labels = list;
        this.tags = tags;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_narrow, container, false);
        View view = rootView.findViewById(R.id.padding_view);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getActionMasked();
                switch (action) {
                    case MotionEvent.ACTION_UP:
                        view.setTag("NarrowFragment");
                        mListener.onViewUpEvent(view);
                        return true;
                    default:
                        return true;
                }
            }
        });
        mListView = (ListView) rootView.findViewById(R.id.narrow_list_view);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(onItemClickListener);
        mListView.setOnItemSelectedListener(onItemSelectedListener);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

/*
    @Override
    public Animation onCreateAnimation(int transit,final boolean enter, int nextAnim) {
        Animation anim;
        if(enter){
            anim = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
        } else{
            anim = AnimationUtils.loadAnimation(getActivity(),R.anim.slide_out_top);
        }
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                View padding = getView().findViewById(R.id.padding_view);
                if(!enter)
                    padding.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                View padding = getView().findViewById(R.id.padding_view);
                if(enter)
                    padding.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        return anim;
    }*/

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(KEY_LABELS, labels);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnNarrowFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        View view = getView();
        if (view != null) {
            if (hidden)
                exitAnimation(view);
            else
                enterAnimation(view);
        }
        super.onHiddenChanged(hidden);
    }

    /**
     * Sliding animation to be played when Fragment becomes Visible
     *
     * @param view Root view of the Fragment
     */
    public void enterAnimation(View view) {
        Animation facetAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_top);
        Animation viewAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        view.findViewById(R.id.narrow_list_view).startAnimation(facetAnimation);
        view.findViewById(R.id.padding_view).startAnimation(viewAnimation);
    }

    /**
     * Sliding animation to be played when Fragment becomes hidden
     *
     * @param view Root view of the Fragment
     */
    public void exitAnimation(View view) {
        Animation facetAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_top);
        Animation viewAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
        view.findViewById(R.id.narrow_list_view).startAnimation(facetAnimation);
        view.findViewById(R.id.padding_view).startAnimation(viewAnimation);
    }


    public interface OnNarrowFragmentInteractionListener {
        public void onViewUpEvent(View view);

        public void onItemSelected(String tag);
    }

}
