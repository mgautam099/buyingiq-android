package com.buyingiq.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.buyingiq.app.listadapters.NavigationAdapter;
import com.buyingiq.app.widget.RobotoCheckedTextView;
import com.buyingiq.app.widget.RobotoTextView;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final float ALL_CATEGORIES_FONT_SIZE = 12.0f;
    private static final float HOME_FONT_SIZE = 13.0f;

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private NavigationAdapter mDrawerAdapter;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;

    private int mNavDrawable;
    private boolean isTaskRoot;

    private String[] shareOptions = {"Spread the Word", "Feedback", "Rate this App"};
    private int[] icons = {R.drawable.ic_action_share_dark, R.drawable.ic_action_email, R.drawable.ic_action_important};

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, true);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
        isTaskRoot = getActivity().isTaskRoot();
        if ((((BaseActivity) getActivity()).flags & BaseActivity.FLAG_BACK_ENABLED) != 0)
            isTaskRoot = true;
        mNavDrawable = isTaskRoot ? R.drawable.ic_drawer : R.drawable.ic_action_previous_item;
        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerListView = (ListView) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
            }
        });
        ListView.LayoutParams lp = new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT);
        RobotoCheckedTextView rTV1 = (RobotoCheckedTextView) getActivity().getLayoutInflater().inflate(R.layout.item_navigation, mDrawerListView, false);
        rTV1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, HOME_FONT_SIZE);
        rTV1.setText("  HOME");
        rTV1.setTypeFaceRoboto(RobotoTextView.ROBOTO_MEDIUM);
        RobotoTextView rTV = new RobotoTextView(getActivity());
        rTV.setLayoutParams(lp);
        rTV.setBackgroundColor(getResources().getColor(R.color.header_bg_grey));
        rTV.setTextSize(TypedValue.COMPLEX_UNIT_DIP, ALL_CATEGORIES_FONT_SIZE);
        rTV.setText("ELECTRONICS");
        float density = getResources().getDisplayMetrics().density;
        rTV.setPadding((int) (17 * density), (int) (6 * density), (int) (10 * density), (int) (6 * density));
        rTV.setTypeFaceRoboto(RobotoTextView.ROBOTO_MEDIUM);
        mDrawerListView.addHeaderView(rTV1, "Home", true);
        mDrawerListView.addHeaderView(rTV, "Electronics", false);
        addFeedbackOptions(lp);
        mDrawerAdapter = new NavigationAdapter(getActivity(), R.layout.item_navigation);
        mDrawerListView.setAdapter(mDrawerAdapter);
        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                mNavDrawable,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).commit();
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                if (!isTaskRoot)
                    super.onDrawerSlide(drawerView, 0);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (isTaskRoot)
                    super.onDrawerSlide(drawerView, slideOffset);
                else
                    super.onDrawerSlide(drawerView, 0);
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int position) {
        if (position == 12)
            return;
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            if (position != 0)
                position -= 1;
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.action_search) {
            Toast.makeText(getActivity(), "Search", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        //    actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }

    public int getCurrentSelectedPosition() {
        return mCurrentSelectedPosition;
    }

    public void setCurrentSelectedPosition(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
    }

    private void addFeedbackOptions(ListView.LayoutParams lp) {
        View view = new View(getActivity());
        view.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (10 * getResources().getDisplayMetrics().density)));
        view.setOnClickListener(null);
        mDrawerListView.addFooterView(view);
        RobotoTextView shareHead = new RobotoTextView(getActivity());
        shareHead.setLayoutParams(lp);
        shareHead.setBackgroundColor(getResources().getColor(R.color.header_bg_grey));
        shareHead.setTextSize(TypedValue.COMPLEX_UNIT_DIP, ALL_CATEGORIES_FONT_SIZE);
        shareHead.setText(getResources().getString(R.string.sharing_feedback));
        float density = getResources().getDisplayMetrics().density;
        shareHead.setPadding((int) (17 * density), (int) (6 * density), (int) (10 * density), (int) (6 * density));
        shareHead.setTypeFaceRoboto(RobotoTextView.ROBOTO_MEDIUM);
        mDrawerListView.addFooterView(shareHead, "Feedback", false);
        for (int i = 0; i < shareOptions.length; i++) {
            mDrawerListView.addFooterView(getFeedbackOptions(i), shareOptions[i], true);
        }
    }

    private View getFeedbackOptions(int index) {
        RobotoCheckedTextView rtv = (RobotoCheckedTextView) getActivity().getLayoutInflater().inflate(R.layout.item_navigation, mDrawerListView, false);
        rtv.setText(shareOptions[index]);
        rtv.setCompoundDrawablesWithIntrinsicBounds(icons[index], 0, 0, 0);
        return rtv;
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }
}