package com.buyingiq.app;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.buyingiq.app.resources.NetworkUtils;

/**
 * Created by maxx on 29/9/14.
 */
public class NonNavigationActivity extends FragmentActivity {

    protected String mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NetworkUtils.networkCheck(this);
        if (getIntent().hasExtra("section_label"))
            mTitle = getIntent().getStringExtra("section_label");
        //    ((TextView)findViewById(R.id.section_label)).setText(getIntent().getStringExtra("section_label"));
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(mTitle.toUpperCase());
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //    Utils.changeUpIcon(actionBar, this, R.drawable.ic_action_previous_item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                overridePendingTransition(0, R.anim.slide_out_right);
                return true;
            /*case R.id.action_search:
                onSearchRequested();
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.deal, menu);
        restoreActionBar();
        return true;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        //    if(hasFocus)
        //        getWindow().setBackgroundDrawable(null);
        super.onWindowFocusChanged(hasFocus);
    }


    @Override
    public boolean onSearchRequested() {
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_out_right);
    }
}
