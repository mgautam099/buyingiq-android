package com.buyingiq.app;

import android.os.Parcelable;

/**
 * Created by maxx on 17/04/15.
 */
public interface OnTaskCompleteCallback {
    public void onComplete(Parcelable parcelable);
}
