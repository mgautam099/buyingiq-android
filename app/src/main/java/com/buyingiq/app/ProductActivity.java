package com.buyingiq.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.buyingiq.app.debug.hv.ViewServer;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.entities.ProductReview;
import com.buyingiq.app.entities.StoreDeals;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.buyingiq.app.widget.SlidingTabLayout;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.buyingiq.app.resources.StringUtils.ARG_PRODUCT_ID;
import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCT;

public class ProductActivity extends BaseActivity
        implements ProductPricesFragment.OnFragmentInteractionListener,
        ProductReviewsFragment.OnFragmentInteractionListener,
        ProductAlternativesFragment.OnFragmentInteractionListener,
        ProductOverviewFragment.OnFragmentInteractionListener, OnTaskCompleteCallback{

    private static final String TAG = "ProductActivity";
    //    private static final boolean D = false;
    private static final String ARG_CURRENT_POSITION = "current_index";
    public Uri uri;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionPagerAdapter mSectionsPagerAdapter;
    SlidingTabLayout mSlidingTabLayout;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    ProductDownloadTask productDownloadTask;
    Tracker t;
    private Product mProduct;
    private ProgressDialog progressDialog;
    private int currentPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("");
        super.onCreate(savedInstanceState);
        if (getActionBar() != null)
            getActionBar().setTitle("Loading Product");
        setContentView(R.layout.activity_loading);
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        if (Utils.D)
            ViewServer.get(this).addWindow(this);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if(Intent.ACTION_VIEW.equals(intent.getAction())){
            Uri data = intent.getData();
            int i = data.getPathSegments().get(1).lastIndexOf("-");
            String id = data.getPathSegments().get(1).substring(i+1);
            intent.putExtra(ARG_PRODUCT_ID,id);
        }

        //Case when only product id is available, currently when
        //redirected from website and when clicking recent Products
        if (intent.hasExtra(ARG_PRODUCT_ID)) {
            progressDialog = ProgressDialog.show(this, null, "Please Wait", true, false);
            productDownloadTask = new ProductDownloadTask(this);
            productDownloadTask.execute(getIntent().getStringExtra(ARG_PRODUCT_ID));
        } else {
            //Case when product is completely present in for usage.
            if (intent.hasExtra(StringUtils.TAG_PRODUCT)) {
                mProduct = intent.getParcelableExtra(StringUtils.TAG_PRODUCT);
            }
            //Case when Json is available of the product but has not been parsed
            else
                try {
                    JsonFactory factory = ((BuyingIQApp)getApplication()).getFactory();
                    mProduct = Product.createFromJSON(factory.createParser(intent.getDataString()));
                } catch (JsonParseException e) {
                    Utils.Print.e(TAG, "JSONParseException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "JSONParseException", e.getMessage());
                } catch (IOException e) {
                    Utils.Print.e(TAG, "IOException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "IOException", e.getMessage());
                }
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //    super.onSaveInstanceState(outState);
        outState.putParcelable(TAG_PRODUCT, mProduct);
        outState.putInt(ARG_CURRENT_POSITION, currentPosition);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mProduct = savedInstanceState.getParcelable(TAG_PRODUCT);
            currentPosition = savedInstanceState.getInt(ARG_CURRENT_POSITION);
        }
        if (mProduct != null) {
            createPages();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            getWindow().setBackgroundDrawable(null);
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (productDownloadTask != null)
            productDownloadTask.cancel(true);
        if (Utils.D)
            ViewServer.get(this).removeWindow(this);
    }

    public void createPages() {

        setContentView(R.layout.activity_product);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionPagerAdapter(this, getSupportFragmentManager(), mProduct);
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(SectionPagerAdapter.TOTAL - 1);
        mViewPager.setCurrentItem(currentPosition);
        mSectionsPagerAdapter.notifyDataSetChanged();
        //Set Up tabs
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.item_product_tab, R.id.tab_title);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setFlag(SlidingTabLayout.FLAG_PRODUCT_HEADER);
        //    mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.filter_background_grey));
        mSlidingTabLayout.setDividerColors(Color.parseColor("#00000000"));
        mSlidingTabLayout.setBottomBorderColor(getResources().getColor(R.color.accent_orange));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.accent_orange), getResources().getColor(R.color.accent_orange));
        if (getActionBar() != null)
            getActionBar().setTitle(mProduct.name.toUpperCase());
        mSlidingTabLayout.setSelected(SectionPagerAdapter.POSITION_OVERVIEW);
        if (getIntent().hasExtra(ProductDatabaseHelper.KEY_VISITED_FROM))
            saveProduct(getIntent().getStringExtra(ProductDatabaseHelper.KEY_VISITED_FROM));
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product, menu);
        restoreActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                overridePendingTransition(0, R.anim.slide_out_right);
                return true;
            case R.id.action_search:
                onSearchRequested();
                return true;
            case R.id.action_share:
                onSharingRequested();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void onSharingRequested() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_TEXT, mProduct.url);
        i.setType("text/plain");
        startActivity(i);
    }


    @Override
    public boolean onSearchRequested() {
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        return true;
    }

    @Override
    public void onStoreSelected(StoreDeals deals) {
        if (deals.getDealList().size() > 0 && deals.isInStock()) {
            // Assuming that only deals that are in stock will have deal count > 0
            // so we will only open the deals for the store which actually has deal(s)
            // rather than for every store.
            Intent intent = new Intent(this, DealActivity.class);
            intent.putExtra(DealActivity.EXTRA_PRODUCT_NAME, mProduct.name);
            intent.putExtra(DealActivity.EXTRA_STORE, deals.getStore());
            intent.putExtra(DealActivity.EXTRA_DEAL_LIST, deals.getDealList());
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
        } else {
            // For stores not having deals, a toast will be displayed telling user to bugger off.
            Toast.makeText(this, "No deal available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onReviewSelected(ProductReview productReview, View view) {
        Intent i = new Intent(this, FullProductReviewActivity.class);
        i.putExtra("section_label", mProduct.name);
        i.putExtra(StringUtils.TAG_REVIEW, productReview);
        //    i.putExtra("profile_color",)
    /*    Bundle b = null;
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN)
        {
            b = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight()).toBundle();
            startActivity(i,b);
        }
        else
        {
            startActivity(i);
            overridePendingTransition(R.anim.scale_up_bottom_right, R.anim.activity_exit);
        }*/
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    @Override
    public void onAlternativeSelected(Product product) {
        ArrayList<Product> products = new ArrayList<>();
        products.add(mProduct);
        products.add(product);
        launchCompareActivity(products);
    }

    private void saveProduct(String visitedFrom) {
        ProductDatabaseHelper dh = new ProductDatabaseHelper(this);
        Product p = getProduct(dh, mProduct.mID);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        SQLiteDatabase db = dh.getWritableDatabase();
        String currentTime = dateFormat.format(date);
        ContentValues values = new ContentValues();
        if (p == null) {
            values.put(ProductDatabaseHelper.KEY_ID, mProduct.mID);
            values.put(ProductDatabaseHelper.KEY_NAME, mProduct.name);
            values.put(ProductDatabaseHelper.KEY_PRICE, mProduct.price);
            values.put(ProductDatabaseHelper.KEY_CAT, mProduct.category);
            values.put(ProductDatabaseHelper.KEY_RATING, mProduct.rating);
            values.put(ProductDatabaseHelper.KEY_VOTES, mProduct.votes);
            values.put(ProductDatabaseHelper.KEY_IMAGE_S, mProduct.smallImageUrl);
            values.put(ProductDatabaseHelper.KEY_IMAGE_M, mProduct.mediumImageUrl);
            values.put(ProductDatabaseHelper.KEY_IMAGE_L, mProduct.largeImageUrl);
            values.put(ProductDatabaseHelper.KEY_IMAGE_XL, mProduct.xLargeImageUrl);
            values.put(ProductDatabaseHelper.KEY_URL, mProduct.url);
            values.put(ProductDatabaseHelper.KEY_LAST_VISITED, currentTime);
            values.put(ProductDatabaseHelper.KEY_VISITED_FROM, visitedFrom);
            db.insert(ProductDatabaseHelper.RECENT_TABLE_NAME, null, values);
        } else {
            values.put(ProductDatabaseHelper.KEY_LAST_VISITED, currentTime);
            values.put(ProductDatabaseHelper.KEY_VISITED_FROM, visitedFrom);
            db.update(ProductDatabaseHelper.RECENT_TABLE_NAME, values, ProductDatabaseHelper.KEY_ID + "=" + mProduct.mID, null);
        }
        db.close();
        dh.close();
    }

    private Product getProduct(ProductDatabaseHelper dh, String id) {
        Product p = null;
        SQLiteDatabase db = dh.getReadableDatabase();
        String[] selectionArgs = {id};
        String selection = ProductDatabaseHelper.KEY_ID + "= ?";
        Cursor c = db.query(ProductDatabaseHelper.RECENT_TABLE_NAME, null, selection, selectionArgs, null, null, null);
        if (c.moveToNext()) {
            p = new Product(id);
            p.name = c.getString(1);
            p.category = c.getString(2);
            p.smallImageUrl = c.getString(3);
            p.mediumImageUrl =c.getString(4);
            p.largeImageUrl = c.getString(5);
            p.xLargeImageUrl = c.getString(6);
            p.price = "Rs. "+c.getString(7);
            p.url = c.getString(8);
        }
        db.close();
        return p;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_exit, R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.D)
            ViewServer.get(this).setFocusedWindow(this);
    }

    @Override
    public void onFullSpecificationSelected() {
        mViewPager.setCurrentItem(SectionPagerAdapter.POSITION_SPECIFICATIONS);
    }

    @Override
    public void onComparePriceSelected() {
        mViewPager.setCurrentItem(SectionPagerAdapter.POSITION_PRICES);
    }

    /**
     * Method to be called for comparing products
     *
     * @param products ArrayList of products to be compared
     */

    private void launchCompareActivity(ArrayList<Product> products) {
        Intent i = new Intent(this, CompareActivity.class);
        i.putParcelableArrayListExtra(StringUtils.TAG_PRODUCT_LIST, products);
        i.putExtra(StringUtils.KEY_BACK_ANIMATION, R.anim.slide_out_right);
        //    Bundle b = null;
        //    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN)
        //    {
        //        b = ActivityOptions.makeScaleUpAnimation(view,0,0,view.getWidth(),view.getHeight()).toBundle();
        //    }
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    @Override
    public void onComplete(Parcelable product) {

        mProduct = (Product)product;
        if (mProduct!=null)
            createPages();
        else {
            setTitle("Product Loading Failed");
            showDialog("Unable to load product. Please check your network and try again later.");
        }
        if (progressDialog != null)
            progressDialog.cancel();
    }

    public void showDialog(String alertMessage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this).setMessage(alertMessage)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        dialog.show();
    }
}
