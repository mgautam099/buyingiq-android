package com.buyingiq.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.buyingiq.app.entities.Product;
import com.buyingiq.app.listadapters.ProductAlternativeListAdapter;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.buyingiq.app.resources.StringUtils.ARG_PRODUCT_ID;
import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCT;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the (@link Callbacks)
 * interface.
 */
public class ProductAlternativesFragment extends ListFragment implements AbsListView.OnItemClickListener {

    public static final int MAX_FEATURES = 4;
    public static final String TAG = "ProductAlternatives";
    private static final String TAG_COMPARISON = "comparison";
    Tracker t;
    private String mProductID;
    private String apiUrl;
    private ArrayList<Product> mProductList;
    private OnFragmentInteractionListener mListener;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ArrayAdapter mAdapter;
    private LoadAlternativesTask lat;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductAlternativesFragment() {
    }

    public static ProductAlternativesFragment newInstance(String productID) {
        ProductAlternativesFragment fragment = new ProductAlternativesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCT_ID, productID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mProductID = getArguments().getString(ARG_PRODUCT_ID);
            apiUrl = String.format(StringUtils.BASE_URL + "products/%s/alternatives/", mProductID);
        }
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        mProductList = new ArrayList<>();
        mAdapter = new ProductAlternativeListAdapter(getActivity(), mProductList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_alternatives, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshList();
        setListAdapter(mAdapter);
        getListView().setOnItemClickListener(this);
        getListView().getEmptyView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        lat.cancel(true);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
    /*    Intent intent = new Intent(getActivity().getApplicationContext(), ProductActivity.class);
        intent.putExtra("product", mProductList.get(position));
        intent.putExtra(ProductDatabaseHelper.KEY_VISITED_FROM, TAG);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);*/
        mListener.onAlternativeSelected(mProductList.get(position));
    }

    public void refreshList() {
        lat = new LoadAlternativesTask(getActivity());
        lat.execute();
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = getListView().getEmptyView();
        if (emptyText instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onAlternativeSelected(Product product);
    }

    private class LoadAlternativesTask extends BaseSyncTask<Void> {

        LoadAlternativesTask(Activity context) {
            super(context, apiUrl, 0);
            super.setListView(getListView());
            super.setProgressBar(getView().findViewById(android.R.id.progress));
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer returnCode = super.doInBackground(voids);
            if (returnCode == 0) {
                try {
                    parseJackson(json);
                } catch (IOException e) {
                    Utils.Print.e(TAG, "IOException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "IOException", url + ":" + e.getMessage());
                    setEmptyText("Touch to retry.");
                    //TODO There should be a report option here
                    return -1;
                }
            }
            return returnCode;
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            super.onPostExecute(returnValue);
            if (returnValue.equals(0)) {
                if (mProductList.size() == 0) {
                    setEmptyText("No Alternatives");
                    listView.getEmptyView().setOnClickListener(null);
                } else
                    mAdapter.notifyDataSetChanged();
            }
        }

/*        private String[] comparisonString(JSONArray js) throws JSONException {
            int len = js.length() > MAX_FEATURES ? MAX_FEATURES : js.length();
            String[] comps = new String[len];
            for (int i = 0; i < len; i++) {
                JSONArray jsonArray = js.getJSONArray(i);
                comps[i] = jsonArray.getString(Product.FEATURE_STR_INDEX);
            }
            return comps;
        }*/

        private String[] comparisonString(JsonParser jp, int[] compInt) throws IOException {
            String[] comps = new String[MAX_FEATURES];
            int i = 0;
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                if (i < MAX_FEATURES) {
                    while (jp.nextToken() != JsonToken.END_ARRAY) {
                        jp.nextToken();
                        compInt[i] = jp.getIntValue();
                        jp.nextToken();
                        comps[i] = jp.getText();
                        jp.nextToken();
                    }
                    i++;
                } else {
                    jp.skipChildren();
                }
            }
            return Arrays.copyOfRange(comps, 0, i);
        }

    /*    private void parseJson(String buff) {
            try {
                JSONArray productsArray = new JSONArray(buff);
                for (int i = 0; i < productsArray.length(); i++) {
                    JSONObject productObject = productsArray.getJSONObject(i);
                    JSONObject productInfo = productObject.getJSONObject(TAG_PRODUCT);
                    JSONArray compArray = productObject.getJSONArray(TAG_COMPARISON);
                    Product p = Product.createFromJSON(productInfo);
                    p.setComparisonString(comparisonString(compArray));
                    mProductList.add(p);
                }
            } catch (JSONException e) {
                Utils.Print.e(TAG, "JSONException: " + e.getMessage());
            } catch (Exception e) {
                Utils.Print.e(TAG, "Exception: " + e.getMessage());
            }
        }*/

        private void parseJackson(String json) throws IOException {
            JsonFactory factory = ((BuyingIQApp) ((BaseActivity) mContext).getApplication()).getFactory();
            JsonParser jp = factory.createParser(json);
            jp.nextToken();
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                Product p = new Product("");
                while (jp.nextToken() != JsonToken.END_OBJECT) {
                    String fieldName = jp.getCurrentName();
                    jp.nextToken();
                    switch (fieldName) {
                        case TAG_PRODUCT:
                            p.fillFromJson(jp);
                            break;
                        case TAG_COMPARISON:
                            int[] compInt = new int[4];
                            p.comparisonString = comparisonString(jp, compInt);
                            p.comparisonInt = compInt;
                            break;
                        default:
                            jp.skipChildren();
                            break;
                    }
                }
                mProductList.add(p);
            }
        }
    }
}
