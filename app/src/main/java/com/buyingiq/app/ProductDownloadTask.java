package com.buyingiq.app;

import android.app.Activity;
import android.os.AsyncTask;

import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;

/**
 * Created by maxx on 17/04/15.
 */
public class ProductDownloadTask extends AsyncTask<String, Void, Integer> {

    String TAG = "ProductDownloadTask";
    Activity mContext;
    Product mProduct;

    public ProductDownloadTask(Activity context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        NetworkUtils.networkCheck(mContext);
    }

    @Override
    protected Integer doInBackground(String... strings) {
        NetworkUtils networkUtils = new NetworkUtils(null);
        String url = StringUtils.BASE_URL + "products/" + strings[0] + "/";
        String json = networkUtils.makeServiceCall(url, NetworkUtils.GET);
        if (json != null && !json.contains("ERROR")) {
            try {
                JsonFactory factory = ((BuyingIQApp) mContext.getApplication()).getFactory();
                JsonParser jp = factory.createParser(json);
                jp.nextToken();
                mProduct = Product.createFromJSON(jp);
            } catch (IOException e) {
                Utils.Print.e(TAG, "IOException: " + e.getMessage());
                Utils.logException(e);
                return -2;
            }
        } else
            return -1;
        return 0;
    }

    @Override
    protected void onPostExecute(Integer str) {
        if (str != 0)
            mProduct = null;
        try {
            ((OnTaskCompleteCallback) mContext).onComplete(mProduct);
        } catch (ClassCastException e) {
            throw new ClassCastException(mContext.toString() + " should implement OnTaskCompleteCallback");
        }
    }
}