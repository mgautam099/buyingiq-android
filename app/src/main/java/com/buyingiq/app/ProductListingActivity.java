package com.buyingiq.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.buyingiq.app.debug.hv.ViewServer;
import com.buyingiq.app.entities.Banner;
import com.buyingiq.app.entities.Facet;
import com.buyingiq.app.entities.Folder;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.Categories;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.buyingiq.app.widget.CheckBoxLeft;
import com.buyingiq.app.widget.SlidingTabLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;
import java.util.ArrayList;

import static com.buyingiq.app.resources.Categories.catsToUse;
import static com.buyingiq.app.resources.StringUtils.ARG_NAV_CAT;
import static com.buyingiq.app.resources.StringUtils.ARG_PRODUCT_ID;

public class ProductListingActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        ProductListingFragment.OnTabSelectedListener, NarrowFragment.OnNarrowFragmentInteractionListener,
        FolderFragment.OnFragmentInteractionListener, HomeFragment.OnHomeFragmentInteractionListener,
        BannerFragment.OnBannerSelectedListener {

    //The Actions for the activities.
    public static final String ACTION_BROWSE = "com.buyingiq.action.BROWSE";
    public static final String ACTION_HOME_NAV = "com.buyingiq.action.HOME_NAV";
    //public static final String ACTION_HOME_BANNER = "com.buyingiq.action.HOME_BANNER";
    public static final int MAX_PRODUCT_ID_LEN = 6;
    public static final int MIN_PRODUCT_ID_LEN = 5;
    public static final int TIME_TO_EXIT_CLICK = 2000; // Time to wait for back click to exit the app (in milli seconds)
    public static final String HOME_LOCATION = "home";
    private static final String TAG = "ProductListingActivity";
    //    public static final int SEARCH_ACTIVITY_CODE = 1;
//    private static final int COMPARE_ACTIVITY_CODE = 2;
    public static int mCurrentDefaultImage = 0;
    public static String[] sortProductCategories = {
            "Popularity", "Rating - High to Low", "Price - High to Low", "Price - Low to High"
    };
    public static String[] sortProductTags = {
            "popularity-desc", "rating-desc", "price-desc", "price-asc"
    };
    public static int productListingSize;
    public static int overviewSize;
    //      public static final String BASE_URL = "http://api.buyingiq.com/v1/";
    public ArrayList<Banner> mBannerList;
    Tracker t;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private ProductListingFragment mProductListingFragment;
    private long lastBackClick;
    private HomeFragment mHomeFragment;

//    private ImageLoader1 mImageLoader;

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.

        File cacheDir;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir = new File(context.getExternalCacheDir(), "imgs");
        else
            cacheDir = new File(context.getCacheDir(), "imgs");
        if (!cacheDir.exists())
            cacheDir.mkdirs();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCache(new UnlimitedDiscCache(cacheDir))
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //    if(super.mTitle==null)
        //    super.mTitle = "BuyingIQ";
        setTitle("");
        super.onCreate(savedInstanceState);
        productListingSize = getResources().getDimensionPixelSize(R.dimen.product_image_size) - getResources().getDimensionPixelSize(R.dimen.product_image_padding);
        overviewSize = getResources().getDimensionPixelSize(R.dimen.product_overview_image_height) - (2 * getResources().getDimensionPixelSize(R.dimen.product_overview_image_padding));
        initImageLoader(getApplicationContext());
        //Crashlytics.start(this);
        restoreBanners();
        //    mBannerList = new ArrayList<>();
        //    setContentView(R.layout.activity_product_listing);
        handleIntent(getIntent());
        //    launchCompareActivity();
        if (Utils.D)
            ViewServer.get(this).addWindow(this);
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    /**
     * Handle the intent on activity start if intent is {}
     *
     * @param intent The intent when activity starts
     */


    public void handleIntent(Intent intent) {
        //Handles search action
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String searchQuery = intent.getStringExtra(SearchManager.QUERY);
            doSearch(searchQuery);
        }
        //Handles Open product or list action
        else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String lastSeg = intent.getData().getPathSegments().get(1);
            if(lastSeg.startsWith("q:"))
            {
                String searchQuery = lastSeg.substring(2);
                doSearch(searchQuery);
            }
            else
                displayList(lastSeg);
            /*if (data != null)
                openResult(data);
            else {
                if (intent.hasExtra(SearchManager.QUERY))
                    openResult(intent.getStringExtra(SearchManager.QUERY));
            }*/
        }
        //Handles the action for intent from other app or browser
        /*else if (ACTION_BROWSE.equals(intent.getAction())) {
            Uri data = intent.getData();
            String query = data.getPath().replaceFirst("/", "");
            String productID = getProductID(query);
            if (productID != null) {

            } else {
                String cat = data.getPathSegments().get(0);
                //if category is to be displayed in the app.
                if (iMap.containsKey(cat)) {
                    if (iMap.containsKey(query))
                        displayList(query, null);
                    else
                        displayList(cat, null);
                }
            }
        } */
        else if (ACTION_HOME_NAV.equals(intent.getAction())) {
            int position = intent.getIntExtra(ARG_NAV_CAT, 0);
            onNavigationDrawerItemSelected(position);
        }
    }

//    private String getProductID(String query) {
//        int index = query.lastIndexOf("-");
//        String productID = query.substring(index + 1);
//        if (productID.length() >= MIN_PRODUCT_ID_LEN && productID.length() <= MAX_PRODUCT_ID_LEN) {
//            try {
//                Integer.parseInt(productID);
//                return productID;
//            } catch (NumberFormatException e) {
//                return null;
//            }
//        }
//        return null;
//    }

    public void doSearch(String searchQuery) {
        String[] names = {"q", "facet", "page"};
        String[] values = {searchQuery, "1", "1"};
        mProductListingFragment = null;
        mProductListingFragment
                = ProductListingFragment.newInstance(names, values, true, "Results: " + "\"" + searchQuery + "\"");
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mProductListingFragment, ProductListingFragment.TAG)
                .commit();
    }

    public void openResult(Uri jsonData) {
        openResult(jsonData, null);
    }

    public void openResult(String productID) {
        openResult(null, productID);
    }

    public void openResult(Uri data, String productID) {
        Intent i = new Intent(this, ProductActivity.class);
        i.setData(data);
        if (productID != null)
            i.putExtra(ARG_PRODUCT_ID, productID);
        i.putExtra(ProductDatabaseHelper.KEY_VISITED_FROM, TAG);
        startActivity(i);
        overridePendingTransition(0, 0);
    }


    private void displayList(String tag){
        int index = ((BuyingIQApp) getApplication()).queryInverseMap(tag);
        if(mProductListingFragment!=null)
            displayList(index, mProductListingFragment.mCurrentQuery);
        else
            displayList(index,null);
    }

    /**
     * Used to display a list of products based on a category index and
     * a search term for that index. The search term should be null in order to
     * display all the products in a category.
     * Most common use is in Navigation and homepage category lists, where the
     * index of the category is known.
     *
     * @param index Position of the category to be displayed based on
     * {@link com.buyingiq.app.resources.Categories#catsToUse}
     * @param q Query for search results top be displayed.
     */
    private void displayList(int index, @Nullable String q) {
        if (mProductListingFragment != null)
            mProductListingFragment.stopTasks();
        String[] names;
        String[] values;
        String title;
        String tag = (String) Categories.catsToUse[index][1];
        if (q == null) {
            if (index != -1)
                title = (String) Categories.catsToUse[index][0];
            else
                title = "";
            names = new String[]{"tags", "facet", "page"};
            values = new String[]{tag, "1", "1"};
        } else {
            if (index != -1)
                title = "\"" + q + "\" " + catsToUse[index][0];
            else
                title = "\"" + q + "\" ";
            names = new String[]{"q", "tags", "facet", "page"};
            values = new String[]{q, tag, "1", "1"};
        }
        //update the default image whenever listing is changed.
        mCurrentDefaultImage = (Integer) Categories.catsToUse[index][2];
        // update the product_listing content by replacing fragments
        mProductListingFragment = null;
        mProductListingFragment = ProductListingFragment.newInstance(names, values, false, title);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mProductListingFragment)
                .commit();
    }

    public void displayHome() {
        if (mHomeFragment == null)
            mHomeFragment = HomeFragment.newInstance();
        if (mNavigationDrawerFragment != null)
            mNavigationDrawerFragment.setCurrentSelectedPosition(0);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.container, mHomeFragment)
                .commit();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (position > 0 && position < 11) {
            mCurrentDefaultImage = (Integer) Categories.catsToUse[position - 1][2];
            displayList(position - 1, null);
        } else if (position >= 11) {
            switch (position) {
                case 13:
                    onSharingRequsted();
                    break;
                case 14:
                    onFeedbackRequested();
                    break;
                case 15:
                    onRatingRequested();
                    break;
                default:
                    break;
            }
        } else
            displayHome();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.

            //if we want different menus in Home and Product Listing use this
        /*    if (mHomeFragment != null && !mHomeFragment.isAdded()) {
                //Let home decide its own Menu.
                getMenuInflater().inflate(R.menu.product_listing, menu);
            }
            else
            {
                getMenuInflater().inflate(R.menu.home, menu);
            }*/
            //Else we use this for same items
            getMenuInflater().inflate(R.menu.home, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_share:
                onSharingRequsted();
                return true;
            case R.id.action_rate:
                onRatingRequested();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSharingRequsted() {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_TEXT, getString(R.string.app_sharing_text) + StringUtils.APP_STORE_URL);
        i.setType("text/plain");
        startActivity(i);
    }

    private void onRatingRequested() {
        final Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        final Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (getPackageManager().queryIntentActivities(rateAppIntent, 0).size() > 0) {
            startActivity(rateAppIntent);
        }
    }

    /**
     * A callback method, called when filter tab is selected
     *
     * @param currentSelection Index of the Filter type selected
     */

    @Override
    public void onTabSelected2(int currentSelection, int lastSelection) {
        /*if (mProductListingFragment.updating) {
            Toast.makeText(this, "Loading, please wait.", Toast.LENGTH_SHORT).show();
            return;
        }*/
        FragmentManager fm = mProductListingFragment.getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //    ft.setCustomAnimations(R.anim.slide_in_top, R.anim.slide_in_top);
        Fragment fragment = fm.findFragmentByTag(ProductListingFragment.NAVIGATION_TAG);
        if (fragment.isHidden()) {
            ft.show(fragment);
        } else {
            if (currentSelection == lastSelection) {
                ft.setCustomAnimations(R.anim.activity_exit, R.anim.activity_exit);
                ft.hide(fragment);
            }
        }
        ft.commit();
    }

    @Override
    public void onViewUpEvent(View view) {
        FragmentManager fm = mProductListingFragment.getChildFragmentManager();
        Fragment fragment = fm.findFragmentByTag(ProductListingFragment.NAVIGATION_TAG);
        if (!fragment.isHidden()) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(R.anim.activity_exit, R.anim.activity_exit);
            ft.hide(fragment);
            mProductListingFragment.selectedTab = -1;
            if (view.getTag().equals("FacetFragment")) {
                //Clear previously selected tab
                if (mProductListingFragment.getView() != null) {
                    SlidingTabLayout slidingTabLayout = (SlidingTabLayout) mProductListingFragment.getView().findViewById(R.id.sliding_tabs);
                    slidingTabLayout.deselectLastSelected(-1);
                }
                mProductListingFragment.tabAdapter.notifyDataSetChanged();
            } else {
                if (mProductListingFragment.getView() != null)
                    mProductListingFragment.getView().findViewById(R.id.narrow_textview).setSelected(false);
            }
            ft.commit();
        }
    }

    @Override
    public void onItemSelected(String tag) {
        displayList(tag);
    }

    public void clearFilters(View view) {
        new AlertDialog.Builder(this).setMessage("Do you want to clear all the filters?").setNegativeButton("No", null).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (Folder fo : mProductListingFragment.mFolderList) {
                    for (Facet f : fo.facets) {
                        if (f.isSelected() == 1) {
                            f.deselect();
                        }
                    }
                }
                mProductListingFragment.updateFilters(ProductListingFragment.TYPE_CLEAR_FILTERS);
            }
        }).show();
    }

    /**
     * Called when Accept or Clear Button of Facet List is Pressed
     *
     * @param view Button triggering this method
     */
    public void onFilterButtonsClicked(View view) {
        int id = view.getId();
        //Clear previously selected tab
        if (mProductListingFragment.getView() != null) {
            SlidingTabLayout slidingTabLayout = (SlidingTabLayout) mProductListingFragment.getView().findViewById(R.id.sliding_tabs);
            slidingTabLayout.deselectLastSelected(-1);
        }
        FragmentManager fm = mProductListingFragment.getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FolderFragment fragment = ((FolderFragment) (fm.findFragmentByTag(ProductListingFragment.NAVIGATION_TAG)));
        int response = id == R.id.accept_facet ? 1 : 0;
        if (response == 1) {
            if (fragment != null) {
                mProductListingFragment.updateFilters(ProductListingFragment.TYPE_UPDATE_FILTERS);
            }
        } else {
            clearFilters(view);
        }

        if (fragment != null)
            if (!fragment.isHidden()) {
                ft.setCustomAnimations(R.anim.activity_exit, R.anim.activity_exit);
                ft.hide(fragment);
            }
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (mProductListingFragment != null) {
            FragmentManager fm = mProductListingFragment.getChildFragmentManager();
            Fragment fragment = fm.findFragmentByTag(ProductListingFragment.NAVIGATION_TAG);
            if (fragment != null && !fragment.isHidden()) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.setCustomAnimations(R.anim.activity_exit, R.anim.activity_exit);
                ft.hide(fragment);
                //    ((TextView)productsFragment.getView().findViewById(R.id.tab_title)).setTextColor(getResources().getColor(R.color.tab_text_black));
                mProductListingFragment.selectedTab = -1;
                mProductListingFragment.tabAdapter.notifyDataSetChanged();
                if (mProductListingFragment.getView() != null)
                    mProductListingFragment.getView().findViewById(R.id.narrow_textview).setSelected(false);
                ft.commit();
                SlidingTabLayout slidingTabLayout = (SlidingTabLayout) mProductListingFragment.getView().findViewById(R.id.sliding_tabs);
                slidingTabLayout.deselectLastSelected(-1);
            } else
                exitCheck();
        } else
            exitCheck();
    }

    private void exitCheck() {
        if (isTaskRoot()) {
            if (mNavigationDrawerFragment != null && mNavigationDrawerFragment.getCurrentSelectedPosition() == 0) {
                long currTime = System.currentTimeMillis();
                if (currTime - lastBackClick <= TIME_TO_EXIT_CLICK)
                    super.onBackPressed();
                else {
                    lastBackClick = currTime;
                    Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                }
            } else {
                displayHome();
            }
        } else {
            super.onBackPressed();
            overridePendingTransition(0, R.anim.slide_out_right);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.D)
            ViewServer.get(this).setFocusedWindow(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Utils.D)
            ViewServer.get(this).removeWindow(this);
    }

    /**
     * Callback method for checkBox for Comparison of Products
     *
     * @param view View calling the method
     */
    public void onCompareBoxClicked(View view) {
        if (view != null && view instanceof TextView) {
            CheckBoxLeft check = (CheckBoxLeft) view;
            boolean selected = !check.isSelected();
            check.setChecked(selected);
            mProductListingFragment.productAdapter.onCheckChanged((Integer) check.getTag(), selected);
            mProductListingFragment.updateCompareCount();
        }
    }

    /**
     * A callback method for Button Click Event of Compare Button
     *
     * @param view Compare Button
     */
    public void onCompareButtonClicked(View view) {
        if (view != null && view instanceof Button) {
            ArrayList<Product> tempList = mProductListingFragment.productAdapter.getCompareList();
            launchCompareActivity(tempList);
        }
    }

    /**
     * A callback method for Button Click Event of Empty Compare Button
     *
     * @param view Compare Button
     */
    public void onEmptyButtonClicked(View view) {
        mProductListingFragment.productAdapter.clearCompareList();
        mProductListingFragment.productAdapter.notifyDataSetChanged();
        mProductListingFragment.updateCompareCount();
    }

    /**
     * Method to be called for comparing products
     *
     * @param products ArrayList of products to be compared
     */

    private void launchCompareActivity(ArrayList<Product> products) {
        Intent i = new Intent(this, CompareActivity.class);
        i.putParcelableArrayListExtra(StringUtils.TAG_PRODUCT_LIST, products);
        i.putExtra(StringUtils.KEY_BACK_ANIMATION, R.anim.scale_down_bottom_right);
        //    Bundle b = null;
        //    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN)
        //    {
        //        b = ActivityOptions.makeScaleUpAnimation(view,0,0,view.getWidth(),view.getHeight()).toBundle();
        //    }
        startActivity(i);
        overridePendingTransition(R.anim.scale_up_bottom_right, R.anim.activity_exit);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus)
            getWindow().setBackgroundDrawable(null);
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onCategorySelected(int position) {
        onNavigationDrawerItemSelected(position);
        //Should be pos + 1 due to the non selectable item
        mNavigationDrawerFragment.setCurrentSelectedPosition(position + 1);
    }

    @Override
    public void onBannerSelected(int position) {
        String link = mBannerList.get(position).link;
        t.send(new HitBuilders.EventBuilder("Banner", "Selected").setLabel(link).build());
        if (link.startsWith("/p/"))
            openResult(link.substring(link.lastIndexOf("-") + 1, link.length() - 1));
    }

    @Override
    public void onHomeSearchSelected() {
        super.onSearchRequested();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(getTitle());
        }
    }

    public void restoreBanners() {
        if (mBannerList == null)
            mBannerList = new ArrayList<>();
        ProductDatabaseHelper dh = new ProductDatabaseHelper(this);
        SQLiteDatabase db = dh.getReadableDatabase();
        String[] columns = {ProductDatabaseHelper.KEY_NAME,
                ProductDatabaseHelper.KEY_IMAGE,
                ProductDatabaseHelper.KEY_URL,
                ProductDatabaseHelper.KEY_RANK
        };
        Cursor c = db.query(ProductDatabaseHelper.BANNER_TABLE_NAME, columns, ProductDatabaseHelper.KEY_LOCATION + "=?"
                , new String[]{HOME_LOCATION}, null, null, ProductDatabaseHelper.KEY_RANK + " ASC");
        while (c.moveToNext()) {
            mBannerList.add(Banner.createFromCursor(c));
        }
        c.close();
    }

    public void onBannerLoadingComplete() {
        if (mHomeFragment != null && mHomeFragment.isAdded()) {
            if (mBannerList.size() == 0) {
                mHomeFragment.hideBanners();
            } else {
                mHomeFragment.refreshBanners();
                mHomeFragment.showBanners();
            }
        }

    }

    /*public ImageLoader1 getImageLoader() {
        if (mImageLoader == null)
            mImageLoader = new ImageLoader1(this, R.drawable.iq_logo_grey, getMemoryCache());
        if (mMemoryCache == null)
            return null;
        return mImageLoader;
    }*/

    @Override
    protected void onPause() {
        super.onPause();
    }
}
