package com.buyingiq.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.buyingiq.app.entities.Facet;
import com.buyingiq.app.entities.Folder;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.listadapters.ProductListAdapter;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.Utils;
import com.buyingiq.app.widget.SlidingTabLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.buyingiq.app.ProductListingActivity.sortProductCategories;
import static com.buyingiq.app.ProductListingActivity.sortProductTags;
import static com.buyingiq.app.resources.StringUtils.TAG_FACET_LIST;
import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCT;
import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCT_LIST;
import static com.buyingiq.app.resources.StringUtils.TAG_TITLE;
import static com.buyingiq.app.resources.StringUtils.TAG_TOTAL;

public class ProductListingFragment extends ListFragment implements
        Folder.OnSelectedChangeListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener,
        AdapterView.OnItemClickListener {

    public static final String TAG = "ProductListingFragment";
    public static final String NAVIGATION_TAG = "navigation_fragment";
    //TODO find a solution to when the app comes back from other app and crashes sometimes.
    public View.OnClickListener narrowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            //    ft.setCustomAnimations(R.anim.slide_in_top, R.anim.slide_in_top);
            Fragment fragment = fm.findFragmentByTag(NAVIGATION_TAG);
            if (!view.isSelected()) {
                ft.show(fragment);
                view.setSelected(true);
            } else {
                view.setSelected(false);
                ft.setCustomAnimations(R.anim.activity_exit, R.anim.activity_exit);
                ft.hide(fragment);
            }
            ft.commit();
        }
    };
    public final static int TYPE_CLEAR_FILTERS = 0;
    public final static int TYPE_UPDATE_FILTERS = 1;
    public final static int TYPE_RETAIN_FILTERS = 2;
    private static final String PREF_USER_LEARNED_TOUCH = "user_touch";
    private static final String ARG_NAMES = "names";
    private static final String ARG_VALUES = "values";
    private static final String ARG_SEARCH = "search";
    private static final String ARG_TITLE = "title";
    //These indexes should be updated with every change in compare layout of Product Listing page
    private static final int COMPARE_TEXT_INDEX = 0;
    private static final int COMPARE_BUTTON_INDEX = 1;
    public static String apiUrl = "search/";
    //    private String prev;
    public String next;
    public ArrayList<Product> mProductList;
    public ArrayList<Folder> mFolderList;
    public boolean updating = false;
    public ViewGroup compareView;
    public int selectedTab = -1;
    public OnTabSelectedListener mListener;
    public SlidingTabLayout mSlidingTabLayout;
    public int mCurrentSortIndex = 0;
    public boolean mUserLearnedTouch = false;
    public BaseAdapter tabAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return mFolderList.size();
        }

        @Override
        public Object getItem(int position) {
            return mFolderList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                convertView = inflater.inflate(R.layout.item_tab, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView;
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            Folder f = mFolderList.get(position);
            viewHolder.textView.setText(f.name.toUpperCase());
            return convertView;
        }

        class ViewHolder {
            TextView textView;
        }
    };
    public ListView listView;
    public TextView footerView;
    protected long totalResults = 0;
    protected String mCurrentCategory;
    protected String mCurrentQuery;
    protected String mTitle = "";
    protected ProductListAdapter productAdapter;
    Tracker t;
    Fragment fragment;
    ArrayList<ProductRequestTask> tasks;
    private boolean isSearch = false;
    //    private int mCurrentSelection = -1;
    private View mCurrentSelectionProduct;
    private String[] names;
    private String[] values;
    private int selectedCount;

    public ProductListingFragment() {
    }

    public static ProductListingFragment newInstance(String[] names, String[] values, boolean isSearch, String title) {
        ProductListingFragment fragment = new ProductListingFragment();
        if (names != null) {
            Bundle b = new Bundle();
            b.putStringArray(ARG_NAMES, names);
            b.putStringArray(ARG_VALUES, values);
            b.putBoolean(ARG_SEARCH, isSearch);
            b.putString(ARG_TITLE, title);
            fragment.setArguments(b);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        tasks = new ArrayList<>();
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        t.setScreenName(TAG);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedTouch = sp.getBoolean(PREF_USER_LEARNED_TOUCH, false);
//        mUserLearnedTouch = false;
        if (savedInstanceState != null) {
            mProductList = savedInstanceState.getParcelableArrayList(TAG_PRODUCT_LIST);
            mFolderList = savedInstanceState.getParcelableArrayList(TAG_FACET_LIST);
            totalResults = savedInstanceState.getLong(TAG_TOTAL);
            mTitle = savedInstanceState.getString(TAG_TITLE);
            names = savedInstanceState.getStringArray(ARG_NAMES);
            values = savedInstanceState.getStringArray(ARG_VALUES);
            isSearch = savedInstanceState.getBoolean(ARG_SEARCH);
        } else {
            if (getArguments() != null) {
                Bundle b = getArguments();
                names = b.getStringArray(ARG_NAMES);
                values = b.getStringArray(ARG_VALUES);
                isSearch = b.getBoolean(ARG_SEARCH);
                mTitle = b.getString(ARG_TITLE);
            }
            mProductList = new ArrayList<>();
            mFolderList = new ArrayList<>();
        }
        if (isSearch) {
            sortProductCategories[0] = "Relevance";
            sortProductTags[0] = "relevence-desc";
        } else {
            sortProductCategories[0] = "Popularity";
            sortProductTags[0] = "popularity-desc";
        }
        restoreActionBar();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(TAG_PRODUCT_LIST, mProductList);
        outState.putParcelableArrayList(TAG_FACET_LIST, mFolderList);
        outState.putLong(TAG_TOTAL, totalResults);
        outState.putString(TAG_TITLE, mTitle);
        outState.putStringArray(ARG_NAMES, names);
        outState.putStringArray(ARG_VALUES, values);
        outState.putBoolean(ARG_SEARCH, isSearch);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_listing, container, false);
        if (rootView != null) {
            //    TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //    textView.setText(mTitle);
            if (names[0].equals("q")) {
                mCurrentQuery = values[0];
                if (names[1].equals("tags")) {
                    mCurrentCategory = values[1];
                    t.send(new HitBuilders.EventBuilder().setCategory("Product Listing").setAction("Narrow By Category").setLabel(mCurrentQuery + ":" + mCurrentCategory).setValue(1).build());
                } else
                    t.send(new HitBuilders.EventBuilder().setCategory("Product Listing").setAction("Search").setLabel(mCurrentQuery).setValue(1).build());
                //        textView.setCompoundDrawablePadding(5);
                //        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search_half, 0, 0, 0);
            } else {
                mCurrentCategory = values[0];
                t.send(new HitBuilders.EventBuilder().setCategory("Product Listing").setAction("List").setLabel(mCurrentCategory).setValue(1).build());
            }
            if (!mUserLearnedTouch)
                rootView.findViewById(R.id.touch_tutorial_layout).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setVisibility(View.GONE);
                        touchLearned();
                        v.setOnClickListener(null);
                    }
                });
            compareView = (RelativeLayout) rootView.findViewById(R.id.product_compare_layout);
            rootView.findViewById(R.id.product_sort_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSortButtonClicked();
                }
            });
        }
        return rootView;
    }

    private void touchLearned() {
        if (!mUserLearnedTouch) {
            // The user manually opened the drawer; store this flag to prevent auto-showing
            // the navigation drawer automatically in the future.
            mUserLearnedTouch = true;
            SharedPreferences sp = PreferenceManager
                    .getDefaultSharedPreferences(getActivity());
            sp.edit().putBoolean(PREF_USER_LEARNED_TOUCH, true).apply();
        }
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        listView = getListView();
        productAdapter = new ProductListAdapter(getActivity(), mProductList, isSearch);
        //Crashlytics.setBool("is_search", isSearch);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        footerView = (TextView) inflater.inflate(R.layout.overscroll_footer, listView, false);
        footerView.setVisibility(View.INVISIBLE);
        listView.setDivider(null);
        listView.addFooterView(footerView, "footer", false);
        listView.setAdapter(productAdapter);
        listView.setOnScrollListener(this);
        listView.setOnItemClickListener(this);
        setUp(names, values, isSearch);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActivity().getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle.toUpperCase());
        getActivity().setTitle(mTitle.toUpperCase());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCurrentSelectionProduct != null) {
            mCurrentSelectionProduct.setSelected(false);
        }
    }

    /**
     * Method called whenever the view needs to be changed. Currently used for Sort, or Filter change
     *
     * @param pairs Pair<String,String>s to be used when updating the layout.
     */
    public void updateView(ArrayList<Pair<String,String>> pairs, int type) {
        footerView.setVisibility(View.INVISIBLE);
        listView.setDivider(null);
        mProductList.clear();
        productAdapter.notifyDataSetChanged();
        productAdapter.clearCompareList();
        updating = true;
        int flag = isSearch ? ProductRequestTask.FLAG_SEARCH_ACTIVE : 0;
        if (type != TYPE_RETAIN_FILTERS)
            flag |= ProductRequestTask.FLAG_LOAD_FACET;
        ProductRequestTask asyncTask = new ProductRequestTask(getActivity(), this, flag);
        asyncTask.execute(pairs);
        tasks.add(asyncTask);
        getListView().setBackgroundColor(getResources().getColor(R.color.white));
        updateCompareCount();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnTabSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTabSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onSelectedCountChanged(int count) {
        selectedCount += count;
        if (fragment != null)
            ((FolderFragment) fragment).updateCount(selectedCount);
    }

    public void resetSelectedCount() {
        selectedCount = 0;
    }

    @Override
    public void onRefresh() {

    }

    public void setUp(String[] names, String[] values, boolean search) {
        ArrayList<Pair<String,String>> list = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            if (values[i].contains("/")) {
                String[] temp = values[i].split("/");
                list.add(new Pair<>(names[i], temp[0]));
                for (int j = 1; j < temp.length; j++)
                    list.add(new Pair<>("tags", temp[j]));
            } else
                list.add(new Pair<>(names[i], values[i]));
        }
        int flags = ProductRequestTask.FLAG_LOAD_FACET;
        flags |= search ? ProductRequestTask.FLAG_SEARCH_ACTIVE : 0;
        ProductRequestTask asyncTask = new ProductRequestTask(getActivity(), this, flags);
        asyncTask.execute(list);
        tasks.add(asyncTask);
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (search)
            fragment = new NarrowFragment();
        else
            fragment = FolderFragment.newInstance(mFolderList, selectedCount);
        ft.replace(R.id.navigation_fragment_container, fragment, NAVIGATION_TAG);
        if (!fragment.isHidden())
            ft.hide(fragment);
        ft.commit();
    }

    public void displayProducts() {
        productAdapter.notifyDataSetChanged();
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int first, int visible, int total) {
        if (first + visible >= total && total != 0) {
            if (next != null)
                if (next.length() > 1) {
                    if (!updating) {
                        updating = true;
                        ProductRequestTask asyncTask = new ProductRequestTask(getActivity(), this, ProductRequestTask.FLAG_SCROLL_MODE);
                        asyncTask.execute();
                        tasks.add(asyncTask);
                    }
                }
        }/*
        else if(first==visible&& prevList.size()>0)
        {
            mProductList.add(0, prevList.get(prevList.size()-1));
            prevList.remove(prevList.size()-1);
            nextList.add(mProductList.get(mProductList.size()-1));
            mProductList.remove(mProductList.size()-1);
            displayProducts();
        }*/
    }

    private void selectProduct(int position) {
        //    mCurrentSelection = position;
        Intent intent = new Intent(getActivity().getApplicationContext(), ProductActivity.class);
        intent.putExtra(TAG_PRODUCT, mProductList.get(position));
        intent.putExtra(ProductDatabaseHelper.KEY_VISITED_FROM, TAG);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    /**
     * Called when an update of Product List is requested.
     * This method might be triggered by change in Filter Values or Sort parameter
     *
     * @param pairs The name value pairs to replace the current query with,
     *                       this contains all the selected tags for currents query.
     */
    public void replaceValues(ArrayList<Pair<String,String>> pairs) {
        names = new String[pairs.size()];
        values = new String[pairs.size()];
        for (int i = 0; i < pairs.size(); i++) {
            Pair nvp = pairs.get(i);
            names[i] = (String) nvp.first;
            values[i] = (String) nvp.second;
        }
    }

    public void showCompareView() {
        Utils.showWithAnimation(getActivity(), compareView, R.anim.slide_in_bottom);
    }

    public void hideCompareView() {
        Utils.hideWithAnimation(getActivity(), compareView, R.anim.slide_out_bottom);
    }

    /**
     * Called when product selected for comparison is changed. Also calls {@link #enableCompareButton(boolean)}
     */
    public void updateCompareCount() {
        int count = productAdapter.compareList.size();
        if (compareView == null)
            return;
        if (count == 0)
            hideCompareView();
        if (count == 1)
            showCompareView();
        TextView compareTextView = (TextView) compareView.getChildAt(COMPARE_TEXT_INDEX);
        Spannable compareText = new SpannableString(mTitle + " (" + count + ")");
        compareTextView.setText(compareText);
        enableCompareButton(count > 1);
    }

    /**
     * Call this method to enable of disable Compare Button present
     * in Compare Layout of Product Listing Layout
     *
     * @param enable boolean value to enable of disable the Compare Button
     */
    public void enableCompareButton(boolean enable) {
        if (compareView == null)
            return;
        compareView.getChildAt(COMPARE_BUTTON_INDEX).setEnabled(enable);
    }

    /**
     * Method called when user changes a sort parameter using the Sort button.
     *
     * @param sortIndex Index of selected sort value
     */
    public void sort(int sortIndex) {
        if (sortIndex == mCurrentSortIndex)
            return;
        mCurrentSortIndex = sortIndex;
        updateFilters(TYPE_RETAIN_FILTERS);
    }

    /**
     * Method called when Filters are selected or cleared.
     */
    public void updateFilters(int type) {
        ArrayList<Pair<String,String>> pairs = new ArrayList<>();
        if (mCurrentQuery != null)
            pairs.add(new Pair<>("q", mCurrentQuery));
        if (type != TYPE_CLEAR_FILTERS) {
            for (int i = 0; i < mFolderList.size(); i++) {
                Folder fo = mFolderList.get(i);
                for (Facet f : fo.facets) {
                    if (f.isSelected() == 1) {
                        pairs.add(new Pair<>("tags", f.tag));
                    }
                }
            }
        }
        pairs.add(new Pair<>("tags", mCurrentCategory));
        pairs.add(new Pair<>("tags", sortProductTags[mCurrentSortIndex]));
        if (type != TYPE_RETAIN_FILTERS)
            pairs.add(new Pair<>("facet", "1"));
        pairs.add(new Pair<>("page", "1"));
        updateView(pairs, type);
        selectedTab = -1;
        tabAdapter.notifyDataSetChanged();
    }

    public void stopTasks() {
        if (tasks == null)
            return;
        for (ProductRequestTask a : tasks) {
            a.cancel(true);
        }

    }

    @Override
    public void onDestroyView() {
        stopTasks();
        super.onDestroyView();
    }

    /**
     * Method to be called when sort button is clicked
     */
    public void onSortButtonClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("SORT");
        BaseAdapter arrayAdapter = new DialogAdapter(getActivity(), R.layout.item_product_sort, sortProductCategories);
        builder.setSingleChoiceItems(arrayAdapter, mCurrentSortIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sort(i);
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        selectProduct(i);
        if (mCurrentSelectionProduct != null)
            mCurrentSelectionProduct.setSelected(false);
        mCurrentSelectionProduct = view;
        mCurrentSelectionProduct.setSelected(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public interface OnTabSelectedListener {
        void onTabSelected2(int currentSelection, int lastSelection);
    }

    class DialogAdapter extends BaseAdapter {

        Context mContext;
        List<String> strings;
        int mResource;

        DialogAdapter(Context context, int resource, String[] list) {
            this(context, resource, Arrays.asList(list));
        }

        DialogAdapter(Context context, int resource, List<String> list) {
            mContext = context;
            strings = list;
            mResource = resource;
        }

        @Override
        public int getCount() {
            return strings.size();
        }

        @Override
        public String getItem(int i) {
            return strings.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                view = inflater.inflate(mResource, viewGroup, false);
            }
            CheckedTextView ctv = (CheckedTextView) view;
            ctv.setChecked(i == (mCurrentSortIndex == -1 ? 0 : mCurrentSortIndex));
            ctv.setText(strings.get(i));
            return view;
        }
    }
}