package com.buyingiq.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.Categories;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.widget.IQGraphView;


/**
 *
 */
public class ProductOverviewFragment extends Fragment implements View.OnClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PRODUCT = "product";

    private Product mProduct;
    private ImageLoader1 imageLoader1;
    private OnFragmentInteractionListener mListener;

    public ProductOverviewFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param product Product to be displayed
     * @return A new instance of fragment ProductOverviewFragment.
     */
    public static ProductOverviewFragment newInstance(Product product) {
        ProductOverviewFragment fragment = new ProductOverviewFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
        }
        int i = ((BuyingIQApp) getActivity().getApplication()).findCategoryID(mProduct.categoryID);
        imageLoader1 = new ImageLoader1(getActivity(), Categories.catImgs[i]);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.full_spec_head:
                mListener.onFullSpecificationSelected();
                break;
            case R.id.product_price:
                mListener.onComparePriceSelected();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_product_overview, container, false);
        convertView.findViewById(R.id.full_spec_head).setOnClickListener(this);
        ViewHolder vh = getViewHolder(convertView);
        setOverView(vh, getActivity());
        addKeyFeatures(vh, getActivity());
        return convertView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
/*        Fragment specFrag = ProductSpecificationsFragment.newInstance(mProduct);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container,specFrag).commit();*/
    }

    private ViewHolder getViewHolder(View convertView) {
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.image = (ImageView) convertView.findViewById(R.id.product_image);
        viewHolder.rating = (TextView) convertView.findViewById(R.id.product_rating);
        viewHolder.price = (TextView) convertView.findViewById(R.id.product_price);
        viewHolder.stores = (TextView) convertView.findViewById(R.id.product_stores);
        viewHolder.votes = (TextView) convertView.findViewById(R.id.product_votes);
        viewHolder.name = (TextView) convertView.findViewById(R.id.product_name);
        viewHolder.featureGroup = (ViewGroup) convertView.findViewById(R.id.key_feat_layout);
        viewHolder.biqScore = (TextView) convertView.findViewById(R.id.biq_score);
        viewHolder.biqParent = convertView.findViewById(R.id.biq_score_layout);
        viewHolder.biqChoice = (TextView) convertView.findViewById(R.id.biq_choice);
        viewHolder.biqGraph = convertView.findViewById(R.id.biq_score_graph);
        viewHolder.biqStat = (TextView) convertView.findViewById(R.id.biq_stat);
        viewHolder.meanText = (TextView) convertView.findViewById(R.id.mean_text);
        viewHolder.percText = (TextView) convertView.findViewById(R.id.percent_text);
        //    convertView.setTag(viewHolder);
        return viewHolder;
    }

    private void setOverView(ViewHolder viewHolder, Context context) {
        viewHolder.name.setText(mProduct.name);
        imageLoader1.DisplayImage(mProduct.getOverviewImage(), viewHolder.image);
        viewHolder.votes.setText(mProduct.votes);
        String foot = "/10";
        String text = mProduct.rating;
        Spannable styledText = new SpannableString(text + foot);
        TextAppearanceSpan span = new TextAppearanceSpan(context, R.style.textTotalRating);
        styledText.setSpan(new SuperscriptSpan(), text.length(), text.length() + foot.length(), Spanned.SPAN_COMPOSING);
        styledText.setSpan(span, text.length(), text.length() + foot.length(), Spanned.SPAN_COMPOSING);
        viewHolder.rating.setText(styledText);
        viewHolder.price.setText(mProduct.price);
        viewHolder.price.setOnClickListener(this);
        viewHolder.stores.setText(mProduct.numStores);
        if (mProduct.biqScore > 0) {
            ((IQGraphView) viewHolder.biqGraph).setIqScore(mProduct.biqScore);
            setBiqScore(getResources(), viewHolder, mProduct.biqScore);
            viewHolder.biqParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleIQGraph(v.findViewById(R.id.toggle_iq_detail_button));
                }
            });
            viewHolder.biqStat.setText(getResources().getString(R.string.iq_based, mProduct.curatedReviewCount));
            viewHolder.meanText.setText("WHAT DOES A SCORE OF " + mProduct.biqScore + " MEAN?");
        } else {
            viewHolder.biqParent.setVisibility(View.GONE);
        }
        viewHolder.recycle();

    }

    private void addKeyFeatures(ViewHolder viewHolder, Context mContext) {
        if (mProduct.keyFeatures.size() > 0) {
            for (String s : mProduct.keyFeatures) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                TextView robotoTextView = (TextView) inflater.inflate(R.layout.item_key_features, viewHolder.featureGroup, false);
                robotoTextView.setText(s);
                viewHolder.featureGroup.addView(robotoTextView);
            }
            View emptyView = new View(mContext);
            emptyView.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (10 * mContext.getResources().getDisplayMetrics().density)));
            viewHolder.featureGroup.addView(emptyView);
            /*View divider = new View(mContext);
            divider.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            divider.setBackgroundResource(R.drawable.horizontal_divider);
            //     viewHolder.mainLayout.addView(emptyView);
            viewHolder.featureGroup.addView(divider);*/
        } else {
            viewHolder.featureGroup.setVisibility(View.GONE);
        }
        /*RobotoTextView specTextView = new RobotoTextView(mContext);
        specTextView.setTypeFaceRoboto(RobotoTextView.ROBOTO_BOLD);
        specTextView.setText("FULL SPECIFICATIONS");
    //    specTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.title_head_font_size));
        specTextView.setTextColor(getResources().getColor(R.color.text_dark_grey));
        specTextView.setPadding(0, 20, 0, 5);
    //    specTextView.setOnClickListener(this);
        viewHolder.featureGroup.addView(specTextView);*/
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setBiqScore(Resources res, ViewHolder viewHolder, int score) {
        Drawable bg;
        int iqScoreColor;
        String choiceText;
        String statText;
        if (score >= 0 && score < 50) {
            iqScoreColor = R.color.iq_score1;
            choiceText = res.getString(R.string.iq_choice1);
            statText = res.getString(R.string.iq_stat1);
        } else if (score >= 50 && score < 80) {
            iqScoreColor = R.color.iq_score2;
            choiceText = res.getString(R.string.iq_choice2);
            statText = res.getString(R.string.iq_stat2);
        } else if (score >= 80 && score < 90) {
            iqScoreColor = R.color.iq_score3;
            choiceText = res.getString(R.string.iq_choice3);
            statText = res.getString(R.string.iq_stat3, "%", mProduct.category.toLowerCase());
        } else {
            iqScoreColor = R.color.iq_score4;
            choiceText = res.getString(R.string.iq_choice4);
            statText = res.getString(R.string.iq_stat4, "%", mProduct.category.toLowerCase());
        }
        viewHolder.biqScore.setText("" + score);
        bg = getTintedDrawable(res, R.drawable.iq_score_logo, iqScoreColor);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            viewHolder.biqScore.setBackgroundDrawable(bg);
        } else {
            viewHolder.biqScore.setBackground(bg);
        }
        viewHolder.biqChoice.setText(choiceText);
        viewHolder.percText.setText(statText);
    }

/*    public void changeCaretPos(int score, View caret, View imageView) {
        int width = imageView.getWidth();
        float x = width * (score / 100.0f);
        caret.setX(x);
    }*/

    public Drawable getTintedDrawable(@NonNull Resources res,
                                      @DrawableRes int drawableResId, @ColorRes int colorResId) {
        Drawable drawable = res.getDrawable(drawableResId);
        int color = res.getColor(colorResId);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public void toggleIQGraph(@Nullable View toggleButton) {
        View view = getView();
        if (view == null)
            return;
        final View iqGraph = view.findViewById(R.id.biq_score_graph_layout);
        if (iqGraph.getVisibility() != View.VISIBLE) {
            iqGraph.setVisibility(View.VISIBLE);
            if (toggleButton != null)
                ((ImageView) toggleButton).setImageResource(R.drawable.ic_minus_gray);
        } else {
            if (toggleButton != null)
                ((ImageView) toggleButton).setImageResource(R.drawable.ic_plus_gray);
            iqGraph.setVisibility(View.GONE);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFullSpecificationSelected();

        void onComparePriceSelected();
    }

    private static class ViewHolder {
        ImageView image;
        TextView rating;
        TextView price;
        TextView stores;
        TextView votes;
        TextView name;
        TextView biqScore;
        ViewGroup featureGroup;
        View biqParent;
        TextView biqChoice;
        TextView biqStat;
        View biqGraph;
        TextView meanText;
        TextView percText;

        void recycle() {
            image = null;
            rating = null;
            price = null;
            stores = null;
            votes = null;
            name = null;
            biqChoice = null;
            biqScore = null;
            biqStat = null;
            biqParent = null;
            biqGraph = null;
            meanText = null;
            percText = null;
        }
    }

}
