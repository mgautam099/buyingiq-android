package com.buyingiq.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.buyingiq.app.entities.Deal;
import com.buyingiq.app.entities.Store;
import com.buyingiq.app.entities.StoreDeals;
import com.buyingiq.app.listadapters.StoreListAdapter;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.buyingiq.app.resources.StringUtils.ARG_PRODUCT_ID;
import static com.buyingiq.app.resources.StringUtils.TAG_DEALS;
import static com.buyingiq.app.resources.StringUtils.TAG_IN_STOCK;
import static com.buyingiq.app.resources.StringUtils.TAG_MIN_PRICE_STR;
import static com.buyingiq.app.resources.StringUtils.TAG_SPONSORED;
import static com.buyingiq.app.resources.StringUtils.TAG_STORE;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the
 * {@link com.buyingiq.app.ProductPricesFragment.OnFragmentInteractionListener}
 * interface.
 */
public class ProductPricesFragment extends ListFragment implements AbsListView.OnItemClickListener {

    private static final String KEY_DEAL_LIST = "dealsList";
    private static final String TAG = "ProductPricesFragment";
    public static String apiUrl;
    DealsTask dealsTask;
    Tracker t;
    private String mProductID;
    private ArrayList<StoreDeals> mDealList;
    private OnFragmentInteractionListener mListener;
    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private StoreListAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductPricesFragment() {
    }

    public static ProductPricesFragment newInstance(String productID) {
        ProductPricesFragment fragment = new ProductPricesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCT_ID, productID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mProductID = getArguments().getString(ARG_PRODUCT_ID);
            apiUrl = String.format(StringUtils.BASE_URL + "products/%s/deals/", mProductID);
        }
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        mDealList = new ArrayList<>();
        mAdapter = new StoreListAdapter(getActivity(), mDealList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_prices, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Set the adapter
        setListAdapter(mAdapter);
        refreshList();
        // Set OnItemClickListener so we can be notified on item clicks
        getListView().setOnItemClickListener(this);
        getListView().getEmptyView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });
    }

    private void refreshList() {
        dealsTask = new DealsTask(getActivity());
        dealsTask.execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        dealsTask.cancel(true);
        super.onDestroyView();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onStoreSelected(mDealList.get(position));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnFragmentInteractionListener {
        public void onStoreSelected(StoreDeals deals);
    }

    private class DealsTask extends BaseSyncTask<Void> {

        DealsTask(Activity context) {
            super(context, apiUrl, 0);
            super.setListView(getListView());
            super.setProgressBar(getView().findViewById(android.R.id.progress));
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer returnCode = super.doInBackground(voids);
            if (returnCode == 0) {
                try {
                    JSONArray allDeals = new JSONArray(json);
                    for (int i = 0; i < allDeals.length(); i++) {
                        if (isCancelled())
                            return OTHER_ERROR;
                        JSONObject dealObject = allDeals.getJSONObject(i);
                        JSONObject storeObject = dealObject.getJSONObject(TAG_STORE);
                        StoreDeals storeDeals = new StoreDeals(Store.createFromJson(storeObject));
                        storeDeals.setInStock(dealObject.getBoolean(TAG_IN_STOCK));
                        storeDeals.setMinPrice(dealObject.getString(TAG_MIN_PRICE_STR));
                        storeDeals.setSponsored(dealObject.getBoolean(TAG_SPONSORED));
                        JSONArray dealArray = dealObject.getJSONArray(TAG_DEALS);
                        readDeals(dealArray, storeDeals);
                        mDealList.add(storeDeals);
                    }
                } catch (JSONException e) {
                    Utils.Print.e(TAG, "JSONException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "IOException", url + ":" + e.getMessage());
                    return OTHER_ERROR;
                } catch (Exception e) {
                    Utils.Print.e(TAG, "Exception: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "Exception", url + ":" + e.getMessage());
                    return OTHER_ERROR;
                }
            }
            return returnCode;
        }


        private void readDeals(JSONArray dealArray, StoreDeals storeDeals) throws JSONException {
            for (int i = 0; i < dealArray.length(); i++) {
                JSONObject dealObject = dealArray.getJSONObject(i);
                storeDeals.addDeal(Deal.createFromJson(dealObject));
            }
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            super.onPostExecute(returnValue);
            if (returnValue == 0) {
                if (mDealList.size() < 0) {
                    setEmptyText("No Deals Available");
                    listView.getEmptyView().setOnClickListener(null);
                } else
                    mAdapter.notifyDataSetChanged();
            }
        }
    }


}
