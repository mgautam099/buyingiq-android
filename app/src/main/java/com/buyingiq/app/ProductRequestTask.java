package com.buyingiq.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Pair;
import android.view.View;

import com.buyingiq.app.entities.Facet;
import com.buyingiq.app.entities.Folder;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.buyingiq.app.widget.SlidingTabLayout;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.buyingiq.app.resources.StringUtils.TAG_FOLDERS;
import static com.buyingiq.app.resources.StringUtils.TAG_NEXT;
import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCTS;
import static com.buyingiq.app.resources.StringUtils.TAG_SELECTED_FACETS;
import static com.buyingiq.app.resources.StringUtils.TAG_SELECTED_TAGS;
import static com.buyingiq.app.resources.StringUtils.TAG_TOTAL;

/**
 * Asynctask used to get and parse product list
 */
class ProductRequestTask extends AsyncTask<ArrayList<Pair<String,String>>, Void, String> {


    final static int FLAG_SEARCH_ACTIVE = 1;
    final static int FLAG_SCROLL_MODE = 1 << 1;
    final static int FLAG_LOAD_FACET = 1 << 2;
    private final static int FLAG_NETWORK_AVAILABLE = 1 << 3;
    private static final String TAG = "ProductRequestTask";
    //Map for storing search labels with their count
    private Map<String, Integer> searchLabels;
    //ArrayList for the format of labels as %label_name (%count)
    private ArrayList<String> labels;
    private ArrayList<String> tags;
    private ArrayList<Product> productsLocalCopy;
    private ProductListingFragment mFragment;
    private NetworkUtils networkUtils;
    Activity mContext;
    Tracker t;
    String url;
    private int selectedCount;
    private int mFlags;

    ProductRequestTask(Activity context, ProductListingFragment fragment, int flags) {
        mFlags = flags;
        if (NetworkUtils.isNetworkConnected(context))
            mFlags |= FLAG_NETWORK_AVAILABLE;
        mContext = context;
        mFragment = fragment;
        t = ((BuyingIQApp) context.getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        url = StringUtils.BASE_URL + ProductListingFragment.apiUrl;

    }

    @Override
    protected void onPreExecute() {
        if (isCancelled())
            return;
        super.onPreExecute();
        mFragment.productAdapter.notifyDataSetChanged();
        if ((mFlags & FLAG_SCROLL_MODE) == 0)
            mFragment.getView().findViewById(android.R.id.progress).setVisibility(View.VISIBLE);
        clearFolderList();
//            getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    protected String doInBackground(ArrayList<Pair<String,String>>... args) {
        String toReturn = "";
        String results;
        if ((mFlags & FLAG_NETWORK_AVAILABLE) != 0) {
            searchLabels = new LinkedHashMap<>();
            if (isCancelled())
                return null;
            networkUtils = new NetworkUtils(t);
            String json;
            if ((mFlags & FLAG_SCROLL_MODE) != 0) {
                //If next page is to be loaded.
                url += "?" + mFragment.next;
                json = networkUtils.makeServiceCall(url, NetworkUtils.GET);
            } else {
                mFragment.replaceValues(args[0]);
                json = networkUtils.makeServiceCall(url, NetworkUtils.GET, args[0]);
            }
            if (!json.contains("ERROR")) {
                try {
                    if (isCancelled()) return null;
                    //    parseJSON(json);
                    parseJackSon(json);
                    populateSearchList();
                    if (isCancelled()) return null;
                } catch (IOException e) {
                    Utils.Print.e(TAG, "IOException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "IOException", url + ":" + e.getMessage());

                    //                } catch (JSONException e) {
                    //                    Log.e("MainActivity", "JSONException: " + e.getMessage());
                }

                results = mFragment.mTitle + " (" + mFragment.totalResults + " Results)";
            } else {
                if (mFragment.mProductList.size() == 0) {
                    results = json;
                    toReturn = null;
                } else {
                    toReturn = "restore";
                    results = mFragment.mTitle + " (" + mFragment.totalResults + " Results)";
                }
            }
            return toReturn;
        } else {
            results = "Loading failed, Network Not Found";
            return null;
        }
    }

    /**
     * This list populates the categories available for filtering after the search has been completed.
     */
    private void populateSearchList() {
        //Ignores the method call if the current list is not a search.
        if ((mFlags & FLAG_SEARCH_ACTIVE) != 0) {
            labels = new ArrayList<>();
            tags = new ArrayList<>();
            ArrayList<Facet> facetList = mFragment.mFolderList.get(0).facets;
            for (Facet f : facetList) {
                String buff = String.format("%s (%s)", f.label, f.count);
                labels.add(buff);
                tags.add(f.tag);
            }
        }
    }

    /**
     * Clears the Filter list if Load Facet flag is selected.
     * Else leaves the first element intact and clears the rest.
     */
    private void clearFolderList() {
        if ((mFlags & FLAG_SCROLL_MODE) != 0)
            return;
        if ((mFlags & FLAG_LOAD_FACET) != 0) {
            mFragment.mFolderList.clear();
            mFragment.resetSelectedCount();
            if (mFragment.mSlidingTabLayout != null)
                mFragment.mSlidingTabLayout.setVisibility(View.GONE);
        }/*
         else {
            for (int j = mFragment.mFolderList.size() - 1; j > 0; j--) {
                mFragment.mFolderList.remove(j);
            }
        }*/
        //To prevent the state changed exception of View Pager.
        if (mFragment.fragment instanceof FolderFragment) {
            ((FolderFragment) mFragment.fragment).notifyChanged();
        }
    }

    @Override
    protected void onCancelled() {
        if (networkUtils != null)
            networkUtils.stopOperation();
        //    mFragment.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onCancelled(String s) {
        if (networkUtils != null)
            networkUtils.stopOperation();
        //    mFragment.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            View fragmentView = mFragment.getView();
            if (result != null) {
                //    fragmentView.findViewById(R.id.tab_layout).setVisibility(View.VISIBLE);
                if ((mFlags & FLAG_SCROLL_MODE) == 0) {
                    if ((mFlags & FLAG_SEARCH_ACTIVE) == 0) {
                        //Load the filters if search is inactive.
                        if (mFragment.mSlidingTabLayout == null) {
                            mFragment.mSlidingTabLayout = (SlidingTabLayout) fragmentView.findViewById(R.id.sliding_tabs);
                            mFragment.mSlidingTabLayout.setOnTabSelectedListener(mFragment.mListener);
                        }
                        mFragment.mSlidingTabLayout.setVisibility(View.VISIBLE);
                        fragmentView.findViewById(R.id.narrow_textview).setVisibility(View.GONE);
                        //    setTabs();
                        ((FolderFragment) mFragment.fragment).setUp(mFragment.mSlidingTabLayout, mFragment.mFolderList);
                        mFragment.onSelectedCountChanged(selectedCount);
                    } else {
                        //Show the Narrow by Category if the Search is Active
                        View n = fragmentView.findViewById(R.id.narrow_textview);
                        n.setVisibility(View.VISIBLE);
                        n.setOnClickListener(mFragment.narrowClickListener);
                        if (mFragment.mSlidingTabLayout != null)
                            mFragment.mSlidingTabLayout.setVisibility(View.GONE);
                        ((NarrowFragment) mFragment.fragment).setUp(labels, tags);
                    }
                }
                if (productsLocalCopy != null) {
                    mFragment.mProductList.addAll(productsLocalCopy);
                    mFragment.productAdapter.notifyDataSetChanged();
                }
                if (mFragment.next.length() > 1) {
                    mFragment.footerView.setText("Loading More Results...");
                    mFragment.getListView().setBackgroundResource(0);
                } else {
                    if (mFragment.mProductList.size() == 0)
                        mFragment.footerView.setText("No Results");
                    else {
                        mFragment.footerView.setText("No More Results");
                    }
                    int listheight = mFragment.getListView().getHeight();
                    int footerViewHeight = mFragment.footerView.getHeight();
                    int elementsHeight = (int) (mFragment.mProductList.size() * (120 * mContext.getResources().getDisplayMetrics().density) + footerViewHeight);
                    if (elementsHeight > listheight)
                        mFragment.getListView().setBackgroundResource(0);
                }
                if (mFragment.getView() != null) {
                    if (!mFragment.mUserLearnedTouch)
                        mFragment.getView().findViewById(R.id.touch_tutorial_layout).setVisibility(View.VISIBLE);
                }
            } else {
                StringBuffer s = new StringBuffer("Unable to Load Results");
                if (!NetworkUtils.isNetworkConnected(mContext))
                    s.append(", please check your network connection");
                mFragment.footerView.setText(s);
            }
            //    TextView textView = (TextView) fragmentView.findViewById(R.id.section_label);
            //    textView.setText(results);
            mFragment.footerView.setVisibility(View.VISIBLE);
            mFragment.getView().findViewById(android.R.id.progress).setVisibility(View.GONE);
            mFragment.listView.setDivider(mFragment.getResources().getDrawable(R.drawable.horizontal_divider));
        } catch (NullPointerException e) {
            Utils.Print.e(TAG, "NullPointerException: " + e.getMessage());
            Utils.logException(e);
//            Utils.sendException(t, TAG, "NullPointerException", url + ":" + e.getMessage());
        }
        //Used to prevent running of multiple tasks simultaneously particularly useful in case of Scrolling.
        mFragment.updating = false;
    }


/*    private void parseJSON(String json) throws JSONException {
        JSONObject jsonObject = new JSONObject(json);
        mFragment.totalResults = jsonObject.getLong(TAG_TOTAL);
        int i = 1;
        if ((mFlags & FLAG_LOAD_FACET) == 0)
            i = 2;
        ArrayList<String> selectedFacets = new ArrayList<String>();
        JSONArray selected_facets = jsonObject.getJSONArray(TAG_SELECTED_FACETS);
        for (int k = 0; k < selected_facets.length(); k++) {
            String tag = selected_facets.getJSONObject(k).getString(TAG_TAG);
            selectedFacets.add(tag);
        }
        JSONArray folders = jsonObject.getJSONArray(TAG_FOLDERS);
        for (; i < folders.length(); i++) {
            if (isCancelled()) return;
            JSONObject folderInfo = folders.getJSONObject(i);
            String name = "";
            if (i == 1)
                name += "   ";
            name += folderInfo.getString(TAG_NAME).toUpperCase();
            Folder f = new Folder(i - 1, name);
            //    f.setSelectedCountListener();
            JSONArray facets = folderInfo.getJSONArray(TAG_FACETS);
            int zeroIndex = -1;
            for (int j = 0; j < facets.length(); j++) {
                JSONObject facet = facets.getJSONObject(j);
                int count = facet.getInt(TAG_COUNT);
                if (count > 0 || (mFlags & FLAG_LOAD_FACET) == 0 || i > 1) {
                    String label = facet.getString(StringsUtils.TAG_LABEL);
                    String tag = facet.getString(TAG_TAG);
                    int selected = selectedFacets.contains(tag) ? 1 : 0;
                    if (zeroIndex == -1) {
                        if (count > 0)
                            f.addFacet(label, tag, selected, facet.getInt(TAG_COUNT));
                        else {
                            zeroIndex = j;
                            f.addFacet(label, tag, selected, facet.getInt(TAG_COUNT));
                        }
                    } else if (count > 0) {
                        f.addFacet(zeroIndex, label, tag, selected, facet.getInt(TAG_COUNT));
                        zeroIndex++;
                    } else {
                        f.addFacet(label, tag, selected, facet.getInt(TAG_COUNT));
                    }
                }
            }
            mFragment.mFolderList.add(f);
        }
        JSONArray products = jsonObject.getJSONArray(TAG_PRODUCTS);
        for (int k = 0; k < products.length(); k++) {
            if (isCancelled()) return;
            JSONObject productInfo = products.getJSONObject(k);
            Product p = Product.createFromJSON(productInfo);
            mFragment.mProductList.add(p);
            int count = 1;
            if (searchLabels.containsKey(p.category)) {
                count = searchLabels.get(p.category) + 1;
            }
            searchLabels.put(p.category, count);
        }
        mFragment.next = jsonObject.getString(TAG_NEXT);
    }*/

    /**
     * Parse JSON Using JackSon Parser
     *
     * @param jsonString The json Object returned by network call in form of a String.
     * @throws IOException
     */
    public void parseJackSon(String jsonString) throws IOException {
        JsonFactory factory = ((BuyingIQApp) mContext.getApplication()).getFactory();
        JsonParser jp = factory.createParser(jsonString);
        jp.nextToken();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            switch (fieldName) {
                case TAG_TOTAL:
                    mFragment.totalResults = jp.getLongValue();
                    break;
                case TAG_SELECTED_FACETS:
                    //    jp.skipChildren();
                    ArrayList<String> selectedFacets = new ArrayList<>();
                    readSelectedFacets(jp, selectedFacets);
                    break;
                case TAG_FOLDERS:
                    populateFolderList(jp);
                    break;
                case TAG_SELECTED_TAGS:
                    jp.skipChildren();
                    break;
                case TAG_PRODUCTS:
                    populateProductList(jp);
                    break;
                case TAG_NEXT:
                    mFragment.next = jp.getText();
                    break;
            }
        }
    }

    private void populateProductList(JsonParser jp) throws IOException {
        productsLocalCopy = new ArrayList<>();
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            if (isCancelled()) return;
            productsLocalCopy.add(Product.createFromJSON(jp));
        }
    }

    private void readSelectedFacets(JsonParser jp, ArrayList<String> selectedFacets) throws IOException {
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            Facet f = Facet.createFromJSON(jp, null);
            selectedFacets.add(f.tag);
        }
    }

    private void populateFolderList(JsonParser jp) throws IOException {
        int i = 0;
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            if (isCancelled()) return;
            if (i == 0 && (mFlags & FLAG_SEARCH_ACTIVE) == 0) jp.skipChildren();
            else if ((mFlags & FLAG_LOAD_FACET) == 0 && i == 1)
                jp.skipChildren();
            else {
                Folder folder = Folder.createFromJSON(jp, i, ((BuyingIQApp) mContext.getApplication()).getCategories());
                folder.setSelectedCountListener(mFragment);
                selectedCount += folder.getSelectedCount();
                mFragment.mFolderList.add(folder);
            }
            i++;
        }
    }

}