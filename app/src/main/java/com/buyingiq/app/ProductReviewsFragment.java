package com.buyingiq.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.buyingiq.app.entities.ProductReview;
import com.buyingiq.app.entities.Rating;
import com.buyingiq.app.entities.User;
import com.buyingiq.app.listadapters.ReviewsAdapter;
import com.buyingiq.app.resources.MathUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.buyingiq.app.resources.StringUtils.TAG_ADDED_ON;
import static com.buyingiq.app.resources.StringUtils.TAG_AVERAGE;
import static com.buyingiq.app.resources.StringUtils.TAG_AVERAGES;
import static com.buyingiq.app.resources.StringUtils.TAG_COUNT;
import static com.buyingiq.app.resources.StringUtils.TAG_HEAD;
import static com.buyingiq.app.resources.StringUtils.TAG_IS_FEATURED;
import static com.buyingiq.app.resources.StringUtils.TAG_IS_PUBLISHED;
import static com.buyingiq.app.resources.StringUtils.TAG_IS_TOP_REVIEW;
import static com.buyingiq.app.resources.StringUtils.TAG_NEXT;
import static com.buyingiq.app.resources.StringUtils.TAG_OVERALL_RATING;
import static com.buyingiq.app.resources.StringUtils.TAG_PUBLISHED_ON;
import static com.buyingiq.app.resources.StringUtils.TAG_RATINGS;
import static com.buyingiq.app.resources.StringUtils.TAG_REVIEW;
import static com.buyingiq.app.resources.StringUtils.TAG_REVIEWS;
import static com.buyingiq.app.resources.StringUtils.TAG_SCORE;
import static com.buyingiq.app.resources.StringUtils.TAG_UPVOTES;
import static com.buyingiq.app.resources.StringUtils.TAG_URI;
import static com.buyingiq.app.resources.StringUtils.TAG_USER;
import static com.buyingiq.app.resources.StringUtils.TAG_VIEWS;

/**
 * A simple {@link ListFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductReviewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductReviewsFragment extends ListFragment implements AbsListView.OnScrollListener {

    public static final int FLAG_LOAD_RATINGS = 1;
    public static final int FLAG_RELOAD_REVIEWS = 1 << 1;
    public static final int FLAG_HAS_AVERAGE_RATING = 1 << 3;
    private static final int FLAG_LOAD_NEXT = 1 << 2;
    private static final boolean D = true;
    private static final String TAG = "ProductReviewsFragment";
    private static final String ARG_PRODUCT_ID = "product_id";
    public static String apiUrl;
    public static String selected_sort = "upvotes";
    Spinner.OnItemSelectedListener mSpinnerItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            String sort_by = getResources().getStringArray(R.array.sort_by_params)[i];
            if (!sort_by.equals(selected_sort)) {
                //We need to reload only reviews here, so flag to reload will be called.
                runtask(FLAG_RELOAD_REVIEWS, sort_by);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    public ArrayList<Rating> averageRatings;
    public ArrayList<ProductReview> productReviews;
    public ViewGroup mRatingsContainer;
    public View mHeader;
    public String next;
    TextView footerView;
    boolean loaded = false;
    boolean updating = false;
    ArrayList<LoadReviewsTask1> tasks;
    Tracker t;
    private String mProductID;
    private Spinner sortSpinner;
    private ReviewsAdapter reviewsAdapter;
    private OnFragmentInteractionListener mListener;
    View.OnClickListener reviewListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ProductReview r = (ProductReview) view.getTag();
            if (mListener != null)
                mListener.onReviewSelected(r, (View) view.getParent());
        }
    };

    public ProductReviewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param productID Parameter 1.
     * @return A new instance of fragment ProductReviewsFragment.
     */
    public static ProductReviewsFragment newInstance(String productID) {
        ProductReviewsFragment fragment = new ProductReviewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCT_ID, productID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductID = getArguments().getString(ARG_PRODUCT_ID);
            apiUrl = String.format(StringUtils.BASE_URL + "products/%s/reviews/", mProductID);
        }
        productReviews = new ArrayList<>();
        reviewsAdapter = new ReviewsAdapter(getActivity(), productReviews, reviewListener);
        tasks = new ArrayList<>();
        next = null;
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_reviews, container, false);
        return view;
    }

    public void init() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.item_review_header, getListView(), false);
        sortSpinner = (Spinner) view.findViewById(R.id.review_sort_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_by_display, R.layout.item_spinner_textview);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(adapter);
        mRatingsContainer = (ViewGroup) view.findViewById(R.id.average_rating_layout);
        footerView = (TextView) inflater.inflate(R.layout.overscroll_footer, null, false);
        getListView().addHeaderView(view);
        getListView().addFooterView(footerView);
        getListView().setOnScrollListener(this);
        getListView().getEmptyView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runtask(FLAG_LOAD_RATINGS | FLAG_RELOAD_REVIEWS, getResources().getStringArray(R.array.sort_by_params)[0]);
            }
        });
        view.setVisibility(View.GONE);
        footerView.setVisibility(View.GONE);
        setListAdapter(reviewsAdapter);
        mHeader = view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        //Initialization of reviews, need to load both reviews and ratings
        runtask(FLAG_LOAD_RATINGS | FLAG_RELOAD_REVIEWS, getResources().getStringArray(R.array.sort_by_params)[0]);
        sortSpinner.setOnItemSelectedListener(mSpinnerItemSelectedListener);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("KEY", "value");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int first, int visible, int total) {
        if (first + visible >= total && total != 0) {
            if (next != null)
                if (next.length() > 1) {
                    if (!updating) {
                        runtask(FLAG_LOAD_NEXT, null);
                    }
                }
        }
    }

    public void runtask(int flags, String sortParam) {
        updating = true;
        String url = apiUrl;

        if ((flags & FLAG_LOAD_NEXT) != 0)
            url += "?" + next;
        if (sortParam != null) {
            url += "?sort_by=" + sortParam + "&sort_order=desc";
            selected_sort = sortParam;
        }
        LoadReviewsTask1 lrt =
                new LoadReviewsTask1(getActivity(), url, flags);
        lrt.execute();
        tasks.add(lrt);
    }

    @Override
    public void onDestroyView() {
        for (LoadReviewsTask1 t : tasks) {
            t.cancel(true);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onReviewSelected(ProductReview r, View view);
    }

    public class LoadReviewsTask1 extends BaseSyncTask<Void> {

        private static final String TAG = "LoadReviewsTask";
        public int total;

        LoadReviewsTask1(Activity context, String url, int flags) {
            super(context, url, flags);
            super.setListView(getListView());
            if (getView() != null)
                super.setProgressBar(getView().findViewById(android.R.id.progress));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if ((flags & ProductReviewsFragment.FLAG_LOAD_RATINGS) != 0)
                for (int i = 1; i < mRatingsContainer.getChildCount(); i++)
                    mRatingsContainer.removeViewAt(i);
            if ((flags & ProductReviewsFragment.FLAG_RELOAD_REVIEWS) != 0) {
                productReviews.clear();
                reviewsAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected Integer doInBackground(Void... args) {
            Integer returnCode = super.doInBackground(args);
            if (returnCode == 0) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    total = jsonObject.getInt(StringUtils.TAG_TOTAL);
                    JSONArray reviewsArray = jsonObject.getJSONArray(TAG_REVIEWS);
                    readReviews(reviewsArray);
                    if ((flags & ProductReviewsFragment.FLAG_LOAD_RATINGS) != 0) {
                        averageRatings = getRatings(jsonObject.getJSONArray(TAG_AVERAGES));
                    }
                    next = jsonObject.getString(TAG_NEXT);
                    if (next.length() < 1 || next.equals("null"))
                        next = null;
                    if (isCancelled())
                        return -3;
                } catch (JSONException e) {
                    Utils.Print.e(TAG, "JSONException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "JSONException", url + ":" + e.getMessage());
                    return -2;
                }
            }
            return returnCode;
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            super.onPostExecute(returnValue);
            if (returnValue.equals(0)) {
                if (productReviews.size() > 0) {
                    if ((flags & FLAG_LOAD_RATINGS) != 0) {
                        if (averageRatings != null && averageRatings.size() > 1) {
                            for (Rating r : averageRatings) {
                                if (!r.head.equalsIgnoreCase("Overall Rating")) {
                                    View child = getRating(r);
                                    mRatingsContainer.addView(child);
                                }
                            }
                        } else {
                            mRatingsContainer.setVisibility(View.GONE);
                        }
                    }
                    listView.setVisibility(View.VISIBLE);
                    setEmptyText("");
                } else {
                    listView.setVisibility(View.INVISIBLE);
                    setEmptyText("No Reviews for this product yet");
                    listView.getEmptyView().setOnClickListener(null);
                }
                reviewsAdapter.notifyDataSetChanged();
                if (mHeader != null && mHeader.getVisibility() != View.VISIBLE)
                    mHeader.setVisibility(View.VISIBLE);
                ((TextView) mHeader.findViewById(R.id.review_count)).setText("Showing " + total + " Reviews");
                if (next != null && !next.equals("null") && next.length() > 1) {
                    footerView.setText("Loading more Reviews");
                    footerView.setVisibility(View.VISIBLE);
                } else {
                    //    footerView.setText("End of Reviews");
                    footerView.setVisibility(View.GONE);
                    //        getListView().removeFooterView(footerView);
                }
                //        getView().findViewById(R.id.loading_progressbar).setVisibility(View.GONE);
                loaded = true;
                updating = false;
            }
        }

        private View getRating(Rating r) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View child = inflater.inflate(R.layout.item_average_rating, mRatingsContainer, false);
            ((TextView) child.findViewById(R.id.average_rating_label)).setText(r.head.toUpperCase());
            ((ProgressBar) child.findViewById(R.id.average_rating_progressbar)).setProgress(r.isAverage ? (int) (r.average * 20) : r.score);
            String text = "" + (r.isAverage ? r.average : r.score);
            String foot = "/5";
            Spannable styledText = new SpannableString(text + foot);
            TextAppearanceSpan span = new TextAppearanceSpan(getActivity(), R.style.textLightGrey);
            styledText.setSpan(span, text.length() + 1, styledText.length(), Spanned.SPAN_COMPOSING);
            ((TextView) child.findViewById(R.id.average_rating_text)).setText(styledText);
            return child;
        }


        private void readReviews(JSONArray reviewsArray) throws JSONException {
            for (int i = 0; i < reviewsArray.length(); i++) {
                ProductReview r = getReview(reviewsArray.getJSONObject(i));
                if (r != null)
                    productReviews.add(r);
            }
        }

        private ProductReview getReview(JSONObject reviewObject) throws JSONException {
            ProductReview r = new ProductReview(reviewObject.getString(TAG_URI));
            r.setPublished(reviewObject.getBoolean(TAG_IS_PUBLISHED));
            if (!r.isPublished())
                return null;
            r.setUser(User.createFromJson(reviewObject.getJSONObject(TAG_USER)));
            r.setAddedOn(reviewObject.getString(TAG_ADDED_ON));
            r.setFeatured(reviewObject.getBoolean(TAG_IS_FEATURED));
            r.setOverallRating(reviewObject.getString(TAG_OVERALL_RATING));
            r.setPublishedOn(reviewObject.getString(TAG_PUBLISHED_ON));
            String d = "";
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = sdf.parse(r.getPublishedOn());
                sdf.applyPattern("MMM ''yy");
                d = sdf.format(date);
            } catch (ParseException e) {
                Utils.Print.e(TAG, "Date format unknown: " + r.getPublishedOn());
                Utils.logException(e);
//                Utils.sendException(t, TAG, "DateFormatUnknown", r.getUri() + ":" + r.getPublishedOn());
            }
            r.setPublishedOnStr(d);
            r.setRatings(getRatings(reviewObject.getJSONArray(TAG_RATINGS)));
            r.setReview(reviewObject.getString(TAG_REVIEW));
            r.setTopReview(reviewObject.getBoolean(TAG_IS_TOP_REVIEW));
            r.setUpvotes(reviewObject.getInt(TAG_UPVOTES));
            r.setViews(reviewObject.getInt(TAG_VIEWS));
            return r;
        }

        private ArrayList<Rating> getRatings(JSONArray json) throws JSONException {
            ArrayList<Rating> r = new ArrayList<Rating>();
            for (int i = 0; i < json.length(); i++) {
                JSONObject js = json.getJSONObject(i);
                Rating ra = new Rating();
                if (js.has(TAG_COUNT)) {
                    ra.isAverage = true;
                    ra.head = js.getString(TAG_HEAD).toUpperCase();
                    if (ra.head.contains("("))
                        ra.head = ra.head.split("\\(")[0];
                    if (ra.head.contains("/"))
                        ra.head = ra.head.split("/")[0];
                    ra.average = js.getDouble(TAG_AVERAGE);
                    ra.average = MathUtils.round(ra.average, 1);
                    ra.count = js.getInt(TAG_COUNT);
                } else {
                    ra.isAverage = false;
                    ra.head = js.getString(TAG_HEAD).toUpperCase();
                    if (ra.head.contains("("))
                        ra.head = ra.head.split("\\(")[0];
                    if (ra.head.contains("/"))
                        ra.head = ra.head.split("/")[0];
                    ra.score = js.getInt(TAG_SCORE);
                }
                r.add(ra);
            }
            return r;
        }
    }


    /*  private class LoadReviewsTask extends AsyncTask<String, ProductReview, Integer> {
        private final Context mContext;
        private int mFlags;
        public int total;
        public ArrayList<Rating> averageRatings;

        LoadReviewsTask(Context context, int flags) {
            mContext = context;
            mFlags = flags;
        }

        @Override
        protected void onPreExecute() {
            if ((mFlags & ProductReviewsFragment.FLAG_LOAD_RATINGS) != 0)
                for (int i = 1; i < mRatingsContainer.getChildCount(); i++)
                    mRatingsContainer.removeViewAt(i);
            if ((mFlags & ProductReviewsFragment.FLAG_RELOAD_REVIEWS) != 0) {
                productReviews.clear();
                ((TextView) getListView().getEmptyView()).setText("");
                reviewsAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected Integer doInBackground(String... args) {
            NetworkUtils networkUtils = new NetworkUtils();
            String buff = networkUtils.makeServiceCall(args[0], NetworkUtils.GET);
            try {
                if (buff != null && !buff.contains("ERROR")) {
                    JSONObject json = new JSONObject(buff);
                    total = json.getInt(StringUtils.TAG_TOTAL);
                    JSONArray reviewsArray = json.getJSONArray(TAG_REVIEWS);
                    readReviews(reviewsArray);
                    if ((mFlags & ProductReviewsFragment.FLAG_LOAD_RATINGS) != 0) {
                        averageRatings = getRatings(json.getJSONArray(TAG_AVERAGES));
                        if (averageRatings != null && averageRatings.size() > 0)
                            mFlags |= FLAG_HAS_AVERAGE_RATING;
                    }
                    next = json.getString(TAG_NEXT);
                    if (next.length() < 1 || next.equals("null"))
                        next = null;
                    if (isCancelled())
                        return -3;
                } else {
                    Utils.Print.e(TAG, "Got null json");
                    return -1;
                }
            } catch (JSONException e) {
                Utils.Print.e(TAG, "JSONException: " + e.getMessage());
                return -2;
            }
            return 0;
        }

        @Override
        protected void onProgressUpdate(ProductReview... values) {
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            if (returnValue.equals(0)) {
                if ((mFlags & FLAG_HAS_AVERAGE_RATING) != 0 && productReviews.size() > 0) {
                    if (averageRatings.size() > 1) {
                        for (Rating r : averageRatings) {
                            if (!r.head.equalsIgnoreCase("Overall Rating")) {
                                View child = getRating(r);
                                mRatingsContainer.addView(child);
                            }
                        }
                    } else {
                        mRatingsContainer.setVisibility(View.GONE);
                    }
                    getListView().setVisibility(View.VISIBLE);
                    ((TextView) getListView().getEmptyView()).setText("");
                } else {
                    getListView().setVisibility(View.INVISIBLE);
                    ((TextView) getListView().getEmptyView()).setText("No Reviews for this product yet");
                }
                reviewsAdapter.notifyDataSetChanged();
                if (mHeader != null && mHeader.getVisibility() != View.VISIBLE)
                    mHeader.setVisibility(View.VISIBLE);
                ((TextView) mHeader.findViewById(R.id.review_count)).setText("Showing " + total + " Reviews");
                if (next != null && !next.equals("null") && next.length() > 1) {
                    footerView.setText("Loading more Reviews");
                    footerView.setVisibility(View.VISIBLE);
                } else {
                    //    footerView.setText("End of Reviews");
                    footerView.setVisibility(View.GONE);
                    //        getListView().removeFooterView(footerView);
                }
                //        getView().findViewById(R.id.loading_progressbar).setVisibility(View.GONE);
                View progress = getView().findViewById(android.R.id.progress);
                if (progress != null)
                    progress.setVisibility(View.GONE);
                loaded = true;
                updating = false;
            } else {
                NetworkUtils.networkCheck((ProductActivity) mContext);
            }
        }

        private View getRating(Rating r) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View child = inflater.inflate(R.layout.item_average_rating, mRatingsContainer, false);
            ((TextView) child.findViewById(R.id.average_rating_label)).setText(r.head.toUpperCase());
            ((ProgressBar) child.findViewById(R.id.average_rating_progressbar)).setProgress(r.isAverage ? (int) (r.average * 20) : r.score);
            String text = "" + (r.isAverage ? r.average : r.score);
            String foot = "/5";
            Spannable styledText = new SpannableString(text + foot);
            TextAppearanceSpan span = new TextAppearanceSpan(getActivity(), R.style.textLightGrey);
            styledText.setSpan(span, text.length() + 1, styledText.length(), Spanned.SPAN_COMPOSING);
            ((TextView) child.findViewById(R.id.average_rating_text)).setText(styledText);
            return child;
        }


        private void readReviews(JSONArray reviewsArray) throws JSONException {
            for (int i = 0; i < reviewsArray.length(); i++) {
                ProductReview r = getReview(reviewsArray.getJSONObject(i));
                if (r != null)
                    productReviews.add(r);
            }
        }

        private ProductReview getReview(JSONObject reviewObject) throws JSONException {
            ProductReview r = new ProductReview(reviewObject.getString(TAG_URI));
            r.setPublished(reviewObject.getBoolean(TAG_IS_PUBLISHED));
            if (!r.isPublished())
                return null;
            r.setUser(User.createFromJson(reviewObject.getJSONObject(TAG_USER)));
            r.setAddedOn(reviewObject.getString(TAG_ADDED_ON));
            r.setFeatured(reviewObject.getBoolean(TAG_IS_FEATURED));
            r.setOverallRating(reviewObject.getString(TAG_OVERALL_RATING));
            r.setPublishedOn(reviewObject.getString(TAG_PUBLISHED_ON));
            String d = "";
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = sdf.parse(r.getPublishedOn());
                sdf.applyPattern("MMM ''yy");
                d = sdf.format(date);
            } catch (ParseException e) {
                Utils.Print.e(TAG, "Date format unknown: " + r.getPublishedOn());
            }
            r.setPublishedOnStr(d);
            r.setRatings(getRatings(reviewObject.getJSONArray(TAG_RATINGS)));
            r.setReview(reviewObject.getString(TAG_REVIEW));
            r.setTopReview(reviewObject.getBoolean(TAG_IS_TOP_REVIEW));
            r.setUpvotes(reviewObject.getInt(TAG_UPVOTES));
            r.setViews(reviewObject.getInt(TAG_VIEWS));
            return r;
        }

        private ArrayList<Rating> getRatings(JSONArray json) throws JSONException {
            ArrayList<Rating> r = new ArrayList<Rating>();
            for (int i = 0; i < json.length(); i++) {
                JSONObject js = json.getJSONObject(i);
                Rating ra = new Rating();
                if (js.has(TAG_COUNT)) {
                    ra.isAverage = true;
                    ra.head = js.getString(TAG_HEAD).toUpperCase();
                    if (ra.head.contains("("))
                        ra.head = ra.head.split("\\(")[0];
                    if (ra.head.contains("/"))
                        ra.head = ra.head.split("/")[0];
                    ra.average = js.getDouble(TAG_AVERAGE);
                    ra.average = MathUtils.round(ra.average, 1);
                    ra.count = js.getInt(TAG_COUNT);
                } else {
                    ra.isAverage = false;
                    ra.head = js.getString(TAG_HEAD).toUpperCase();
                    if (ra.head.contains("("))
                        ra.head = ra.head.split("\\(")[0];
                    if (ra.head.contains("/"))
                        ra.head = ra.head.split("/")[0];
                    ra.score = js.getInt(TAG_SCORE);
                }
                r.add(ra);
            }
            return r;
        }
    } */

}
