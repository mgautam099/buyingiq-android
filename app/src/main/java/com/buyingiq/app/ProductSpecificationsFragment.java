package com.buyingiq.app;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.buyingiq.app.listadapters.SpecificationAdapter;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link ListFragment} subclass.
 * Use the {@link ProductSpecificationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductSpecificationsFragment extends ListFragment implements AbsListView.OnScrollListener {
    private static final String ARG_PRODUCT_ID = "product";
    private static final String TAG_GROUP = "group";
    private static final String TAG_FEATURES = "features";
    private static final String TAG_NAME = "name";
    private static final String TAG_DESCR = "descr";
    private static final String TAG_VALUE = "value";
    private static final String TAG = "ProductSpecificationsFragment";
    LoadFeaturesTask lft;
    //    private Product mProduct;
    private String apiUrl;
    private ArrayList<String[]> features;
    private SpecificationAdapter mAdapter;
    private Tracker t;
//    private ImageLoader imageLoader;

//    private View actionLayout;

    public ProductSpecificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param productID Parameter 1.
     * @return A new instance of fragment ProductSpecificationsFragment.
     */
    public static ProductSpecificationsFragment newInstance(String productID) {
        ProductSpecificationsFragment fragment = new ProductSpecificationsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PRODUCT_ID, productID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mProductID = getArguments().getString(ARG_PRODUCT_ID);
            apiUrl = String.format(StringUtils.BASE_URL + "products/%s/features/", mProductID);
        }
//        imageLoader = new ImageLoader(getActivity(), 0);
        features = new ArrayList<>();
        mAdapter = new SpecificationAdapter(getActivity(), R.layout.item_spec, features);
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_specifications, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //    new LoadOverviewTask(getActivity()).execute();
        refreshList();
        getListView().setOnScrollListener(this);
        setListAdapter(mAdapter);
        getListView().getEmptyView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshList();
            }
        });
    }

    public void refreshList() {
        lft = new LoadFeaturesTask(getActivity());
        lft.execute();
    }

    @Override
    public void onDestroyView() {
        lft.cancel(true);
        super.onDestroyView();
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }


/*    private static class ViewHolder{
        ImageView image;
        TextView rating;
        TextView price;
        TextView stores;
        TextView votes;
        ViewGroup featureGroup;
    }*/

    static class Feature {
        String name;
        String value;
        String descr;

    }

    static class Group {
        String group;
        ArrayList<Feature> features;
    }

    class LoadFeaturesTask extends BaseSyncTask<Void> {
        //    private final Context mContext;
        ArrayList<Group> groups;

        LoadFeaturesTask(Activity context) {
            super(context, apiUrl, 0);
            super.setListView(getListView());
            super.setProgressBar(getView().findViewById(android.R.id.progress));
            //    mContext = context;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer returnCode = super.doInBackground(voids);
            groups = new ArrayList<>();
            if (returnCode == 0) {
                try {
                    JSONArray jsonArray = new JSONArray(json);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject groupObj = jsonArray.getJSONObject(i);
                        Group g = new Group();
                        g.group = groupObj.getString(TAG_GROUP);
                        features.add(new String[]{g.group});
                        g.features = new ArrayList<>();
                        JSONArray featureArr = groupObj.getJSONArray(TAG_FEATURES);
                        for (int j = 0; j < featureArr.length(); j++) {
                            Feature fe = new Feature();
                            JSONObject featureObj = featureArr.getJSONObject(j);
                            fe.name = featureObj.getString(TAG_NAME);
                            fe.descr = featureObj.getString(TAG_DESCR);
                            fe.value = featureObj.getString(TAG_VALUE);
                            if (!fe.value.isEmpty() && !fe.name.isEmpty() && !fe.value.equals("null")) {
                                features.add(new String[]{fe.name, fe.value});
                                g.features.add(fe);
                            }
                        }
                        if (featureArr.length() > 0)
                            groups.add(g);
                    }
                } catch (JSONException e) {
                    Utils.Print.e(TAG, "JSONException: " + e.getMessage());
                    Utils.logException(e);
//                    Utils.sendException(t, TAG, "JSONException", url + ":" + e.getMessage());
                    return OTHER_ERROR;
                }
            }
            return returnCode;
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            super.onPostExecute(returnValue);
            if (returnValue == 0) {
                mAdapter.notifyDataSetChanged();
            }

        }
    }

/*    RobotoTextView footerTextView;
    class LoadOverviewTask extends AsyncTask<Void,View,ViewHolder>{

        ViewHolder viewHolder;
        Context mContext;
        LoadOverviewTask(Context context){
            mContext = context;
        }
        @Override
        protected void onProgressUpdate(View... values) {
            if(isCancelled())
                return;
            if(values.length==0)
            {
                viewHolder.featureGroup.findViewById(R.id.key_feat_head).setVisibility(View.GONE);
            }
            else if(values[0].getTag()==null)
            {
                viewHolder.featureGroup.addView(values[0]);
            }
            else{
                getListView().addHeaderView(values[0],"Overview",false);
                getListView().addFooterView(values[1],"loading",false);
                setListAdapter(mAdapter);
            }
        }

        @Override
        protected ViewHolder doInBackground(Void... voids) {
            if(isCancelled())
                return null;
            try {
                View overView = getOverView(getListView());
                viewHolder = (ViewHolder)overView.getTag();
                setOverView(viewHolder,mContext);
                footerTextView = new RobotoTextView(mContext);
                footerTextView.setTypeFaceRoboto(RobotoTextView.ROBOTO);
                footerTextView.setText("Loading Specifications...");
                footerTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                footerTextView.setTextColor(getResources().getColor(R.color.text_lightest_grey));
                if(isCancelled())
                    return null;
                publishProgress(overView, footerTextView);
                if(mProduct.keyFeatures.size()>0)
                {
                    if(isCancelled())
                        return null;
                    for(String s: mProduct.keyFeatures)
                    {
                        LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
                        TextView robotoTextView = (TextView)inflater.inflate(R.layout.item_key_features, null, false);
                        robotoTextView.setText(s);
                        publishProgress(robotoTextView);
                    }
                    if(isCancelled())
                        return null;
                    View emptyView = new View(mContext);
                    emptyView.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 20));
                    publishProgress(emptyView);
                    View divider = new View(mContext);
                    divider.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    divider.setBackgroundResource(R.drawable.horizontal_divider);
                    //     viewHolder.mainLayout.addView(emptyView);
                    publishProgress(divider);

                }
                else
                {
                    if(isCancelled())
                        return null;
                    publishProgress();
                }
                if(isCancelled())
                    return null;
                RobotoTextView specTextView = new RobotoTextView(mContext);
                specTextView.setTypeFaceRoboto(RobotoTextView.ROBOTO_BOLD);
                specTextView.setText("FULL SPECIFICATIONS");
                specTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.title_head_font_size));
                specTextView.setTextColor(getResources().getColor(R.color.text_dark_grey));
                specTextView.setPadding(0,20,0,5);
                publishProgress(specTextView);
                if(isCancelled())
                    return null;
                return viewHolder;
                }
            catch (IllegalStateException e)
            {
                cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ViewHolder viewHolder) {
            setOverView(viewHolder,mContext);
        }
    }

    private View getOverView(ListView listView)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout convertView = (LinearLayout)inflater.inflate(R.layout.item_product_overview,listView,false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.image = (ImageView) convertView.findViewById(R.id.product_image);
        viewHolder.rating = (TextView)convertView.findViewById(R.id.product_rating);
        viewHolder.price =  (TextView)convertView.findViewById(R.id.product_price);
        viewHolder.stores = (TextView)convertView.findViewById(R.id.product_stores);
        viewHolder.votes = (TextView)convertView.findViewById(R.id.product_votes);
        viewHolder.featureGroup = convertView;
        convertView.setTag(viewHolder);
        return convertView;
    }

    private void setOverView(ViewHolder viewHolder,Context context)
    {
        imageLoader.DisplayImage(mProduct.xLargeImageUrl, viewHolder.image);
        int votes = Integer.parseInt(mProduct.votes);
        if (votes > 0) {
            viewHolder.votes.setText(mProduct.votes + " Votes");
        }
        String foot = "/10";
        String text = mProduct.rating;
        Spannable styledText = new SpannableString(text+foot);
        TextAppearanceSpan span = new TextAppearanceSpan(context,R.style.textTotalRating);
        styledText.setSpan(new SuperscriptSpan(), text.length(), text.length() + foot.length(), Spanned.SPAN_COMPOSING);
        styledText.setSpan(span, text.length(), text.length() + foot.length(), Spanned.SPAN_COMPOSING);
        viewHolder.rating.setText(styledText);
        viewHolder.price.setText("Rs. " + mProduct.price);
        viewHolder.stores.setText(mProduct.numStores + " Stores");
    }*/

}
