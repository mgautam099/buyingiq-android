package com.buyingiq.app;

import android.app.ActionBar;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.buyingiq.app.listadapters.SearchCursorAdapter;
import com.buyingiq.app.resources.MemoryCache;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.SearchProvider;
import com.buyingiq.app.resources.StringUtils;

public class SearchActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        TextWatcher, TextView.OnEditorActionListener {

    public static final String TAG = "SearchActivity";
    CursorAdapter mAdapter;
    TextView emptyText;
    View progressbar;
    TextView recentView;
    private String mCurFilter;
    private MemoryCache mMemoryCache;

    //    TextView sectionLabel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        //    sectionLabel = (TextView)findViewById(R.id.section_label);
        mAdapter = new SearchCursorAdapter(this, null);
        //    View header = getLayoutInflater().inflate(R.layout.item_recent_search,getListView(),false);
        //    getListView().addHeaderView(header);
        recentView = (TextView) findViewById(R.id.recent_view);
        setListAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
        emptyText = (TextView) findViewById(android.R.id.empty);
        progressbar = findViewById(android.R.id.progress);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.search_layout);
        actionBar.setLogo(R.drawable.ic_logo);
        EditText text = (EditText) actionBar.getCustomView().findViewById(R.id.search_field);
        text.setOnEditorActionListener(this);
        text.addTextChangedListener(this);
        text.requestFocus();
        showSoftKeyboard(text);
        //    actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
        //            | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME);
        //    Utils.changeUpIcon(actionBar,this,R.drawable.ic_action_previous_item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        restoreActionBar();
    /*    SearchManager sm = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setSearchableInfo(sm.getSearchableInfo(getComponentName()));
        searchMenuItem.expandActionView();
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).
                toggleSoftInput(InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_IMPLICIT_ONLY);
        mSearchView.setOnQueryTextListener(this);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.action_search:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showResults(String s) {
        hideSoftKeyboard();
        Intent i = new Intent(this, ProductListingActivity.class);
        i.setAction(Intent.ACTION_SEARCH);
        i.putExtra(SearchManager.QUERY, s);
        startActivity(i);
        //    finish();
        overridePendingTransition(0, 0);
    }


    public void openResult(Uri data, String productID) {
        hideSoftKeyboard();
        Intent i = new Intent(this, ProductActivity.class);
        i.setData(data);
        if (productID != null) {
            i.putExtra(StringUtils.ARG_PRODUCT_ID, productID);
        }
        i.putExtra(ProductDatabaseHelper.KEY_VISITED_FROM, TAG);
        startActivity(i);
        //    finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    public void openProduct(String query) {
        Uri data = Uri.parse(query);
        openResult(data, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        mAdapter.swapCursor(cursor);
        progressbar.setVisibility(View.GONE);
        if (mCurFilter == null) {
            if (cursor != null && cursor.getCount() > 0) {
                //    sectionLabel.setText("Recently Viewed");
                //    sectionLabel.setVisibility(View.VISIBLE);
                recentView.setVisibility(View.VISIBLE);
                recentView.setText("RECENT SEARCHES");
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
                emptyText.setText("");
                getListView().startAnimation(anim);
            } else {
                //    sectionLabel.setVisibility(View.GONE);
                recentView.setVisibility(View.GONE);
                emptyText.setText(Html.fromHtml("<font color=\"#949494\">Search by typing something in the search box.</font>"));
            }
        } else {
            if (cursor != null && cursor.getCount() > 0) {
                recentView.setVisibility(View.VISIBLE);
                recentView.setText("Showing results for " + mCurFilter);
                //sectionLabel.setText("\""+mCurFilter+"\"");
                emptyText.setText("");
            } else {
                //sectionLabel.setText("No Results");
                if (NetworkUtils.isNetworkConnected(this)) {
                    recentView.setVisibility(View.GONE);
                    emptyText.setText(Html.fromHtml("No Results for \"<b>" + mCurFilter + "</b>\""));
                } else {
                    recentView.setVisibility(View.GONE);
                    emptyText.setText(Html.fromHtml("Please check your network connection"));
                }
            }
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
        progressbar.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.GONE);
    }
/*
    @Override
    public boolean onQueryTextSubmit(String query) {
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                SearchProvider.AUTHORITY, SearchProvider.MODE);
        suggestions.saveRecentQuery(query, null);
        showResults(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return true;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }
        mCurFilter = newFilter;
        progressbar.setVisibility(View.VISIBLE);
        if(newFilter!=null)
        {
            mAdapter.swapCursor(null);
            emptyText.setVisibility(View.GONE);
        }
        else
        {
            emptyText.setText("Popular Searches: Samsung, HTC, Sony");
            emptyText.setVisibility(View.VISIBLE);
        }
        getLoaderManager().restartLoader(0, null, this);
        return true;
    }*/

    /*
    @Override
    public boolean onClose() {
        if(!TextUtils.isEmpty(mSearchView.getQuery()))
            mSearchView.setQuery(null,true);
        return true;
    }*/

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Cursor c = mAdapter.getCursor();
        c.moveToPosition(position);
        int it1 = c.getColumnIndex(BaseColumns._ID);
        String query = c.getString(it1);
        openResult(null, query);
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader, so we don't care about the ID.
        // First, pick the base URI to use depending on whether we are
        // currently filtering.
        Uri baseUri = SearchProvider.SEARCH_URI;
        String[] selectionArgs = new String[1];
        if (mCurFilter != null)
            selectionArgs[0] = mCurFilter;
        else
            selectionArgs[0] = "";
        String selection = " ?";
        return new CursorLoader(this, baseUri,
                null, selection, selectionArgs, null);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

    }

    @Override
    public void onTextChanged(CharSequence newText, int i, int i2, int i3) {
        // Called when the action bar search text has changed.  Update
        // the search filter, and restart the loader to do a new query
        // with this filter.
        String t = newText.toString();
        String newFilter = !TextUtils.isEmpty(t) ? t : null;
        // Don't do anything if the filter hasn't actually changed.
        // Prevents restarting the loader when restoring state.
        if (mCurFilter == null && newFilter == null) {
            return;
        }
        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return;
        }
        mCurFilter = newFilter;
        progressbar.setVisibility(View.VISIBLE);
        if (newFilter != null) {
            mAdapter.swapCursor(null);
            emptyText.setVisibility(View.GONE);
        } else {
            emptyText.setText(Html.fromHtml("<font color=\"#949494\">Search by typing something in the search box.</font>"));
            emptyText.setVisibility(View.VISIBLE);
        }
        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public boolean onEditorAction(TextView textView, int imeAction, KeyEvent keyEvent) {
        if (imeAction == EditorInfo.IME_ACTION_SEARCH) {
            String query = textView.getText().toString();
            showResults(query);
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Shows the soft keyboard
     */
    public void showSoftKeyboard(EditText view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        //    inputMethodManager.showSoftInput(view,InputMethodManager.SHOW_FORCED);
    }

    public MemoryCache getMemoryCache() {
//        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getFragmentManager());
//        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }
}