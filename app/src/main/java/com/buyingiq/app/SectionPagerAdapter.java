package com.buyingiq.app;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.buyingiq.app.entities.Product;

import java.util.Locale;

/**
 * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionPagerAdapter extends FragmentPagerAdapter {


    public static final int TOTAL = 5;
    public static final int POSITION_OVERVIEW = 0;
    public static final int POSITION_PRICES = 1;
    public static final int POSITION_REVIEWS = 2;
    public static final int POSITION_SPECIFICATIONS = 3;
    public static final int POSITION_ALTERNATIVES = 4;
    Context mContext;
    Product mProduct;

    public SectionPagerAdapter(Context context, FragmentManager fm, Product product) {
        super(fm);
        mProduct = product;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case POSITION_OVERVIEW:
                return ProductOverviewFragment.newInstance(mProduct);
            //    return ProductSpecificationsFragment.newInstance(mProduct);
            case POSITION_PRICES:
                return ProductPricesFragment.newInstance(mProduct.mID);
            case POSITION_REVIEWS:
                //        return ProductReviewsFragmentOld.newInstance(mProduct.mID);
                return ProductReviewsFragment.newInstance(mProduct.mID);
            case POSITION_SPECIFICATIONS:
//                    return ProductOverviewFragment.newInstance(mProduct);
                return ProductSpecificationsFragment.newInstance(mProduct.mID);
            case POSITION_ALTERNATIVES:
                return ProductAlternativesFragment.newInstance(mProduct.mID);
        }

        return null;
    }

    @Override
    public int getCount() {
        return TOTAL;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case POSITION_OVERVIEW:
                return mContext.getString(R.string.title_section_overview).toUpperCase(l);
            case POSITION_PRICES:
                return mContext.getString(R.string.title_section_compare_prices).toUpperCase(l);
            case POSITION_REVIEWS:
                return mContext.getString(R.string.title_section_reviews).toUpperCase(l);
            case POSITION_SPECIFICATIONS:
                return mContext.getString(R.string.title_section_specifications).toUpperCase(l);
            case POSITION_ALTERNATIVES:
                return mContext.getString(R.string.title_section_alternatives).toUpperCase(l);
        }
        return null;
    }
}
