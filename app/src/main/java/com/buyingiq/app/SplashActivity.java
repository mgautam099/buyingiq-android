package com.buyingiq.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;

import com.buyingiq.app.entities.Notif;
import com.buyingiq.app.resources.StringUtils;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;


public class SplashActivity extends Activity {

    SplashTask st;
    Tracker t;
    private String screenName = "SplashScreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash2);
        drawSplashScreen((ImageView) findViewById(R.id.splash_view));
        st = new SplashTask(this);
        st.execute();
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.GLOBAL_TRACKER);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onHomeLoadingComplete(ArrayList<Notif> notifs) {
        Intent i = new Intent(this, ProductListingActivity.class);
        i.setAction(BaseActivity.ACTION_SPLASH);
        if (notifs != null && notifs.size() > 0)
            i.putParcelableArrayListExtra(StringUtils.KEY_NOTIFICATION_LIST, notifs);
        finish();
        startActivity(i);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        st.cancel(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        t.setScreenName(screenName);
    }

    /*class SplashTask1 extends AsyncTask<Void, Void, Void> {

        Context mContext;

        SplashTask1(Context context) {
            mContext = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent i = new Intent(mContext, ProductListingActivity.class);
            i.setAction(BaseActivity.ACTION_SPLASH);
            finish();
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    }*/

    public void drawSplashScreen(ImageView splashImage) {
        float h = getResources().getDisplayMetrics().heightPixels;
        int[] colors = {getResources().getColor(R.color.splash_center), getResources().getColor(R.color.splash_side)};
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TL_BR, colors);
        gd.setGradientType(GradientDrawable.RADIAL_GRADIENT);
        float radius = 0.90f * h;
        gd.setGradientRadius(radius);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            splashImage.setBackgroundDrawable(gd);
        } else
            splashImage.setBackground(gd);
    }

}
