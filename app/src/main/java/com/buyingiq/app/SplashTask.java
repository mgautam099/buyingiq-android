package com.buyingiq.app;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Pair;

import com.buyingiq.app.entities.Banner;
import com.buyingiq.app.entities.Notif;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.ProductDatabaseHelper;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.google.android.gms.analytics.Tracker;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Task to load basic elements of home screen. This task will not
 * block user experience and will load only in background.
 */
class SplashTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "SplashTask";
    private static final long SPLASH_DISPLAY_TIME = 2000;
    private final Tracker t;
    Context mContext;
    private ArrayList<Banner> mBannerList;
    private ArrayList<Notif> mNotifications;

    SplashTask(Activity context) {
        mContext = context;
        mBannerList = new ArrayList<>();
        t = ((BuyingIQApp) context.getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    protected Void doInBackground(Void... params) {
        long startTime = System.currentTimeMillis();
        if (NetworkUtils.isNetworkConnected(mContext)) {
            try {
                NetworkUtils networkUtils = new NetworkUtils(t);
                ArrayList<Pair<String,String>> pairs = new ArrayList<>();
                pairs.add(new Pair<>("app_ver", "" + BuildConfig.VERSION_CODE));
                pairs.add(new Pair<>("android_ver", Build.VERSION.RELEASE));
                pairs.add(new Pair<>("code", Build.MODEL));
                String json = networkUtils.makeServiceCall(StringUtils.BASE_URL + "home/", NetworkUtils.GET, pairs);
                if (!json.contains("ERROR")) {
                    parseJson(json);
                    saveBanners();
                    downloadBanners();
                }
            } catch (IOException e) {
                Utils.Print.e(TAG, "IOException: " + e.getMessage());
                Utils.logException(e);
//                Utils.sendException(t, TAG, "IOException", e.getMessage());
            }
        }
        long runTime = System.currentTimeMillis() - startTime;
        if (runTime < SPLASH_DISPLAY_TIME) {
            try {
                Thread.sleep(SPLASH_DISPLAY_TIME - runTime);
            } catch (InterruptedException e) {
                Utils.Print.e(TAG, e.getMessage());
                Utils.logException(e);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        ((SplashActivity) mContext).onHomeLoadingComplete(mNotifications);
    }

    private void parseJson(String json) throws IOException {
        JsonFactory factory = ((BuyingIQApp) ((Activity) mContext).getApplication()).getFactory();
        JsonParser jp = factory.createParser(json);
        jp.nextToken();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String field = jp.getCurrentName();
            jp.nextToken();
            switch (field) {
                case StringUtils.TAG_BANNERS:
                    readBanners(jp);
                    break;
                /*case StringUtils.TAG_TRENDING:
                    jp.skipChildren();
                    break;*/
                case StringUtils.TAG_NOTIFICATIONS:
                    readNotifications(jp);
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
    }

    private void readNotifications(JsonParser jp) throws IOException {
        if (mNotifications == null)
            mNotifications = new ArrayList<>();
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            Notif notif = Notif.createFromJson(jp);
            mNotifications.add(notif);
        }
    }

    private void readBanners(JsonParser jp) throws IOException {
        mBannerList.clear();
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            //Assumes banners are in sorted order of their rank
            Banner b = Banner.createFromJson(jp);
            mBannerList.add(b);
        }
    }

    private void saveBanners() {
        if (mBannerList != null) {
            ProductDatabaseHelper dh = new ProductDatabaseHelper(mContext);
            SQLiteDatabase db = dh.getWritableDatabase();
            ContentValues values = new ContentValues();
            db.delete(ProductDatabaseHelper.BANNER_TABLE_NAME, ProductDatabaseHelper.KEY_LOCATION + "=?",
                    new String[]{ProductListingActivity.HOME_LOCATION});
            for (Banner b : mBannerList) {
                values.put(ProductDatabaseHelper.KEY_NAME, b.text);
                values.put(ProductDatabaseHelper.KEY_IMAGE, b.image);
                values.put(ProductDatabaseHelper.KEY_URL, b.link);
//                values.put(ProductDatabaseHelper.KEY_RANK, b.rank);
                values.put(ProductDatabaseHelper.KEY_LOCATION, ProductListingActivity.HOME_LOCATION);
                db.insert(ProductDatabaseHelper.BANNER_TABLE_NAME, null, values);
            }
            db.close();
            dh.close();
        }
    }

   /* public void saveNotifications(){

    }*/

    private void downloadBanners() {
        for (Banner b : mBannerList) {
            NetworkUtils networkUtils = new NetworkUtils(t);
            networkUtils.getImage(b.image, mContext);
        }
    }
}
