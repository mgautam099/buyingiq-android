package com.buyingiq.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.buyingiq.app.entities.Store;
import com.buyingiq.app.entities.StoreReview;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 */
public class StoreActivity extends NonNavigationActivity implements StoreReviewsFragment.OnFragmentInteractionListener {

    private Store mStore;
    StoreDownTask storeDownTask;
    Tracker t;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_loading);
        super.onCreate(savedInstanceState);
        t = ((BuyingIQApp) getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if(Intent.ACTION_VIEW.equals(intent.getAction()))
        {
            progressDialog = ProgressDialog.show(this, null, "Please Wait", true, false);
            storeDownTask = new StoreDownTask(this);
            storeDownTask.execute(intent.getData().getPathSegments().get(1));
            super.mTitle = intent.getData().getPathSegments().get(1)+ " Reviews";
        }
        else {
            mStore = intent.getParcelableExtra(StringUtils.ARG_STORE);
            super.mTitle = mStore.name + " Reviews";
            displayPage();
        }

    }

    public void displayPage()
    {
        setContentView(R.layout.activity_store);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, StoreReviewsFragment.newInstance(mStore))
                .commit();
    }

    @Override
    public void onReviewSelected(StoreReview r, View view) {
        Intent i = new Intent(this, FullStoreReviewActivity.class);
        i.putExtra("section_label", mStore.name + " Reviews");
        i.putExtra(StoreReviewsFragment.TAG_REVIEW, r);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_right, R.anim.activity_exit);
    }

    /*public MemoryCache getMemoryCache() {
        RetainFragment retainFragment = RetainFragment.findOrCreateRetainFragment(getSupportFragmentManager());
        mMemoryCache = retainFragment.mRetainedCache;
        if (mMemoryCache == null)
            mMemoryCache = new MemoryCache();
        return mMemoryCache;
    }*/

    private class StoreDownTask extends AsyncTask<String, Void, Integer> {

        String TAG = "StoreDownTask";
        Context mContext;

        public StoreDownTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            NetworkUtils.networkCheck((NonNavigationActivity) mContext);
        }

        @Override
        protected Integer doInBackground(String... strings) {
            NetworkUtils networkUtils = new NetworkUtils(t);
            String url = StringUtils.BASE_URL + "stores/" + strings[0] + "/";
            String json = networkUtils.makeServiceCall(url, NetworkUtils.GET);
            if (json != null && !json.contains("ERROR")) {
                try {
                    mStore = Store.createFromJson(new JSONObject(json));
                } catch (JSONException e) {
                    Utils.Print.e(TAG,"JSONException: "+e.getMessage());
                    Utils.logException(e);
                    return -2;
                }
            } else
                return -1;
            return 0;
        }

        @Override
        protected void onPostExecute(Integer str) {
            if (str == 0)
                displayPage();
            else {
                setTitle("Seller Loading Failed");
                showDialog("Unable to load seller. Please check your network and try again later.");
            }
            if (progressDialog != null)
                progressDialog.cancel();
        }


        public void showDialog(String alertMessage) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(mContext).setMessage(alertMessage)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((Activity) mContext).finish();
                        }
                    });
            dialog.show();
        }
    }
}
