package com.buyingiq.app;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.buyingiq.app.entities.Rating;
import com.buyingiq.app.entities.Store;
import com.buyingiq.app.entities.StoreReview;
import com.buyingiq.app.entities.User;
import com.buyingiq.app.listadapters.StoreReviewsAdapter;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.NetworkUtils;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.resources.Utils;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;


/**
 * A simple {@link android.support.v4.app.ListFragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.buyingiq.app.StoreReviewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.buyingiq.app.StoreReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreReviewsFragment extends ListFragment implements AbsListView.OnScrollListener {

    //Not really needed in this case, but will keep for future reference
    private static final int FLAG_LOAD_RATINGS = 1;
    private static final int FLAG_RELOAD_REVIEWS = 1 << 1;
    private static final int FLAG_LOAD_NEXT = 1 << 2;
    private static final int FLAG_HAS_AVERAGE_RATING = 1 << 3;

    private static final String TAG = "StoreReviewsFragment";
    private static final String TAG_SHOP_AGAIN_PERC = "shop_again_perc";
    private static final String TAG_REVIEWS = "reviews";
    public static String TAG_NEXT = "next";
    public static String TAG_REVIEW = "review";
    public static String TAG_RATINGS = "ratings";
    public static String TAG_USER = "user";
    public static String TAG_NAME = "name";
    public static String TAG_TOTAL_VOTES = "total_votes";
    public static String TAG_ADDED_ON = "added_on";
    public static String apiUrl;
    public static String selected_sort = "upvotes";
    public ArrayList<StoreReview> storeReviews;
    public View mHeader;
    public String next;
    TextView footerView;
    boolean loaded = false;
    boolean updating = false;
    Spinner.OnItemSelectedListener mSpinnerItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            String sort_by = getResources().getStringArray(R.array.sort_by_params)[i];
            if (!sort_by.equals(selected_sort)) {
                String url = apiUrl + "?sort_by=" + sort_by + "&sort_order=desc";
                //We need to reload only reviews here, so flag to reload will be called.
                LoadReviewsTask lrt =
                        new LoadReviewsTask(getActivity(), FLAG_RELOAD_REVIEWS);
                updating = true;
                lrt.execute(url);
                tasks.add(lrt);
                selected_sort = sort_by;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    ArrayList<LoadReviewsTask> tasks;
    String[] revs = {"OVERALL", "AUTHENTICITY", "ISSUE RESOLVING", "DELIVERY"};
    private Store mStore;
    private Spinner sortSpinner;
    private StoreReviewsAdapter reviewsAdapter;
    private OnFragmentInteractionListener mListener;
    View.OnClickListener reviewListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            StoreReview r = (StoreReview) view.getTag();
            if (mListener != null)
                mListener.onReviewSelected(r, (View) view.getParent());
        }
    };
    private int total;
    private Tracker t;

    public StoreReviewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param store Store to be displayed
     * @return A new instance of fragment StoreReviewsFragment.
     */
    public static StoreReviewsFragment newInstance(Store store) {
        StoreReviewsFragment fragment = new StoreReviewsFragment();
        Bundle args = new Bundle();
        args.putParcelable(StringUtils.ARG_STORE, store);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mStore = getArguments().getParcelable(StringUtils.ARG_STORE);
            apiUrl = String.format(StringUtils.BASE_URL + "stores/%s/reviews/", mStore.getUri());
        }
        storeReviews = new ArrayList<>();
        reviewsAdapter = new StoreReviewsAdapter(getActivity(), storeReviews, reviewListener);
        tasks = new ArrayList<>();
        t = ((BuyingIQApp) getActivity().getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //its a simple list so why not use the already existing listview.
        View view = inflater.inflate(R.layout.fragment_product_reviews, container, false);
        return view;
    }

    public void init() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.item_store_review_header, getListView(), false);
        sortSpinner = (Spinner) view.findViewById(R.id.review_sort_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_by_display, R.layout.item_spinner_textview);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(adapter);
        footerView = (TextView) inflater.inflate(R.layout.overscroll_footer, null, false);
        getListView().addHeaderView(view);
        getListView().addFooterView(footerView);
        getListView().setOnScrollListener(this);
        view.setVisibility(View.GONE);
        footerView.setVisibility(View.GONE);
        setListAdapter(reviewsAdapter);
        mHeader = view;
        setStore();
    }

    public void setStore() {
        new ImageLoader1(getActivity(), 0).DisplayImage(mStore.getImageLarge(), (ImageView) mHeader.findViewById(R.id.store_image));
        ((TextView) mHeader.findViewById(R.id.shop_again_perc)).setText(mStore.getShopAgainPerc() + "% will shop here again");
        ((RatingBar) mHeader.findViewById(R.id.store_rating1)).setRating(Float.parseFloat(mStore.getRatingOverall()));
        ((RatingBar) mHeader.findViewById(R.id.store_rating2)).setRating(Float.parseFloat(mStore.getRatingDelivery()));
        ((RatingBar) mHeader.findViewById(R.id.store_rating3)).setRating(Float.parseFloat(mStore.getRatingAuth()));
        ((RatingBar) mHeader.findViewById(R.id.store_rating4)).setRating(Float.parseFloat(mStore.getRatingIssue()));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        //Initialization of reviews, need to load both reviews and ratings
        LoadReviewsTask lrt = new LoadReviewsTask(getActivity(), FLAG_LOAD_RATINGS | FLAG_RELOAD_REVIEWS);
        updating = true;
        lrt.execute(apiUrl);
        sortSpinner.setOnItemSelectedListener(mSpinnerItemSelectedListener);
        tasks.add(lrt);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("KEY", "value");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int first, int visible, int total) {
        if (first + visible >= total && total != 0) {
            if (next != null)
                if (next.length() > 1) {
                    if (!updating) {
                        updating = true;
                        LoadReviewsTask asyncTask = new LoadReviewsTask(getActivity(), FLAG_LOAD_NEXT);
                        updating = true;
                        asyncTask.execute(apiUrl + "?" + next);
                        tasks.add(asyncTask);
                    }
                }
        }
    }

    @Override
    public void onDestroyView() {
        for (LoadReviewsTask t : tasks) {
            t.cancel(true);
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onReviewSelected(StoreReview r, View view);
    }

    private class LoadReviewsTask extends AsyncTask<String, StoreReview, Integer> {
        private final Context mContext;
        private int mFlags;

        LoadReviewsTask(Context context, int flags) {
            mContext = context;
            mFlags = flags;
        }

        @Override
        protected void onPreExecute() {
            if ((mFlags & StoreReviewsFragment.FLAG_RELOAD_REVIEWS) != 0) {
                storeReviews.clear();
                reviewsAdapter.notifyDataSetChanged();
            }
        }

        @Override
        protected Integer doInBackground(String... args) {
            if (!NetworkUtils.isNetworkConnected(mContext))
                return -1;
            NetworkUtils networkUtils = new NetworkUtils(t);
            String buff = networkUtils.makeServiceCall(args[0], NetworkUtils.GET);
            try {
                if (buff != null && !buff.contains("ERROR")) {
                    JSONObject json = new JSONObject(buff);
                    total = json.getInt(StringUtils.TAG_TOTAL);
                    JSONArray reviewsArray = json.getJSONArray(TAG_REVIEWS);
                    readReviews(reviewsArray);
                    next = json.getString(TAG_NEXT);
                    if (next.length() < 1 || next.equals("null"))
                        next = null;
                    if (isCancelled())
                        return -3;
                } else {
                    Utils.Print.e("StoreReviewsFragment", "Got null json");
                    return -2;
                }
            } catch (JSONException e) {
                Utils.Print.e("StoreReviewsFragment", "JSONException: " + e.getMessage());
                Utils.logException(e);
//                Utils.sendException(t, TAG, "JSONException", args[0] + ":" + e.getMessage());
                return -1;
            }
            return 0;
        }

        @Override
        protected void onProgressUpdate(StoreReview... values) {
        }

        @Override
        protected void onPostExecute(Integer returnValue) {
            if (returnValue.equals(0)) {
                if (storeReviews.size() > 0) {
                    getListView().setVisibility(View.VISIBLE);
                    ((TextView) getListView().getEmptyView()).setText("");
                } else {
                    getListView().setVisibility(View.INVISIBLE);
                    ((TextView) getListView().getEmptyView()).setText("No Reviews for this Store");
                }
                reviewsAdapter.notifyDataSetChanged();
                if (mHeader != null && mHeader.getVisibility() != View.VISIBLE)
                    mHeader.setVisibility(View.VISIBLE);
                ((TextView) mHeader.findViewById(R.id.review_count)).setText("Showing " + total + " Reviews");
                if (next != null && !next.equals("null") && next.length() > 1) {
                    footerView.setText("Loading more Reviews");
                    footerView.setVisibility(View.VISIBLE);
                } else {
                    //    footerView.setText("End of Reviews");
                    footerView.setVisibility(View.GONE);
                }
                //        getView().findViewById(R.id.loading_progressbar).setVisibility(View.GONE);
                View progress = getView().findViewById(android.R.id.progress);
                if (progress != null)
                    progress.setVisibility(View.GONE);
                loaded = true;
                updating = false;
            } else {
                NetworkUtils.networkCheck((StoreActivity) mContext);
            }

        }

        private void readReviews(JSONArray reviewsArray) throws JSONException {
            for (int i = 0; i < reviewsArray.length(); i++) {
                StoreReview r = getReview(reviewsArray.getJSONObject(i));
                storeReviews.add(r);
            }
        }

        private StoreReview getReview(JSONObject reviewObject) throws JSONException {
            StoreReview r = new StoreReview();
            r.setUser(User.createFromJson(reviewObject.getJSONObject(TAG_USER)));
            r.setAddedOn(reviewObject.getString(TAG_ADDED_ON));
            String d = "";
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date date = sdf.parse(r.getAddedOn());
                sdf.applyPattern("MMM ''yy");
                d = sdf.format(date);
            } catch (ParseException e) {
                Utils.Print.e(TAG, "Date format unknown: " + r.getAddedOn());
                Utils.logException(e);
//                Utils.sendException(t, TAG, "DateFormatUnknown", r.getAddedOn());
            }
            r.setAddedOnStr(d);
            r.setRatings(getRatings(reviewObject.getJSONObject(TAG_RATINGS)));
            r.setReview(reviewObject.getString(TAG_REVIEW));
            r.setUpvotes(reviewObject.getInt(StringUtils.TAG_UPVOTES));
            r.setTotalVotes(reviewObject.getInt(TAG_TOTAL_VOTES));
            return r;
        }

        private ArrayList<Rating> getRatings(JSONObject json) throws JSONException {
            ArrayList<Rating> r = new ArrayList<Rating>();
            Iterator<String> keys = json.keys();
            int i = 0;
            while (keys.hasNext()) {
                Rating ra = new Rating();
                ra.head = revs[i++];
                ra.score = json.getInt(keys.next());
                r.add(ra);
            }
            return r;
        }
    }
}
