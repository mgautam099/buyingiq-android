package com.buyingiq.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.StringUtils;

/**
 * This Activity is only for the purpose of testing and debugging, it shall be stripped from the production version.
 */
public class TestActivity extends BaseActivity implements ProductAlternativesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, ProductAlternativesFragment.newInstance("14447"))
                .commit();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (position != 0) {
            Intent i = new Intent(this, ProductListingActivity.class);
            i.setAction(ProductListingActivity.ACTION_HOME_NAV);
            i.putExtra(StringUtils.ARG_NAV_CAT, position);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    }


    /*@Override
    public void onReviewSelected(StoreReview r, View view) {

    }*/

    @Override
    public void onAlternativeSelected(Product p) {

    }
}
