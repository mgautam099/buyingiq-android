package com.buyingiq.app.entities;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;

/**
 * An object to store banner attributes.
 */
public class Banner implements Parcelable {

    public static final Creator<Banner> CREATOR = new Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel source) {
            return new Banner(source);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };
    public String text;
    public String image;
    public String link;
//    public int rank;

    /**
     *
     * @param p can be null if initializing
     */
    private Banner(Parcel p) {
        if(p!=null)
        {
            text = p.readString();
            image = p.readString();
            link = p.readString();
        }
//        rank = p.readInt();
    }

    public static Banner createFromJson(JsonParser jp) throws IOException {
        Banner b = new Banner(null);
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String field = jp.getCurrentName();
            jp.nextToken();
            switch (field) {
                case StringUtils.TAG_TEXT:
                    b.text = jp.getText();
                    break;
                case StringUtils.TAG_IMAGE:
                    b.image = jp.getText();
                    break;
                case StringUtils.TAG_LINK:
                    b.link = jp.getText();
                    break;
//                case StringUtils.TAG_RANK:
//                    b.rank = jp.getIntValue();
//                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return b;
    }

    public static Banner createFromCursor(Cursor c) {
        Banner b = new Banner(null);
        b.text = c.getString(0);
        b.image = c.getString(1);
        b.link = c.getString(2);
//        b.rank = c.getInt(3);
        return b;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeString(image);
        dest.writeString(link);
//        dest.writeInt(rank);
    }
}
