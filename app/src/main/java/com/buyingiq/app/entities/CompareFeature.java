package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;

/**
 * Entity representing a Compare Feature
 */
public class CompareFeature implements Parcelable {

    public static final Creator<CompareFeature> CREATOR = new Creator<CompareFeature>() {
        @Override
        public CompareFeature createFromParcel(Parcel parcel) {
            return new CompareFeature(parcel);
        }

        @Override
        public CompareFeature[] newArray(int i) {
            return new CompareFeature[i];
        }
    };
    public String name;
    public String product1;
    public String product2;

    private CompareFeature(@Nullable Parcel parcel) {
        if(parcel!=null){
            name = parcel.readString();
            product1 = parcel.readString();
            product2 = parcel.readString();
        }
    }

    public static CompareFeature createFromJSON(JsonParser jp, String id1, String id2) throws IOException {
        CompareFeature cf = new CompareFeature(null);
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            if (StringUtils.TAG_NAME.equals(fieldName))
                cf.name = jp.getText();
            else if (id1.equals(fieldName))
                cf.product1 = jp.getText();
            else if (id2.equals(fieldName))
                cf.product2 = jp.getText();
        }
        return cf;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(product1);
        parcel.writeString(product2);
    }
/*
    public String product1 {
        return product1;
    }

    public String product2 {
        return product2;
    }*/

}
