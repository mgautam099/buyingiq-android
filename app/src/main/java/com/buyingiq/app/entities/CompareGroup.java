package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Entity representing a Group for compare api
 */
public class CompareGroup implements Parcelable {

    public static final Creator<CompareGroup> CREATOR = new Creator<CompareGroup>() {
        @Override
        public CompareGroup createFromParcel(Parcel parcel) {
            return new CompareGroup(parcel);
        }

        @Override
        public CompareGroup[] newArray(int i) {
            return new CompareGroup[i];
        }
    };
    public String name;
    public ArrayList<CompareFeature> compareFeatures;

    private CompareGroup(@Nullable Parcel parcel) {
        compareFeatures = new ArrayList<>();
        if (parcel!=null){
            name = parcel.readString();
            parcel.readTypedList(compareFeatures, CompareFeature.CREATOR);
        }
    }

    public static CompareGroup createFromJSON(JsonParser jp, String id1, String id2) throws IOException {
        CompareGroup cg = new CompareGroup(null);
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            switch (fieldName) {
                case StringUtils.TAG_NAME:
                    cg.name = jp.getText();
                    break;
                case StringUtils.TAG_FEATURES:
                    cg.populateFeatureList(jp, id1, id2);
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return cg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeTypedList(compareFeatures);
    }

    private void populateFeatureList(JsonParser jp, String id1, String id2) throws IOException {
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            compareFeatures.add(CompareFeature.createFromJSON(jp, id1, id2));
        }
    }
}
