package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A class to representing a Deal from a particular store.
 * This class follows a Relational model between store and deal, rather than considering a Deal as a Direct part of store.
 */
public class Deal implements Parcelable {

    public static final Creator<Deal> CREATOR = new Creator<Deal>() {
        @Override
        public Deal createFromParcel(Parcel parcel) {
            return new Deal(parcel);
        }

        @Override
        public Deal[] newArray(int i) {
            return new Deal[i];
        }
    };
    public String name;
    public String offer;
    public String stockInfo;
    //TODO create a more elegant solution to this
    public int stockInt;
    public String deliveryTime;
    public String basePrice;
    public String cashBack;
    public String shipCost;
    public String url;
    public String redirectHash;
    public String[] stockStrings = {"Coming Soon", "In Stock", "Available", "Out of Stock", "Unknown", "Discontinued"};

    public Deal(String name) {
        this.name = name;
//        coupons = new ArrayList<String>();
    }

    public Deal(Parcel in) {
        name = in.readString();
        offer = in.readString();
        stockInfo = in.readString();
        deliveryTime = in.readString();
        basePrice = in.readString();
        cashBack = in.readString();
        shipCost = in.readString();
        url = in.readString();
        redirectHash = in.readString();
        stockInt = in.readInt();
//        effPrice = parcel.readString();
//        parcel.readStringList(coupons);
    }

    public static Deal createFromJson(JSONObject dealObject) throws JSONException {
        Deal deal = new Deal(dealObject.getString(StringUtils.TAG_NAME));
        deal.offer = dealObject.getString(StringUtils.TAG_OFFER);
        deal.deliveryTime = dealObject.getString(StringUtils.TAG_DELIVERY_TIME);
        deal.basePrice = dealObject.getString(StringUtils.TAG_BASE_PRICE_STR);
        deal.cashBack = dealObject.getString(StringUtils.TAG_CASHBACK_STR);
        deal.shipCost = dealObject.getString(StringUtils.TAG_SHIP_COST_STR);
        deal.url = dealObject.getString(StringUtils.TAG_URL);
        deal.redirectHash = dealObject.getString(StringUtils.TAG_REDIRECT_HASH);
        deal.setStockInfo(dealObject.getString(StringUtils.TAG_STOCK_INFO));
        return deal;
//        deal.effPrice = dealObject.getString(TAG_EFF_PRICE_STR);
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(name);
        out.writeString(offer);
        out.writeString(stockInfo);
        out.writeString(deliveryTime);
        out.writeString(basePrice);
        out.writeString(cashBack);
        out.writeString(shipCost);
        out.writeString(url);
        out.writeString(redirectHash);
        out.writeInt(stockInt);
//        in.writeString(effPrice);
//        in.writeStringList(coupons);
    }

    public void setStockInfo(String stockInfo) {
        this.stockInfo = stockInfo;
        if (stockInfo.equals(stockStrings[0]) || stockInfo.equals(stockStrings[1]) || stockInfo.equals(stockStrings[2]))
            stockInt = 1;
        else
            stockInt = 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    //    private Enum<String> stockInfo;
//    private String effPrice;
    //    private ArrayList<String> coupons;


    //    public ArrayList<String> getCoupons() {
//        return coupons;
//    }

//    public void addCoupon(ArrayList<String> coupons) {
//        this.coupons.addAll(coupons);
//    }

//    public void addCoupon(String coupon) {
//        coupons.add(coupon);
//    }

}
