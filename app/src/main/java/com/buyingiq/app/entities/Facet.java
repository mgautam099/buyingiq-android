package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;

public class Facet implements Parcelable {


    public static final Creator<Facet> CREATOR = new Creator<Facet>() {
        @Override
        public Facet createFromParcel(Parcel parcel) {
            return new Facet(parcel);
        }

        @Override
        public Facet[] newArray(int i) {
            return new Facet[i];
        }
    };
    public String label;
    public String tag;
    public int selected = -1;
    public int count;
    //Every facet has a parent folder.
    public Folder parent;

    //    OnFacetClickListener mListener;
    public Facet(Parcel parcel) {
        label = parcel.readString();
        tag = parcel.readString();
        selected = parcel.readInt();
        count = parcel.readInt();
    }

    public Facet(String label, String tag, int selected) {
        this.label = label;
        this.tag = tag;
        this.selected = selected;
    }

    public Facet(Folder parent) {
        this.parent = parent;
    }

    public static Facet createFromJSON(JsonParser jp, Folder parent) throws IOException {
        Facet f = new Facet(parent);
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            //    System.out.println(fieldName);
            switch (fieldName) {
                case StringUtils.TAG_LABEL:
                    f.label = jp.getText();
                    break;
                case StringUtils.TAG_TAG:
                    f.tag = jp.getText();
                    break;
                case StringUtils.TAG_IS_SELECTED:
                    if (jp.getValueAsBoolean())
                        f.select();
                    else
                        f.deselect();
                    break;
                case StringUtils.TAG_COUNT:
                    f.count = jp.getValueAsInt();
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return f;
    }

    public int isSelected() {
        return selected;
    }

    public void select() {
        selected = 1;
        if (parent != null)
            parent.incrementSelectedCount();
    }

    public void deselect() {
        if (parent != null && selected != -1)
            parent.decrementSelectedCount();
        selected = 0;
    }

/*
    public String getLabel() {
        return label;
    }

    public String getTag() {
        return tag;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }
*/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(label);
        parcel.writeString(tag);
        parcel.writeInt(selected);
        parcel.writeInt(count);
    }

    /*
    public interface OnFacetClickListener
    {
        public void onFacetClick(boolean isSelected);
    }

    public void setListener(OnFacetClickListener listener)
    {
        mListener = listener;
    }  */
}