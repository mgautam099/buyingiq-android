package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Folder implements Parcelable {
    public static final Creator<Folder> CREATOR = new Creator<Folder>() {
        @Override
        public Folder createFromParcel(Parcel parcel) {
            return new Folder(parcel);
        }

        @Override
        public Folder[] newArray(int i) {
            return new Folder[i];
        }
    };
    //    private int FLAG_SELECTED = 1 << 0;
//    private int flags;
    private OnSelectedChangeListener mListener;
    private int guid;
    public String name;
    private int selectedCount = 0;
    public ArrayList<Facet> facets;

    public Folder(int guid) {
        this.guid = guid;
        facets = new ArrayList<>();
    }

    public Folder(Parcel p) {
        guid = p.readInt();
        name = p.readString();
        selectedCount = p.readInt();
        facets = new ArrayList<Facet>();
        p.readTypedList(facets, Facet.CREATOR);
        setParent();
    }

    public Folder(int index, String name) {
        guid = index;
        this.name = name;
        facets = new ArrayList<Facet>();
    }

    /**
     * Create a new Folder object from {@link com.fasterxml.jackson.core.JsonParser}
     *
     * @param jp   The JsonParser object to extract the Folder object from.
     * @param guid The index of folder in Json. This is used as GUID of the folder.
     * @return A folder object
     * @throws IOException
     */
    public static Folder createFromJSON(JsonParser jp, int guid, HashMap<String, Integer> categoryMap) throws IOException {
        Folder f = new Folder(guid);
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            if (StringUtils.TAG_FACETS.equals(fieldName))
                f.populateFacetList(jp, categoryMap);
            else if (StringUtils.TAG_NAME.equals(fieldName))
                f.name = ((guid == 1 ? "   " : "") + jp.getText().toUpperCase());
        }
        return f;
    }

   /* public static void populateFolderList(JsonParser jp, ArrayList<Folder> folderList, boolean isSearch, boolean facetLoad, OnSelectedChangeListener listener) throws IOException {
        int i = 0;
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            if (i == 0 && !isSearch)
                jp.skipChildren();
            else if (!facetLoad && i == 1)
                jp.skipChildren();
            else {
                folderList.add(createFromJSON(jp, i, listener));
            }
            i++;
        }
    }*/

    private void setParent() {
        for (Facet f : facets)
            f.parent = this;
    }

    public void setSelectedCountListener(OnSelectedChangeListener listener) {
        mListener = listener;
    }

    public void addFacet(Facet f) {
        addFacet(-1, f);
    }

    public void addFacet(int index, Facet f) {
        if (index > -1)
            facets.add(index, f);
        else
            facets.add(f);
    }

    public void addFacet(int index, String facetLabel, String facetTag) {
        addFacet(index, facetLabel, facetTag, 0, 0);
    }

    public void addFacet(int index, String facetLabel, String facetTag, int selected, int count) {
        Facet f = new Facet(facetLabel, facetTag, selected);
        f.count = count;
        addFacet(index, f);
    }

    public void addFacet(String facetLabel, String facetTag) {
        addFacet(-1, facetLabel, facetTag, 0, 0);
    }

    public void addFacet(String facetLabel, String facetTag, int isSelected) {
        addFacet(-1, facetLabel, facetTag, isSelected, 0);
    }

//    public void setName(String name) {
//        this.name = name;
//    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(guid);
        parcel.writeString(name);
        parcel.writeInt(selectedCount);
        parcel.writeTypedList(facets);
    }

    public void addFacet(String label, String tag, int selected, int count) {
        addFacet(-1, label, tag, selected, count);
    }

    private void populateFacetList(JsonParser jp, HashMap<String, Integer> categoryMap) throws IOException {

        //Used in case we want to display 0 results.
        //int zeroIndex = -1;
        //int i=0;
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            Facet f = Facet.createFromJSON(jp, this);
            if (f.count > 0) // || getGuid()>1)  //Used in case we want to display all the phone models available
            {
                //iMap is used in case the folder is created of a category from the Search
                //Used for narrowing the result.
                if (guid != 0 || categoryMap.containsKey(f.tag)) {
                    addFacet(f);
                    //Used in case we want to display 0 results.
                /*    if(zeroIndex==-1)
                    {
                        if(f.getCount()==0)
                            zeroIndex = i;
                        addFacet(f);
                    }
                    else
                    {
                        addFacet(f.getCount()>0?zeroIndex++:-1,f);
                    }*/
                }
            }
            //    i++;
        }
    }

    public void incrementSelectedCount() {
        selectedCount++;
        if (mListener != null)
            mListener.onSelectedCountChanged(1);
        //    else
        //        throw new IllegalStateException("Set selected count change listener");
    }

    public void decrementSelectedCount() {
        if (selectedCount == 0)
            return;
        selectedCount--;
        if (mListener != null)
            mListener.onSelectedCountChanged(-1);
        else
            throw new IllegalStateException("Set selected count change listener");
    }

    public int getSelectedCount() {
        return selectedCount;
    }

    public interface OnSelectedChangeListener {
        /**
         * Called when the selected count changes
         *
         * @param count Should be 1 or -1 depending on the increment or decrement
         */
        void onSelectedCountChanged(int count);
    }

//    public int getGuid() {
//        return guid;
//    }

/*    public int[] getSelectedFacets()
    {
        int[] mSel=new int[facets.size()];
        for(int i=0;i<mSel.length;i++)
        {
            mSel[i] = facets.get(i).isSelected();
        }
        return mSel;
    }


    public boolean isSelected() {
        return selected;
    }

    public void select() {
        selected=true;
    }

    public void deselect(){
        selected=false;
    }




    @Override
    public void onFacetClick(boolean isSelected) {
        int toAdd = isSelected?1:-1;
        int temp = selectedCount;
        temp+=toAdd;
        if(temp<0)
            temp=0;
        if(mListener!=null)
        {
            if(selectedCount==0&&temp>0)
                mListener.onSelectedCountChanged(guid,true);
            if(selectedCount>0&&temp==0)
                mListener.onSelectedCountChanged(guid,false);
        }
        selectedCount=temp;
    }

    */

}
