package com.buyingiq.app.entities;

/**
 * Object used to store Navigation Item Attributes
 */
public class NavigationItem {
    public boolean isExpandable;
    public String mTag;
    public String mLabel;
    public int mImageID;
    public int mParent;

    public NavigationItem(String tag, String label, int imageId, int parent) {
        mTag = tag;
        mLabel = label;
        mImageID = imageId;
        mParent = parent;
    }
/*

    public int getParent() {
        return mParent;
    }

    public int getImageID() {
        return mImageID;
    }

    public void setImageID(int imageID) {
        mImageID = imageID;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    public String getTag() {
        return mTag;
    }

    public void setTag(String tag) {
        mTag = tag;
    }

    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean isExpandable) {
        this.isExpandable = isExpandable;
    }
*/

}
