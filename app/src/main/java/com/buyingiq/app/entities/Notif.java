package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;

/**
 * Created by Mayank on 1/8/2015.
 */
public class Notif implements Parcelable {
    public static final byte UPDATE_CRITICAL = 0;
    public static final byte UPDATE = 1;
    public static final byte NOTIFICATION = 2;
    public static final Creator<Notif> CREATOR = new Creator<Notif>() {
        @Override
        public Notif createFromParcel(Parcel source) {
            return new Notif(source);
        }

        @Override
        public Notif[] newArray(int size) {
            return new Notif[size];
        }
    };
    public byte level;
    public String message;
    public String link;

    public Notif(Parcel source) {
        level = source.readByte();
        message = source.readString();
        link = source.readString();
    }

    public Notif() {
        //Required empty constructor
    }

    public static Notif createFromJson(JsonParser jp) throws IOException {
        Notif n = new Notif();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String field = jp.getCurrentName();
            jp.nextToken();
            switch (field) {
                case StringUtils.TAG_LEVEL:
                    switch (jp.getText()) {
                        case "Update Critical":
                            n.level = UPDATE_CRITICAL;
                            break;
                        case "Update":
                            n.level = UPDATE;
                            break;
                        default:
                            n.level = NOTIFICATION;
                            break;
                    }
                    break;
                case StringUtils.TAG_MESSAGE:
                    n.message = jp.getText();
                    break;
                case StringUtils.TAG_LINK:
                    n.link = jp.getText();
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return n;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(level);
        dest.writeString(message);
        dest.writeString(link);
    }
}
