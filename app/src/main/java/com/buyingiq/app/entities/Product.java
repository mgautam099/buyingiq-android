package com.buyingiq.app.entities;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseArray;

import com.buyingiq.app.ProductListingActivity;
import com.buyingiq.app.resources.StringUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.buyingiq.app.resources.StringUtils.DECIMAL_FORMAT;
import static com.buyingiq.app.resources.StringUtils.TAG_AVG_RATING;
import static com.buyingiq.app.resources.StringUtils.TAG_CATEGORY;
import static com.buyingiq.app.resources.StringUtils.TAG_ID;
import static com.buyingiq.app.resources.StringUtils.TAG_IMAGES_O;
import static com.buyingiq.app.resources.StringUtils.TAG_KEY_FEATURES;
import static com.buyingiq.app.resources.StringUtils.TAG_LARGE_IMAGE;
import static com.buyingiq.app.resources.StringUtils.TAG_MEDIUM_IMAGE;
import static com.buyingiq.app.resources.StringUtils.TAG_MIN_PRICE_STR;
import static com.buyingiq.app.resources.StringUtils.TAG_NAME;
import static com.buyingiq.app.resources.StringUtils.TAG_RATING_COUNT;
import static com.buyingiq.app.resources.StringUtils.TAG_SMALL_IMAGE;
import static com.buyingiq.app.resources.StringUtils.TAG_STORE_COUNT;
import static com.buyingiq.app.resources.StringUtils.TAG_URL;
import static com.buyingiq.app.resources.StringUtils.TAG_USER;
import static com.buyingiq.app.resources.StringUtils.TAG_XLARGE_IMAGE;

/**
 * A Parcelable {@link android.os.Parcelable} object which stores the details of the product.
 */
public class Product implements Parcelable {
    public static final int FEATURE_STR_INDEX = 2;
    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    /**
     * ID of the product
     */
    //TODO: Store all the instances of product IDs and check them for unique instance else return already present instance if available
    public String mID;
    /**
     * Name of the Product
     */
    public String name;
    //    private String brand;
    public String category;
    public int categoryID;
    /**
     *
     */
    public String price;
    public String rating;
    public String votes;
    //Should be in the format "<num> Stores"
    public String numStores;
    //    private String[]
    public ArrayList<SparseArray<ProductImage>> multipleImages;
    public String smallImageUrl;
    public String mediumImageUrl;
    public String largeImageUrl;
    public String xLargeImageUrl;
    public int listingImageIndex;
    public int overviewImageIndex;
    public int largestImageIndex;
    public ArrayList<String> keyFeatures;
    public int isAlternative = 0;
    public String[] comparisonString;
    public int[] comparisonInt;
    public boolean isSelectedForCompare;
    public String url;
    public int biqScore;
    public int curatedReviewCount;

    /**
     * A constructor to generate a new Product instance.
     *
     * @param id A unique Identifier for each product instance.
     */
    public Product(String id) {
        mID = id;
        keyFeatures = new ArrayList<>();
        multipleImages = new ArrayList<>();
        biqScore = -1;
    }

    /**
     * A constructor to retrieve a Product instance from a Parcel
     *
     * @param in A parcel containing the product details
     */
    public Product(Parcel in) {
        mID = in.readString();
        name = in.readString();
        //    brand = in.readString();
        category = in.readString();
        categoryID = in.readInt();
        price = in.readString();
        rating = in.readString();
        votes = in.readString();
        numStores = in.readString();
        multipleImages = new ArrayList<>();
        readImages(in, multipleImages);
        smallImageUrl = in.readString();
        mediumImageUrl = in.readString();
        largeImageUrl = in.readString();
        xLargeImageUrl = in.readString();
        listingImageIndex = in.readInt();
        overviewImageIndex = in.readInt();
        largestImageIndex = in.readInt();
        keyFeatures = new ArrayList<>();
        in.readStringList(keyFeatures);
        isAlternative = in.readInt();
        if (isAlternative == 1) {
            comparisonInt = new int[4];
            comparisonString = new String[4];
            in.readStringArray(comparisonString);
            in.readIntArray(comparisonInt);
        }
        isSelectedForCompare = in.readByte() != 0;
        url = in.readString();
        biqScore = in.readInt();
        curatedReviewCount = in.readInt();
    }

    /**
     * Creates a new product from a Jackson Library's {@link com.fasterxml.jackson.core.JsonParser}
     *
     * @param jp A {@link com.fasterxml.jackson.core.JsonParser} object at starting position of a product array.
     * @return A new product object.
     * @throws IOException
     */
    public static Product createFromJSON(JsonParser jp) throws IOException {
        Product p = new Product("");
        p.fillFromJson(jp);
        return p;
    }

    /**
     * Creates a new product from a Jackson Library's {@link com.fasterxml.jackson.core.JsonParser} for Search Layout.
     *
     * @param jp A {@link com.fasterxml.jackson.core.JsonParser} object at starting position of a product array.
     * @return A new product object.
     * @throws IOException
     */
    public static Product createForSearch(JsonParser jp) throws IOException {
        Product p = new Product("");
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            switch (fieldName) {
                case TAG_ID:
                    p.mID = jp.getText();
                    break;
                case TAG_NAME:
                    p.name = jp.getText();
                    break;
                case TAG_CATEGORY:
                    p.category = jp.getText();
                    break;
                case StringUtils.TAG_CATEGORY_ID:
                    p.categoryID = jp.getIntValue();
                    break;
                case TAG_IMAGES_O:
                    p.readImages(jp);
                    break;
                case StringUtils.TAG_IMAGES:
                    p.readMultipleImages(jp);
                    break;
                //    case TAG_MIN_PRICE_STR:
                //        p.price = "Rs. " + jp.getText();
                //        break;
                //    case TAG_AVG_RATING:
                //        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
                //        p.setRating("" + df.format(Double.parseDouble(jp.getText())));
                //        break;
                //    case TAG_RATING_COUNT:
                //        p.setVotes(jp.getText());
                //        break;
                //    case TAG_KEY_FEATURES:
                //        jp.skipChildren();
                //        break;
                //    case TAG_USER:
                //        jp.skipChildren();
                //        break;
                //    case TAG_URL:
                //        p.url = jp.getText();
                //        break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return p;
    }

    public static void populateProductList(JsonParser jp, ArrayList<Product> products, boolean forSearch) throws IOException {
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            if (forSearch)
                products.add(createForSearch(jp));
            else
                products.add(createFromJSON(jp));
        }
    }

    public void fillFromJson(JsonParser jp) throws IOException {
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            switch (fieldName) {
                case TAG_ID:
                    mID = jp.getText();
                    break;
                case TAG_NAME:
                    name = jp.getText();
                    break;
            /*    case TAG_BRAND:
                    p.Brand(jp.getText());
                    break;*/
                case TAG_CATEGORY:
                    category = jp.getText();
                    break;
                case StringUtils.TAG_CATEGORY_ID:
                    categoryID = jp.getIntValue();
                    break;
                case TAG_MIN_PRICE_STR:
                    price = "Rs. " + jp.getText();
                    break;
                case TAG_STORE_COUNT:
                    numStores = jp.getText() + " Stores";
                    break;
                case TAG_AVG_RATING:
                    DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
                    setRating("" + df.format(Double.parseDouble(jp.getText())));
                    break;
                case TAG_RATING_COUNT:
                    setVotes(jp.getText());
                    break;
                case TAG_IMAGES_O:
                    readImages(jp);
                    break;
                case TAG_KEY_FEATURES:
                    readKeyFeatures(jp);
                    break;
                case TAG_USER:
                    jp.skipChildren();
                    break;
                case TAG_URL:
                    url = jp.getText();
                    break;
                case StringUtils.TAG_IMAGES:
                    readMultipleImages(jp);
                    break;
                case StringUtils.TAG_BIQ_SCORE:
                    //biqScore = 80;
                    biqScore = jp.getIntValue();
                    break;
                case StringUtils.TAG_CURATED_REVIEW_COUNT:
                    curatedReviewCount = jp.getIntValue();
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
    }

    public void readImages(JsonParser jp) throws IOException {
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = jp.getCurrentName();
            jp.nextToken();
            switch (fieldName) {
                case TAG_SMALL_IMAGE:
                    smallImageUrl = jp.getText();
                    break;
                case TAG_MEDIUM_IMAGE:
                    mediumImageUrl = jp.getText();
                    break;
                case TAG_LARGE_IMAGE:
                    largeImageUrl = jp.getText();
                    break;
                case TAG_XLARGE_IMAGE:
                    xLargeImageUrl = jp.getText();
                    break;
            }
        }
    }

    public void readMultipleImages(JsonParser jp) throws IOException {
        int diff = Integer.MAX_VALUE;
        int diff2 = Integer.MAX_VALUE;
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            SparseArray<ProductImage> images = new SparseArray<>();
            while (jp.nextToken() != JsonToken.END_OBJECT) {
                String[] dim = jp.getCurrentName().split("x");
                jp.nextToken();
                ProductImage image = new ProductImage();
                image.width = Integer.parseInt(dim[0]);
                image.height = Integer.parseInt(dim[1]);
                image.url = jp.getText();
                images.append(image.width, image);
                if (multipleImages != null && multipleImages.size() == 0) {
                    if (ProductListingActivity.productListingSize <= image.width) {
                        if (image.width - ProductListingActivity.productListingSize < diff) {
                            diff = image.width - ProductListingActivity.productListingSize;
                            listingImageIndex = image.width;
                        }
                    }
                    if (ProductListingActivity.overviewSize <= image.width) {
                        if (image.width - ProductListingActivity.overviewSize < diff2) {
                            diff2 = image.width - ProductListingActivity.overviewSize;
                            overviewImageIndex = image.width;
                        }
                    }
                    if (image.width > largestImageIndex) {
                        largestImageIndex = image.width;
                    }
                }
            }
            multipleImages.add(images);
        }
    }

    public void readKeyFeatures(JsonParser jp) throws IOException {
        while (jp.nextToken() != JsonToken.END_ARRAY) {
            while (jp.nextToken() != JsonToken.END_ARRAY) {
                jp.nextToken();
                jp.nextToken();
                keyFeatures.add(jp.getText());
            }
        }
    }


    public void setRating(String rating) {
        if (TextUtils.indexOf(rating, ".") < 0 && !rating.equals("10"))
            rating += ".0";
        this.rating = rating;
    }
/*
    public String votes {
        return votes;
    }*/

    public void setVotes(String votes) {
        if (votes.indexOf("0") != 0)
            this.votes = votes + " Votes";
        else
            this.votes = "";

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mID);
        parcel.writeString(name);
        //    parcel.writeString(brand);
        parcel.writeString(category);
        parcel.writeInt(categoryID);
        parcel.writeString(price);
        parcel.writeString(rating);
        parcel.writeString(votes);
        parcel.writeString(numStores);
        writeImages(parcel, multipleImages);
        parcel.writeString(smallImageUrl);
        parcel.writeString(mediumImageUrl);
        parcel.writeString(largeImageUrl);
        parcel.writeString(xLargeImageUrl);
        parcel.writeInt(listingImageIndex);
        parcel.writeInt(overviewImageIndex);
        parcel.writeInt(largestImageIndex);
        parcel.writeStringList(keyFeatures);
        parcel.writeInt(isAlternative);
        if (isAlternative == 1) {
            parcel.writeStringArray(comparisonString);
            parcel.writeIntArray(comparisonInt);
        }
        parcel.writeByte((byte) (isSelectedForCompare ? 1 : 0));
        parcel.writeString(url);
        parcel.writeInt(biqScore);
        parcel.writeInt(curatedReviewCount);
    }


    public void writeImages(Parcel p, ArrayList<SparseArray<ProductImage>> val) {
        if (val == null) {
            p.writeInt(-1);
            return;
        }
        int N = val.size();
        p.writeInt(N);
        int i = 0;
        while (i < N) {
            writeSparseArray(p, val.get(i));
            i++;
        }
    }

    public void writeSparseArray(Parcel p, SparseArray<ProductImage> val) {
        if (val == null) {
            p.writeInt(-1);
            return;
        }
        int N = val.size();
        p.writeInt(N);
        int i = 0;
        while (i < N) {
            p.writeInt(val.keyAt(i));
            p.writeParcelable(val.valueAt(i), 0);
            i++;
        }
    }

    public void readImages(Parcel p, ArrayList<SparseArray<ProductImage>> arr) {
        int N = p.readInt();
        int i = 0;
        while (i < N) {
            SparseArray<ProductImage> sa = new SparseArray<>();
            readSparseArray(p, sa);
            arr.add(sa);
            i++;
        }
    }

    public void readSparseArray(Parcel p, SparseArray<ProductImage> sa) {
        int N = p.readInt();
        int i = 0;
        while (i < N) {
            int key = p.readInt();
            ProductImage val = p.readParcelable(ProductImage.class.getClassLoader());
            sa.append(key, val);
            i++;
        }
    }

    public String getOverviewImage() {
        if (multipleImages != null && multipleImages.size() > 0) {
            if (overviewImageIndex != 0)
                return multipleImages.get(0).get(overviewImageIndex).url;
            else if (largestImageIndex != 0)
                return multipleImages.get(0).get(largestImageIndex).url;
        }
        return xLargeImageUrl;
    }

    public String getListingImage() {
        if (multipleImages != null && multipleImages.size() > 0) {
            if (listingImageIndex != 0)
                return multipleImages.get(0).get(listingImageIndex).url;
            else if (largestImageIndex != 0)
                return multipleImages.get(0).get(largestImageIndex).url;
        }
        return xLargeImageUrl;
    }

   /* public int getBiqScore() {
        return biqScore;
    }

    public int getCuratedReviewCount() {
        return curatedReviewCount;
    }

    public int category {
        return categoryID;
    }

    public static Product createFromJSON(JSONObject productInfo) throws JSONException {
        String id = productInfo.getString(TAG_ID);
        Product p = new Product(id);
        p.name = productInfo.getString(TAG_NAME);
        p.numStores = productInfo.getString(TAG_STORE_COUNT) + " Stores";
        p.price = "Rs. " + productInfo.getString(TAG_MIN_PRICE_STR);
        p.category = productInfo.getString(TAG_CATEGORY);
        p.Brand = productInfo.getString(TAG_BRAND));
        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
        p.setRating("" + df.format(Double.parseDouble(productInfo.getString(TAG_AVG_RATING))));
        p.setVotes(productInfo.getString(TAG_RATING_COUNT));
        JSONObject productImages = productInfo.getJSONObject(TAG_IMAGES_O);
        p.smallImageUrl = productImages.getString(TAG_SMALL_IMAGE);
        p.mediumImageUrl = productImages.getString(TAG_MEDIUM_IMAGE);
        p.largeImageUrl = productImages.getString(TAG_LARGE_IMAGE);
        p.xLargeImageUrl = productImages.getString(TAG_XLARGE_IMAGE);
        p.url = productInfo.getString(TAG_URL);
        JSONArray keyFeatures = productInfo.getJSONArray(TAG_KEY_FEATURES);
        for (int j = 0; j < keyFeatures.length(); j++) {
            p.addKeyFeature(keyFeatures.getJSONArray(j).getString(FEATURE_STR_INDEX));
        }
        return p;
    }

   public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setID(String mID) {
        this.mID = mID;
    }

    public String mID {
        return mID;
    }

    public String name {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String price {
        return price;
    }

    public String rating {
        return rating;
    }


    public String smallImageUrl {
        return getListingImage();
    }

    public void setSmallImageUrl(String smallImageUrl) {
        this.smallImageUrl = smallImageUrl;
    }

    //Images based on Small, Medium, Large Model have been deprecated
    public String mediumImageUrl {
        return  getListingImage();
    }

    public void setMediumImageUrl(String mediumImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
    }

    public String largeImageUrl {
        return getOverviewImage();
    }

    public void setLargeImageUrl(String largeImageUrl) {
        this.largeImageUrl = largeImageUrl;
    }

    public String xLargeImageUrl {
        return getOverviewImage();
    }

    public void setxLargeImageUrl(String xLargeImageUrl) {
        this.xLargeImageUrl = xLargeImageUrl;
    }

    public ArrayList<String> keyFeatures {
        return keyFeatures;
    }

    public void addKeyFeature(String feature) {
        keyFeatures.add(feature);
    }

    public int[] comparisonInt {
        return comparisonInt;
    }

    public void setComparisonInt(int[] comparisonInt) {
        this.comparisonInt = comparisonInt;
    }

    public String[] comparisonString() {
        return comparisonString;
    }

    public void setComparisonString(String[] comparisonString) {
        this.comparisonString = comparisonString;
    }

    public String category {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String url {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String numStores {
        return numStores;
    }

    public ArrayList<SparseArray<ProductImage>> getImages() {
        return multipleImages;
    }
    */

/*    public boolean isSelectedForCompare() {
        return isSelectedForCompare;
    }

    public void setSelectedForCompare(boolean isSelectedForCompare) {
        this.isSelectedForCompare = isSelectedForCompare;
    }*/

    /*    public void setKeyFeatures(ArrayList<String> keyFeatures) {
        this.keyFeatures = keyFeatures;
    }*/



    /*public static Product createFromCursor(Cursor c) {
        Product p = new Product("");
        p.mID = c.getString(0);
        p.name = c.getString(1);
        p.category = c.getString(2);
        p.smallImageUrl = c.getString(3);
        p.mediumImageUrl = c.getString(4);
        p.largeImageUrl = c.getString(5);
        p.xLargeImageUrl = c.getString(6);
        p.price = c.getString(7);
        p.url = c.getString(8);
        return p;
    }*/
}
