package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductImage implements Parcelable {
    public static final Creator<ProductImage> CREATOR = new Creator<ProductImage>() {
        @Override
        public ProductImage createFromParcel(Parcel source) {
            return new ProductImage(source);
        }

        @Override
        public ProductImage[] newArray(int size) {
            return new ProductImage[size];
        }
    };
    int width;
    int height;
    String url;

    public ProductImage() {
        //Required empty constructor
    }

    public ProductImage(Parcel src) {
        width = src.readInt();
        height = src.readInt();
        url = src.readString();
    }

    @Override
    public int describeContents() {
        return width;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(url);
    }
}