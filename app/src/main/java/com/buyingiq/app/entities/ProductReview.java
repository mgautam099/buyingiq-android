package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * An object to hold a Product review.
 */

public class ProductReview implements Parcelable {

    public static final Creator<ProductReview> CREATOR = new Creator<ProductReview>() {
        @Override
        public ProductReview createFromParcel(Parcel parcel) {
            return new ProductReview(parcel);
        }

        @Override
        public ProductReview[] newArray(int i) {
            return new ProductReview[0];
        }
    };
    public User user;
    public String uri;
    private String review;
    private String overallRating;
    private ArrayList<Rating> ratings;
    private int upvotes;
    public int views;
    public String addedOn;
    private String publishedOn;
    private String publishedOnStr;
    private boolean isPublished;
    private boolean isFeatured;
    private boolean isTopReview;

    public ProductReview(String uri) {
        this.uri = uri;
    }

    public ProductReview(Parcel parcel) {
        user = parcel.readParcelable(User.class.getClassLoader());
        uri = parcel.readString();
        review = parcel.readString();
        overallRating = parcel.readString();
        ratings = new ArrayList<Rating>();
        parcel.readTypedList(ratings, Rating.CREATOR);
        upvotes = parcel.readInt();
        views = parcel.readInt();
        addedOn = parcel.readString();
        publishedOn = parcel.readString();
        publishedOnStr = parcel.readString();
        isPublished = parcel.readByte() != 0;
        isFeatured = parcel.readByte() != 0;
        isTopReview = parcel.readByte() != 0;
    }

    public String getUri() {
        return uri;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getOverallRating() {
        return overallRating;
    }

    public void setOverallRating(String overallRating) {
        this.overallRating = overallRating;
    }

    public ArrayList<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(ArrayList<Rating> ratings) {
        this.ratings = ratings;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public String getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(String publishedOn) {
        this.publishedOn = publishedOn;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public String getPublishedOnStr() {
        return publishedOnStr;
    }

    public void setPublishedOnStr(String publishedOnStr) {
        this.publishedOnStr = publishedOnStr;
    }

    public boolean isFeatured() {
        return isFeatured;
    }

    public void setFeatured(boolean isFeatured) {
        this.isFeatured = isFeatured;
    }

    public boolean isTopReview() {
        return isTopReview;
    }

    public void setTopReview(boolean isTopReview) {
        this.isTopReview = isTopReview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(user, i);
        parcel.writeString(uri);
        parcel.writeString(review);
        parcel.writeString(overallRating);
        parcel.writeTypedList(ratings);
        parcel.writeInt(upvotes);
        parcel.writeInt(views);
        parcel.writeString(addedOn);
        parcel.writeString(publishedOn);
        parcel.writeString(publishedOnStr);
        parcel.writeByte((byte) (isPublished ? 1 : 0));
        parcel.writeByte((byte) (isFeatured ? 1 : 0));
        parcel.writeByte((byte) (isTopReview ? 1 : 0));
    }
}
