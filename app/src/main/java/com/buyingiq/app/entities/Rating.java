package com.buyingiq.app.entities;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mayank on 07-07-2014.
 */
public class Rating implements Parcelable {
    public static final String KEY_HEAD = "head";
    public static final String KEY_SCORE = "score";
    public static final String KEY_COUNT = "count";
    public static final String KEY_AVERAGE = "average";
    public static final String KEY_IS_AVERAGE = "is_average";
    public static final Creator<Rating> CREATOR = new Creator<Rating>() {
        @Override
        public Rating createFromParcel(Parcel parcel) {
            return new Rating(parcel);
        }

        @Override
        public Rating[] newArray(int i) {
            return new Rating[i];
        }
    };
    public String head;
    public int score;
    public double average;
    public int count;
    public boolean isAverage;

    public Rating(Parcel parcel) {
        Bundle b = parcel.readBundle();
        head = b.getString(KEY_HEAD);
        score = b.getInt(KEY_SCORE);
        average = b.getDouble(KEY_AVERAGE);
        count = b.getInt(KEY_COUNT);
        isAverage = b.getBoolean(KEY_IS_AVERAGE);
    }

    public Rating() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Bundle b = new Bundle();
        b.putString(KEY_HEAD, head);
        b.putInt(KEY_SCORE, score);
        b.putDouble(KEY_AVERAGE, average);
        b.putInt(KEY_COUNT, count);
        b.putBoolean(KEY_IS_AVERAGE, isAverage);
        parcel.writeBundle(b);
    }
}
