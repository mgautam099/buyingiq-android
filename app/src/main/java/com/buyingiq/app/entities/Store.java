package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import static com.buyingiq.app.resources.StringUtils.TAG_HAS_COD;
import static com.buyingiq.app.resources.StringUtils.TAG_HAS_EMI;
import static com.buyingiq.app.resources.StringUtils.TAG_IMAGE_1;
import static com.buyingiq.app.resources.StringUtils.TAG_IMAGE_2;
import static com.buyingiq.app.resources.StringUtils.TAG_IMAGE_3;
import static com.buyingiq.app.resources.StringUtils.TAG_IS_SPONSORED;
import static com.buyingiq.app.resources.StringUtils.TAG_NAME;
import static com.buyingiq.app.resources.StringUtils.TAG_RATING_OVERALL;
import static com.buyingiq.app.resources.StringUtils.TAG_RETURN_POLICY;
import static com.buyingiq.app.resources.StringUtils.TAG_REVIEW_COUNT;
import static com.buyingiq.app.resources.StringUtils.TAG_URI;

/**
 * A {@link android.os.Parcelable} Class to store the attributes of a Product Store
 */
public class Store implements Parcelable {

    public static final Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel parcel) {
            return new Store(parcel);
        }

        @Override
        public Store[] newArray(int i) {
            return new Store[i];
        }
    };
    private static final String TAG_RATING_DELIVERY = "avg_rating_delivery";
    private static final String TAG_RATING_AUTH = "avg_rating_auth";
    private static final String TAG_RATING_ISSUE = "avg_rating_issue";
    private static final String TAG_SHOP_AGAIN_PERC = "shop_again_perc";
    private String uri;
    public String name;
    private String imageSmall;
    private String imageMedium;
    private String imageLarge;
    private String ratingOverall;
    private String ratingDelivery;
    private String ratingAuth;
    private String ratingIssue;
    private String reviewCount;
    private String returnPolicy;
    private boolean hasCOD;
    private boolean hasEMI;
    private boolean isSponsored;
    private int shopAgainPerc;

    public Store(String uri) {
        this.uri = uri;
    }

    public Store(Parcel parcel) {
        uri = parcel.readString();
        name = parcel.readString();
        ratingOverall = parcel.readString();
        ratingDelivery = parcel.readString();
        ratingAuth = parcel.readString();
        ratingIssue = parcel.readString();
        reviewCount = parcel.readString();
        imageSmall = parcel.readString();
        imageMedium = parcel.readString();
        imageLarge = parcel.readString();
        returnPolicy = parcel.readString();
        hasCOD = parcel.readByte() != 0;
        hasEMI = parcel.readByte() != 0;
        isSponsored = parcel.readByte() != 0;
        shopAgainPerc = parcel.readInt();
    }

    public static Store createFromJson(JSONObject storeObject) throws JSONException {
        Store s = new Store(storeObject.getString(TAG_URI));
        s.setHasCOD(storeObject.getBoolean(TAG_HAS_COD));
        s.setHasEMI(storeObject.getBoolean(TAG_HAS_EMI));
        s.setName(storeObject.getString(TAG_NAME));
        s.setImageSmall(storeObject.getString(TAG_IMAGE_1));
        s.setImageMedium(storeObject.getString(TAG_IMAGE_2));
        s.setImageLarge(storeObject.getString(TAG_IMAGE_3));
        s.setRatingOverall(storeObject.getString(TAG_RATING_OVERALL));
        s.setRatingDelivery(storeObject.getString(TAG_RATING_DELIVERY));
        s.setRatingAuth(storeObject.getString(TAG_RATING_AUTH));
        s.setRatingIssue(storeObject.getString(TAG_RATING_ISSUE));
        //    s.setRatingCount(""+storeObject.getInt(TAG_RATING_COUNT));
        s.setReviewCount("" + storeObject.getString(TAG_REVIEW_COUNT));
        s.setReturnPolicy(storeObject.getString(TAG_RETURN_POLICY));
        s.setSponsored(storeObject.getBoolean(TAG_IS_SPONSORED));
        s.setShopAgainPerc(storeObject.getInt(TAG_SHOP_AGAIN_PERC));
        return s;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uri);
        parcel.writeString(name);
        parcel.writeString(ratingOverall);
        parcel.writeString(ratingDelivery);
        parcel.writeString(ratingAuth);
        parcel.writeString(ratingIssue);
        parcel.writeString(reviewCount);
        parcel.writeString(imageSmall);
        parcel.writeString(imageMedium);
        parcel.writeString(imageLarge);
        parcel.writeString(returnPolicy);
        parcel.writeByte((byte) (hasCOD ? 1 : 0));
        parcel.writeByte((byte) (hasEMI ? 1 : 0));
        parcel.writeByte((byte) (isSponsored ? 1 : 0));
        parcel.writeInt(shopAgainPerc);
    }

    public String getUri() {
        return uri;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageSmall() {
        return imageSmall;
    }

    public void setImageSmall(String imageSmall) {
        this.imageSmall = imageSmall;
    }

    public String getImageMedium() {
        return imageMedium;
    }

    public void setImageMedium(String imageMedium) {
        this.imageMedium = imageMedium;
    }

    public String getImageLarge() {
        return imageLarge;
    }

    public void setImageLarge(String imageLarge) {
        this.imageLarge = imageLarge;
    }

    public String getRatingOverall() {
        return ratingOverall;
    }

    public void setRatingOverall(String ratingOverall) {
        this.ratingOverall = ratingOverall;
    }

    public String getRatingDelivery() {
        return ratingDelivery;
    }

    public void setRatingDelivery(String ratingDelivery) {
        this.ratingDelivery = ratingDelivery;
    }

    public String getRatingAuth() {
        return ratingAuth;
    }

    public void setRatingAuth(String ratingAuth) {
        this.ratingAuth = ratingAuth;
    }

    public String getRatingIssue() {
        return ratingIssue;
    }

    public void setRatingIssue(String ratingIssue) {
        this.ratingIssue = ratingIssue;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReturnPolicy() {
        return returnPolicy;
    }

    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    public boolean isHasCOD() {
        return hasCOD;
    }

    public void setHasCOD(boolean hasCOD) {
        this.hasCOD = hasCOD;
    }

    public boolean isHasEMI() {
        return hasEMI;
    }

    public void setHasEMI(boolean hasEMI) {
        this.hasEMI = hasEMI;
    }

    public boolean isSponsored() {
        return isSponsored;
    }

    public void setSponsored(boolean isSponsored) {
        this.isSponsored = isSponsored;
    }

    public int getShopAgainPerc() {
        return shopAgainPerc;
    }

    public void setShopAgainPerc(int shopAgainPerc) {
        this.shopAgainPerc = shopAgainPerc;
    }

}
