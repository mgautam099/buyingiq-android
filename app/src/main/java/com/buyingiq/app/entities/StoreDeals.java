package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * A relation class between {@link com.buyingiq.app.entities.Store} and {@link com.buyingiq.app.entities.Deal},
 * used to store all the deals of a product from a particular store.
 */
public class StoreDeals implements Parcelable {
    public static final Creator<StoreDeals> CREATOR = new Creator<StoreDeals>() {
        @Override
        public StoreDeals createFromParcel(Parcel parcel) {
            return new StoreDeals(parcel);
        }

        @Override
        public StoreDeals[] newArray(int i) {
            return new StoreDeals[i];
        }
    };
    private Store store;
    private ArrayList<Deal> dealList;
    private boolean inStock;
    private String minPrice;
    private boolean sponsored;
    private ArrayList<String> coupons;
    private int offerCount;

    public StoreDeals(Parcel p) {
        store = p.readParcelable(Store.class.getClassLoader());
        p.readList(dealList, Deal.class.getClassLoader());
        inStock = p.readByte() != 0;
        minPrice = p.readString();
        sponsored = p.readByte() != 0;
        p.readStringList(coupons);
        offerCount = p.readInt();
    }

    public StoreDeals(Store store) {
        this.store = store;
        dealList = new ArrayList<Deal>();
        offerCount = 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(store, i);
        parcel.writeList(dealList);
        parcel.writeByte((byte) (inStock ? 1 : 0));
        parcel.writeString(minPrice);
        parcel.writeByte((byte) (sponsored ? 1 : 0));
        parcel.writeStringList(coupons);
        parcel.writeInt(offerCount);
    }

    public void addDeal(Deal deal) {
        dealList.add(deal);
        if (!deal.offer.isEmpty())
            incrementOfferCount();
    }

    public ArrayList<Deal> getDealList() {
        return dealList;
    }

    public Store getStore() {
        return store;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public boolean isSponsored() {
        return sponsored;
    }

    public void setSponsored(boolean sponsored) {
        this.sponsored = sponsored;
    }

    public ArrayList<String> getCoupons() {
        return coupons;
    }

    public void setCoupons(ArrayList<String> coupons) {
        this.coupons = coupons;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void incrementOfferCount() {
        offerCount++;
    }

    public void decrementOfferCount() {
        offerCount--;
    }

    public int getOfferCount() {
        return offerCount;
    }

}
