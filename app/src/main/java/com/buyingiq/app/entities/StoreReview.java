package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Mayank on 07-07-2014.
 */

public class StoreReview implements Parcelable {

    public static final Creator<StoreReview> CREATOR = new Creator<StoreReview>() {
        @Override
        public StoreReview createFromParcel(Parcel parcel) {
            return new StoreReview(parcel);
        }

        @Override
        public StoreReview[] newArray(int i) {
            return new StoreReview[0];
        }
    };
    private User user;
    private String title;
    private String review;
    private ArrayList<Rating> ratings;
    private int upvotes;
    private int totalVotes;
    private String addedOn;
    private String addedOnStr;
    private String purchased;

    public StoreReview() {
        //Required empty constructor.
    }

    public StoreReview(Parcel parcel) {
        user = parcel.readParcelable(User.class.getClassLoader());
        title = parcel.readString();
        review = parcel.readString();
        ratings = new ArrayList<Rating>();
        parcel.readTypedList(ratings, Rating.CREATOR);
        upvotes = parcel.readInt();
        totalVotes = parcel.readInt();
        addedOn = parcel.readString();
        addedOnStr = parcel.readString();
        purchased = parcel.readString();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public ArrayList<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(ArrayList<Rating> ratings) {
        this.ratings = ratings;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public int getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(int totalVotes) {
        this.totalVotes = totalVotes;
    }

    public String getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(String addedOn) {
        this.addedOn = addedOn;
    }

    public String getAddedOnStr() {
        return addedOnStr;
    }

    public void setAddedOnStr(String addedOnStr) {
        this.addedOnStr = addedOnStr;
    }

    public String getPurchased() {
        return purchased;
    }

    public void setPurchased(String purchased) {
        this.purchased = purchased;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(user, i);
        parcel.writeString(title);
        parcel.writeString(review);
        parcel.writeTypedList(ratings);
        parcel.writeInt(upvotes);
        parcel.writeInt(totalVotes);
        parcel.writeString(addedOn);
        parcel.writeString(addedOnStr);
        parcel.writeString(purchased);
    }
}
