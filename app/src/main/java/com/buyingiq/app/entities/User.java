package com.buyingiq.app.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.buyingiq.app.resources.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class User implements Parcelable {

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }

        @Override
        public User[] newArray(int i) {
            return new User[i];
        }
    };
    //Should be updated whenever the user colors change
    private static final int TOTAL_COLORS = 8;
    private String username;
    public String name;
    private String shortName;
    private String image1;
    private String image2;
    private String image3;
    private long points;
    private String website;
    private int level;
    private String location;
    private String joinedOn;
    //Random number for images
    private int rand;

    public User(String username) {
        this.username = username;
    }

    public User(Parcel parcel) {
        username = parcel.readString();
        name = parcel.readString();
        shortName = parcel.readString();
        image1 = parcel.readString();
        image2 = parcel.readString();
        image3 = parcel.readString();
        points = parcel.readLong();
        website = parcel.readString();
        level = parcel.readInt();
        location = parcel.readString();
        joinedOn = parcel.readString();
        rand = parcel.readInt();
    }

    public static User createFromJson(JSONObject json) throws JSONException {
        User u = new User(json.getString(StringUtils.TAG_USERNAME));
        u.setImage1(json.getString(StringUtils.TAG_IMAGE_1));
        u.setImage2(json.getString(StringUtils.TAG_IMAGE_2));
        u.setImage3(json.getString(StringUtils.TAG_IMAGE_3));
        Random r = new Random();
        u.setRand(r.nextInt(TOTAL_COLORS));
        u.setJoinedOn(json.getString(StringUtils.TAG_JOINED_ON));
        u.setLevel(json.getInt(StringUtils.TAG_LEVEL));
        u.setLocation(json.getString(StringUtils.TAG_LOCATION));
        u.setName(json.getString(StringUtils.TAG_NAME));
        u.setPoints(json.getInt(StringUtils.TAG_POINTS));
        u.setWebsite(json.getString(StringUtils.TAG_WEBSITE));
        return u;
    }

    public String getUsername() {
        return username;
    }

    public void setName(String name) {
        this.name = name.trim();
        if (this.name.contains(" ")) {
            String[] temp = this.name.split("\\s+");
            setShortName(temp[0] + " " + temp[1].substring(0, 1).toUpperCase() + ".");
        } else
            setShortName(this.name);
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getJoinedOn() {
        return joinedOn;
    }

    public void setJoinedOn(String joinedOn) {
        this.joinedOn = joinedOn;
    }

    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(name);
        parcel.writeString(shortName);
        parcel.writeString(image1);
        parcel.writeString(image2);
        parcel.writeString(image3);
        parcel.writeLong(points);
        parcel.writeString(website);
        parcel.writeInt(level);
        parcel.writeString(location);
        parcel.writeString(joinedOn);
        parcel.writeInt(rand);
    }

}
