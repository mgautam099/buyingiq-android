package com.buyingiq.app.listadapters;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.buyingiq.app.BannerFragment;
import com.buyingiq.app.entities.Banner;

import java.util.ArrayList;


/**
 * A {@link android.support.v4.app.FragmentStatePagerAdapter} to display the banners in the app for the front page.
 */
public class BannerPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Banner> mBannerList;

    public BannerPagerAdapter(FragmentManager fm, ArrayList<Banner> bannerList) {
        super(fm);
        mBannerList = bannerList;
    }

    @Override
    public Fragment getItem(int position) {
        if (mBannerList.size() > 0) {
            return BannerFragment.newInstance(mBannerList.get(position).image, position);
        } else
            return BannerFragment.newInstance(null, position);
    }

    @Override
    public int getCount() {
        return mBannerList.size();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }

}
