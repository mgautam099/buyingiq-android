package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.buyingiq.app.BuyingIQApp;
import com.buyingiq.app.R;
import com.buyingiq.app.entities.Deal;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

public class DealListAdapter extends BaseAdapter {

    Context mContext;
    String mStoreName;
    ArrayList<Deal> mDealList;
    Tracker t;

    public DealListAdapter(Activity context, String storeName, ArrayList<Deal> dealList) {
        mDealList = dealList;
        mContext = context;
        mStoreName = storeName;
        t = ((BuyingIQApp) context.getApplication()).getTracker(BuyingIQApp.TrackerName.APP_TRACKER);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_deal, parent, false);
            holder = new ViewHolder();
            holder.nameView = (TextView) convertView.findViewById(R.id.deal_name);
            holder.inStockView = (TextView) convertView.findViewById(R.id.deal_stock_info);
            holder.deliveryTimeView = (TextView) convertView.findViewById(R.id.deal_delivery_time);
            holder.basePriceView = (TextView) convertView.findViewById(R.id.deal_base_price);
            holder.shipCostView = (TextView) convertView.findViewById(R.id.deal_ship_cost);
            holder.visitStoreButton = (TextView) convertView.findViewById(R.id.visit_store_button);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        Deal d = mDealList.get(position);
        if (!d.offer.isEmpty())
            holder.nameView.setText(d.name + " + " + d.offer);
        else
            holder.nameView.setText(d.name);
        if (d.basePrice.equals("0")) {
            holder.basePriceView.setText("");
            holder.deliveryTimeView.setText("");
            holder.shipCostView.setText("");
        } else {
            if (d.shipCost.equals("0")) {
                holder.shipCostView.setText(mContext.getString(R.string.text_free_shipping));
            } else {
                holder.shipCostView.setText("+ Rs. " + d.shipCost);
            }
            holder.basePriceView.setText("Rs. " + d.basePrice);
        }
        holder.inStockView.setText(d.stockInfo);
        if (d.stockInt == 0) {
            holder.deliveryTimeView.setText("");
            holder.inStockView.setSelected(false);
        } else {
            if (TextUtils.isEmpty(d.deliveryTime))
                holder.deliveryTimeView.setText(String.format(mContext.getString(R.string.text_delivery_time), d.deliveryTime));
            else
                holder.deliveryTimeView.setText("");
            holder.inStockView.setSelected(true);
        }
        holder.visitStoreButton.setTag(d);
        holder.visitStoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Deal d1 = (Deal) view.getTag();
                final String link = d1.url;
                t.send(new HitBuilders.EventBuilder("DealRedirect", mStoreName)
                        .setLabel(d1.redirectHash)
                        .setValue(1)
                        .build());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(link));
                Intent openInChooser = Intent.createChooser(intent, null);
                mContext.startActivity(openInChooser);
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return mDealList.size();
    }

    @Override
    public Object getItem(int i) {
        return mDealList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    static class ViewHolder {
        TextView nameView;
        TextView inStockView;
        TextView deliveryTimeView;
        TextView basePriceView;
        TextView shipCostView;
        TextView visitStoreButton;
    }
}
