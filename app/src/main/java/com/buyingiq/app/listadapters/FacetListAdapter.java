package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import com.buyingiq.app.R;
import com.buyingiq.app.entities.Facet;

import java.util.ArrayList;

public class FacetListAdapter extends BaseAdapter {
    private ArrayList<Facet> mFacets;
    private Context mContext;

    public FacetListAdapter(Context context, ArrayList<Facet> facets) {
        mFacets = facets;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mFacets.size();
    }

    @Override
    public Facet getItem(int i) {
        return mFacets.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_facet, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.label = (CheckedTextView) convertView.findViewById(R.id.facet_label);
            convertView.setTag(viewHolder);
            viewHolder.label.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckedTextView checkedTextView = (CheckedTextView) view;
                    if (checkedTextView.isChecked()) {
                        checkedTextView.setChecked(false);
                        ((Facet) checkedTextView.getTag()).deselect();
                    } else {
                        checkedTextView.setChecked(true);
                        ((Facet) checkedTextView.getTag()).select();
                    }
                }
            });
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Facet f = mFacets.get(position);
        //    viewHolder.label.setText(Html.fromHtml(f.getLabel()));
        Spannable styledText = new SpannableString(f.label + " (" + f.count + ")");
        TextAppearanceSpan tas = new TextAppearanceSpan(mContext, R.style.textLightGrey);
        if (f.count > 0) {
            styledText.setSpan(tas, f.label.length() + 1, styledText.length(), Spanned.SPAN_COMPOSING);
        } else {
            styledText.setSpan(tas, 0, styledText.length(), Spanned.SPAN_COMPOSING);
        }
        viewHolder.label.setText(styledText);
        viewHolder.label.setChecked(f.isSelected() == 1);
        viewHolder.label.setTag(f);
        return convertView;
    }

    private static class ViewHolder {
        CheckedTextView label;
    }


}
