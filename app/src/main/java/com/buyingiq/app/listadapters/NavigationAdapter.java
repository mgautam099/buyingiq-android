package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.buyingiq.app.R;
import com.buyingiq.app.resources.Categories;

public class NavigationAdapter extends ArrayAdapter {

    private Context mContext;

    public NavigationAdapter(Context context, int resource) {
        super(context, resource);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_navigation, parent, false);
            holder = new ViewHolder();
            holder.head = (TextView) convertView.findViewById(R.id.label);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        Object[] cat = Categories.catsToUse[position];
        holder.head.setText((String) cat[0]);
        holder.head.setCompoundDrawablesWithIntrinsicBounds((Integer) cat[2], 0, 0, 0);
        return convertView;
    }

    @Override
    public int getCount() {
        return Categories.catsToUse.length;
    }

    static class ViewHolder {
        TextView head;
    }
}
