package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buyingiq.app.ProductListingActivity;
import com.buyingiq.app.R;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.widget.FeatureTextView;

import java.util.ArrayList;

import static com.buyingiq.app.ProductAlternativesFragment.MAX_FEATURES;

public class ProductAlternativeListAdapter extends ArrayAdapter<Product> {
    //    private static int DPI_CODE;
    private ArrayList<Product> mProductList;
    private Context mContext;
    private ImageLoader1 imageLoader1;

    public ProductAlternativeListAdapter(Activity context, ArrayList<Product> productList) {
        super(context, R.layout.item_product_alt, productList);
        mProductList = productList;
        mContext = context;
        imageLoader1 = new ImageLoader1(context, ProductListingActivity.mCurrentDefaultImage);
//        DPI_CODE = mContext.getResources().getDisplayMetrics().densityDpi;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_product_alt, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.product_image);
            viewHolder.name = (TextView) convertView.findViewById(R.id.product_name);
            viewHolder.price = (TextView) convertView.findViewById(R.id.product_price);
            viewHolder.stores = (TextView) convertView.findViewById(R.id.product_stores);
            viewHolder.features = (LinearLayout) convertView.findViewById(R.id.feature_layout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Product p = mProductList.get(position);
        //Crashlytics.setString(StringUtils.TAG_PRODUCT, p.mID);
        String productImage = p.getListingImage();
        if (productImage != null && !TextUtils.isEmpty(productImage))
            imageLoader1.DisplayImage(productImage, viewHolder.image);
        else {
            viewHolder.image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            viewHolder.image.setImageResource(ProductListingActivity.mCurrentDefaultImage);
        }
        /*if(p.mID.equals("-1111111")) {
            convertView.setBackgroundColor(Color.parseColor("#ffacacac"));
            convertView.findViewById(R.id.product_info_view).setVisibility(View.GONE);
            convertView.findViewById(R.id.progress_view).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.product_image).setVisibility(View.GONE);
        }
        else {
            convertView.setBackgroundResource(R.drawable.bag_card);
            convertView.findViewById(R.id.product_info_view).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.progress_view).setVisibility(View.GONE);
            convertView.findViewById(R.id.product_image).setVisibility(View.VISIBLE);
            viewHolder.image.setImageResource(R.drawable.default_mobile);*/
        viewHolder.name.setText(p.name);
        viewHolder.price.setText(p.price);
        viewHolder.stores.setText(p.numStores);
        if (p.comparisonString != null && p.comparisonString.length > 0) {
            for (int i = 0; i < p.comparisonString.length; i++) {
                FeatureTextView t = (FeatureTextView) viewHolder.features.getChildAt(i);
                t.setText(p.comparisonString[i], p.comparisonInt[i]);
            }
        } else {
            ArrayList<String> keyfeat = p.keyFeatures;
            int total = keyfeat.size() > MAX_FEATURES ? MAX_FEATURES : keyfeat.size();
            for (int i = 0; i < total; i++) {
                FeatureTextView t = (FeatureTextView) viewHolder.features.getChildAt(i);
                t.setText(keyfeat.get(i), 2);
            }
        }
        return convertView;
    }

//    private String getImageByDensity(Product p) {
//        switch (DPI_CODE) {
//            case DisplayMetrics.DENSITY_LOW:
//                return p.mediumImageUrl;
//            case DisplayMetrics.DENSITY_MEDIUM:
//                return p.largeImageUrl;
//            case DisplayMetrics.DENSITY_HIGH:
//                return p.largeImageUrl;
//            case DisplayMetrics.DENSITY_XHIGH:
//                return p.xLargeImageUrl;
//            default:
//                return p.xLargeImageUrl;
//        }
//    }

    private static class ViewHolder {
        ImageView image;
        TextView name;
        TextView price;
        TextView stores;
        LinearLayout features;
    }

}
