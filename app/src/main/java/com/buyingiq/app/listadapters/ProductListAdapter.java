package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.buyingiq.app.ProductListingActivity;
import com.buyingiq.app.R;
import com.buyingiq.app.entities.Product;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.StringUtils;
import com.buyingiq.app.widget.CheckBoxLeft;

import java.util.ArrayList;

public class ProductListAdapter extends BaseAdapter {
    public static final int COMPARE_LIMIT = 2;
    private static int DPI_CODE;
    public ArrayList<Integer> compareList;
    private ArrayList<Product> mProductList;
    private Context mContext;
    private ImageLoader1 imageLoader1;
    private boolean isSearch;

    public ProductListAdapter(Activity context, ArrayList<Product> productList, boolean isSearch) {
        mProductList = productList;
        mContext = context;
        imageLoader1 = new ImageLoader1(context, ProductListingActivity.mCurrentDefaultImage);
        compareList = new ArrayList<>();
        this.isSearch = isSearch;
        DPI_CODE = mContext.getResources().getDisplayMetrics().densityDpi;
    }

    public void changeDefaultImage(int imageID) {
        imageLoader1.changeCurrentDefaultImage(imageID);
    }

    public ArrayList<Product> getCompareList() {
        ArrayList<Product> tempList = new ArrayList<>();
        tempList.add(getItem(compareList.get(0)));
        tempList.add(getItem(compareList.get(1)));
        return tempList;
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public Product getItem(int i) {
        return mProductList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(mProductList.get(i).mID);
    }

    @Override
    public View getView(final int position, View rootView, ViewGroup parent) {
        final ViewHolder holder;
        if (rootView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            rootView = inflater.inflate(R.layout.item_product, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) rootView.findViewById(R.id.product_image);
            holder.name = (TextView) rootView.findViewById(R.id.product_name);
            holder.rating = (TextView) rootView.findViewById(R.id.product_rating);
            holder.price = (TextView) rootView.findViewById(R.id.product_price);
            holder.stores = (TextView) rootView.findViewById(R.id.product_stores);
            holder.votes = (TextView) rootView.findViewById(R.id.product_votes);
            holder.compare = (CheckBoxLeft) rootView.findViewById(R.id.product_compare_check);
            rootView.setTag(holder);
        } else {
            holder = (ViewHolder) rootView.getTag();
        }
        Product p = mProductList.get(position);
        //Crashlytics.setString(StringUtils.TAG_PRODUCT, p.mID);
        String productImage = p.getListingImage();
        if (productImage != null && !TextUtils.isEmpty(productImage))
            imageLoader1.DisplayImage(productImage, holder.image);
        else {
            holder.image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            holder.image.setImageResource(ProductListingActivity.mCurrentDefaultImage);
        }
        holder.name.setText(p.name);
        holder.votes.setText(p.votes);
        holder.rating.setText(p.rating);
        holder.price.setText(p.price);
        holder.stores.setText(p.numStores);
        if (isSearch)
            holder.compare.setVisibility(View.GONE);
        else {
            if (compareList.contains(position)) {
                holder.compare.setChecked(true);
                holder.compare.setEnabled(true);
            } else if (compareList.size() == COMPARE_LIMIT) {
                holder.compare.setEnabled(false);
                holder.compare.setChecked(false);
            } else {
                holder.compare.setEnabled(true);
                holder.compare.setChecked(false);
            }
        }
        holder.compare.setTag(position);

    /*    holder.compare.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    compareList.add(position);
                else
                    compareList.remove(position);
                notifyDataSetChanged();
            }
        });*/
        return rootView;
    }

    /**
     * Called if a compare checkbox changes in the product list
     *
     * @param position  Position of product to be added or removed from compare list
     * @param isChecked Current state of checkbox
     */
    public void onCheckChanged(Integer position, boolean isChecked) {
        if (isChecked)
            compareList.add(position);
        else
            compareList.remove(position);
        notifyDataSetChanged();
    }

    public void clearCompareList() {
        compareList.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        changeDefaultImage(ProductListingActivity.mCurrentDefaultImage);
        super.notifyDataSetChanged();
    }

    private String getImageByDensity(Product p) {
        switch (DPI_CODE) {
            case DisplayMetrics.DENSITY_LOW:
                return p.mediumImageUrl;
            case DisplayMetrics.DENSITY_MEDIUM:
                return p.largeImageUrl;
            case DisplayMetrics.DENSITY_HIGH:
                return p.largeImageUrl;
            case DisplayMetrics.DENSITY_XHIGH:
                return p.xLargeImageUrl;
            default:
                return p.xLargeImageUrl;
        }
    }

    private static class ViewHolder {
        ImageView image;
        TextView name;
        TextView rating;
        TextView price;
        TextView stores;
        TextView votes;
        CheckBoxLeft compare;
    }
}
