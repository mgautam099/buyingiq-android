package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.buyingiq.app.R;
import com.buyingiq.app.entities.ProductReview;
import com.buyingiq.app.entities.User;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.widget.LevelTextView;
import com.buyingiq.app.widget.TextImageView;

import java.util.ArrayList;

public class ReviewsAdapter extends BaseAdapter {
    int count = 0;
    ArrayList<ProductReview> mProductReviews;
    Context mContext;
    ImageLoader1 imageLoader1;
    View.OnClickListener mReviewListener;

    public ReviewsAdapter(Activity context, ArrayList<ProductReview> productReviews, View.OnClickListener reviewListener) {
        mProductReviews = productReviews;
        mContext = context;
        mReviewListener = reviewListener;
        imageLoader1 = new ImageLoader1(context, 0);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public ProductReview getItem(int i) {
        return mProductReviews.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        count = mProductReviews.size();
        super.notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_review_compact, parent, false);
            holder = new ViewHolder();
            holder.userImage = (TextImageView) convertView.findViewById(R.id.user_display_image);
            holder.userName = ((TextView) convertView.findViewById(R.id.user_name));
            holder.userLevel = ((LevelTextView) convertView.findViewById(R.id.user_level));
            holder.reviewText = ((TextView) convertView.findViewById(R.id.review_text));
            holder.reviewFullLink = ((TextView) convertView.findViewById(R.id.review_full_link));
            holder.productRating = ((TextView) convertView.findViewById(R.id.product_rating));
            holder.reviewDate = ((TextView) convertView.findViewById(R.id.review_date));
            holder.reviewFullLink.setOnClickListener(mReviewListener);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        setReviewView(mProductReviews.get(position), holder);
        return convertView;
    }

    private void setReviewView(ProductReview r, ViewHolder holder) {
        User u = r.getUser();
        holder.userName.setText(u.getShortName());
        if (!u.name.isEmpty())
            holder.userImage.setText(u.name.substring(0, 1).toUpperCase());
        else
            holder.userImage.setText("—");
        TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.user_bg_colors);
        holder.userImage.setBackgroundColor(imgs.getColor(u.getRand(), Color.GRAY));
        imgs.recycle();
        if (!TextUtils.isEmpty(r.getUser().getImage3())) {
            imageLoader1.DisplayImage(r.getUser().getImage3(), holder.userImage);
        } else
            holder.userImage.setImageBitmap(null);
        holder.userLevel.setLevel(u.getLevel());
        holder.reviewText.setText(r.getReview());
        holder.reviewFullLink.setTag(r);
        holder.productRating.setText(r.getOverallRating());
        holder.reviewDate.setText(r.getPublishedOnStr());
    }

    static class ViewHolder {
        TextImageView userImage;
        TextView userName;
        LevelTextView userLevel;
        TextView reviewText;
        TextView reviewFullLink;
        TextView productRating;
        TextView reviewDate;
    }


}
