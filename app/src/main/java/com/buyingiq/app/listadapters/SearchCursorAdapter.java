package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.buyingiq.app.R;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.resources.ProductDatabaseHelper;

/**
 * Custom adapter to display search results to user instantly from the result cursor.
 */
public class SearchCursorAdapter extends CursorAdapter {

    Context mContext;
    ImageLoader1 imageLoader1;
    LayoutInflater mInflater;
    String[] columns = {
            BaseColumns._ID,
            ProductDatabaseHelper.KEY_NAME,
            ProductDatabaseHelper.KEY_CAT,
            ProductDatabaseHelper.KEY_PRICE,
            ProductDatabaseHelper.KEY_RATING,
            ProductDatabaseHelper.KEY_VOTES,
            ProductDatabaseHelper.KEY_IMAGE_L,
            ProductDatabaseHelper.KEY_URL
    };
    private ViewHolder holder;

    public SearchCursorAdapter(Activity context, Cursor c) {
        super(context, c, true);
        mContext = context;
        // getting category on the run from hashtable is costly and might
        // slow down the search experience, so a default Q image is used.
        imageLoader1 = new ImageLoader1(context, R.drawable.iq_logo_grey);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.item_search, viewGroup, false);
        holder = new ViewHolder();
        holder.name = (TextView) rootView.findViewById(R.id.product_name);
        holder.subHead = (TextView) rootView.findViewById(R.id.product_category);
        holder.image = (ImageView) rootView.findViewById(R.id.product_image);
        //    holder.rating = (TextView) rootView.findViewById(R.id.product_rating);
        //    holder.price =  (TextView) rootView.findViewById(R.id.product_price);
        //    holder.votes = (TextView) rootView.findViewById(R.id.product_votes);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        holder = (ViewHolder) view.getTag();
        int indexName = cursor.getColumnIndex(columns[1]);
        int indexCategory = cursor.getColumnIndex(columns[2]);
        //    int indexPrice = cursor.getColumnIndex(columns[3]);
        //    int indexRating = cursor.getColumnIndex(columns[4]);
        //    int indexVotes = cursor.getColumnIndex(columns[5]);
        int indexImage = cursor.getColumnIndex(columns[6]);
        String text = cursor.getString(indexName);
        //    String price = cursor.getString(indexPrice);
        //    String rating = cursor.getString(indexRating);
        //    String votes = cursor.getString(indexVotes);
        if (indexCategory != -1) {
            String subText = cursor.getString(indexCategory);
            holder.subHead.setText(Html.fromHtml("<font color='#949494'>in </font>" + subText));
        } else {
            holder.subHead.setText(Html.fromHtml("<font color='#949494'>recently searched</font>"));
        }
        String image = cursor.getString(indexImage);
        //Show default image when not available.
        if (TextUtils.indexOf(image, "http") == -1) {
            holder.image.setImageResource(R.drawable.iq_logo_grey);
        } else {
            imageLoader1.DisplayImage(image, holder.image);
        }
        holder.name.setText(text);
        //    holder.price.setText("Rs. "+price);
    /*    if (rating != null)
            holder.rating.setText(rating);
        else
            holder.rating.setText("-");
        if (votes != null && !votes.startsWith("0"))
            holder.votes.setText(votes);
        else
            holder.votes.setText("");*/
    }

    private static class ViewHolder {
        ImageView image;
        TextView name;
        //    TextView rating;
        //    TextView price;
        TextView subHead;
        //    TextView votes;
    }
}
