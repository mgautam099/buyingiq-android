package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.buyingiq.app.R;
import com.buyingiq.app.widget.RobotoTextView;

import java.util.ArrayList;

public class SpecificationAdapter extends ArrayAdapter {
    Typeface normal;
    Typeface light;
    private Context mContext;
    private ArrayList<String[]> mSpecs;
    private int count = 0;
    private int paddingHead;
    private int paddingNorm;
    private int paddingLeft;
    private int paddingRight;

    public SpecificationAdapter(Context context, int resource, ArrayList<String[]> specs) {
        super(context, resource);
        mContext = context;
        mSpecs = specs;
        count = specs.size();
        normal = Typeface.create("sans-serif", Typeface.NORMAL);
        light = Typeface.create("sans-serif-light", Typeface.NORMAL);
        paddingHead = (int) (3 * mContext.getResources().getDisplayMetrics().density);
        paddingNorm = (int) (5 * mContext.getResources().getDisplayMetrics().density);
        paddingLeft = context.getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
        paddingRight = context.getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_spec, parent, false);
            holder = new ViewHolder();
            holder.nameView = (RobotoTextView) convertView.findViewById(R.id.spec_name);
            holder.valueView = (TextView) convertView.findViewById(R.id.spec_value);
            holder.divider = convertView.findViewById(R.id.divider);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        String[] spec = mSpecs.get(position);
        if (spec.length > 1) {
            holder.nameView.setText(spec[0]);
            holder.nameView.setTypeFaceRoboto(RobotoTextView.ROBOTO_LIGHT);
            holder.valueView.setText(spec[1]);
            holder.valueView.setVisibility(View.VISIBLE);
            //        holder.nameView.setBackgroundResource(0);
            //        holder.nameView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.nameView.setPadding(paddingLeft, paddingNorm, paddingRight, paddingNorm);
            holder.valueView.setPadding(paddingLeft, paddingNorm, paddingRight, paddingNorm);
            holder.divider.setVisibility(View.VISIBLE);
            convertView.setBackgroundResource(0);
        } else {
            holder.nameView.setText(spec[0].toUpperCase());
            holder.nameView.setTypeFaceRoboto(RobotoTextView.ROBOTO_MEDIUM);
            holder.valueView.setText("");
            holder.valueView.setVisibility(View.GONE);
            holder.divider.setVisibility(View.GONE);
            holder.nameView.setPadding(paddingLeft, paddingHead, paddingRight, paddingHead);
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.header_bg_grey));
        }
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        count = mSpecs.size();
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return count;
    }

    static class ViewHolder {
        RobotoTextView nameView;
        View divider;
        TextView valueView;
    }
}
