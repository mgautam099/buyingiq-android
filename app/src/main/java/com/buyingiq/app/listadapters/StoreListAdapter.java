package com.buyingiq.app.listadapters;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.buyingiq.app.R;
import com.buyingiq.app.entities.StoreDeals;
import com.buyingiq.app.resources.ImageLoader1;
import com.buyingiq.app.widget.RobotoTextView;

import java.util.ArrayList;

/**
 * An {@link android.widget.ArrayAdapter} for storing and displaying list of Stores available for a Product.
 * <p/>
 * Here it is assumed that the list that came from server is in the order that is to
 * be displayed, therefore, no change is made to the order of the list.
 */
public class StoreListAdapter extends ArrayAdapter<StoreDeals> {
    private Context mContext;
    private ArrayList<StoreDeals> mDealList;
    private ImageLoader1 mImageLoader1;

    public StoreListAdapter(Activity context, ArrayList<StoreDeals> dealList) {
        super(context, R.layout.item_compare_price, dealList);
        mContext = context;
        mDealList = dealList;
        mImageLoader1 = new ImageLoader1(context, R.drawable.iq_logo_grey);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_compare_price, parent, false);
            holder = new ViewHolder();
            holder.storeImage = (ImageView) convertView.findViewById(R.id.store_image);
            holder.storePrice = (RobotoTextView) convertView.findViewById(R.id.store_price);
            holder.deals = (RobotoTextView) convertView.findViewById(R.id.store_deals_count);
            holder.sponsorView = convertView.findViewById(R.id.sponsored_text);
            holder.offerView = convertView.findViewById(R.id.offer_label);
            holder.plusView = convertView.findViewById(R.id.plus);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        StoreDeals storeDeals = mDealList.get(position);
        mImageLoader1.DisplayImage(storeDeals.getStore().getImageLarge(), holder.storeImage);
        if (storeDeals.isInStock()) {
            holder.storePrice.setText("Rs. " + storeDeals.getMinPrice());
            holder.storePrice.setTextColor(mContext.getResources().getColor(R.color.text_dark_grey));
            holder.storePrice.setTypeFaceRoboto(RobotoTextView.ROBOTO_BOLD);
            holder.storePrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        } else {
            holder.storePrice.setText("Out of Stock");
            holder.storePrice.setTypeFaceRoboto(RobotoTextView.ROBOTO);
            holder.storePrice.setTextColor(mContext.getResources().getColor(R.color.deals_stock_info_red));
            holder.storePrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        }
        if (storeDeals.isSponsored() && storeDeals.isInStock()) {
            convertView.setBackgroundResource(R.drawable.sponsored_bg_selector);
            holder.sponsorView.setVisibility(View.VISIBLE);
        } else {
            convertView.setBackgroundResource(R.drawable.product_bg_selector);
            holder.sponsorView.setVisibility(View.INVISIBLE);
        }
        if (storeDeals.getOfferCount() > 0 && storeDeals.isInStock()) {
            holder.plusView.setVisibility(View.VISIBLE);
            holder.offerView.setVisibility(View.VISIBLE);
        } else {
            holder.offerView.setVisibility(View.GONE);
            holder.plusView.setVisibility(View.GONE);
        }
        if (storeDeals.getDealList().size() > 0 && storeDeals.isInStock()) {
            holder.deals.setVisibility(View.VISIBLE);
            holder.deals.setText(storeDeals.getDealList().size() + " offer" + (storeDeals.getDealList().size() > 1 ? "s" : ""));
        } else {
            holder.deals.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    static class ViewHolder {
        RobotoTextView storePrice;
        ImageView storeImage;
        TextView deals;
        View sponsorView;
        View offerView;
        View plusView;
    }
}
