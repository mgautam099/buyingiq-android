package com.buyingiq.app.resources;

import com.buyingiq.app.R;

public class Categories {
    /*
        public static final String[] cols ={"Mobiles & Tablets","Computers","Cameras","Home Entertainment","Appliances"};

        public static final String[][] cats = {
                {"Mobiles", "Tablets", "Mobile Accessories"},
                {"Laptops", "Desktops", "Printers", "Monitors", "Laptop Accessories"},
                {"Digital SLRs", "Point & Shoot", "Mirrorless", "Camcorders"},
                {"TVs", "Video Players", "Home Theatres", "Speakers", "Mp3 Players"},
                {"Personal Appliances","Home Appliances","Kitchen Appliances"}
        };

        public static final String[][] catsTags = {
                {"mobiles", "tablets", "mobile-accessories"},
                {"laptops", "desktops", "printers", "monitors", "laptop-accessories"},
                {"cameras/slr", "cameras/point-n-shoot", "cameras/mirrorless", "cameras/camcorder"},
                {"tvs", "video-players", "home-theatres", "he-speakers", "mp3-players"},
                {"personal-appliances","home-appliances","kitchen-appliances"}
        };

        public static final String[][][] mouses = {
                {{}, {}, {"Batteries", "Cables", "Chargers", "Headphones", "Headsets", "Memory Cards", "Speakers", "Car Chargers", "Car Cradles", "Car Kits"}},
                {{}, {}, {}, {}, {"Adapters", "Laptop Batteries", "External Hard Disks", "Keyboards", "Mouse", "Pen Drives", "Headphones", "Headsets", "Speakers"}},
                {{}, {}, {}, {}},
                {{}, {}, {}, {}, {}},
                {{"Hair Curlers", "Hair Dryers", "Hair Straighteners", "Hair Stylers", "Shavers", "Epilators", "Trimmers"}, {"Refrigerators", "Washing Machines", "Irons", "Landlines", "Water Purifiers", "Stabilizers", "Vacuum Cleaners", "Air Conditioners"},{"Induction Cooktops", "Juicer Mixer Grinders", "Sandwich and Roti Makers", "Electric Cookers", "Food Processors", "Pop-up Toasters", "Hand Blenders", "Coffee Makers", "Electric Kettles"}}
        };


        public static final String[][] catsAll = {{"Mobiles","mobiles"},
                {"Cameras","cameras"},
                {"Laptops","laptops"},
                {"Tablets","tablets"},
                {"TVs","tvs"},
                {"Mp3 Players","mp3-players"},
                {"Batteries","mobile-batteries"},
                {"Cables","mobile-cables"},
                {"Chargers","mobile-chargers"},
                {"Mobile Headphones","mobile-headphones"},
                {"Mobile Headsets","mobile-headsets"},
                {"Memory Cards","mobile-memory-cards"},
                {"Mobile Speakers","mobile-speakers"},
                {"Car Chargers","mobile-car-chargers"},
                {"Car Cradles","mobile-car-cradles"},
                {"Car Kits","mobile-car-kits"},
                {"Desktops","desktops"},
                {"Home Theatres","home-theatres"},
                {"Video Players","video-players"},
                {"Printers","printers"},
                {"Monitors","monitors"},
                {"Home Entertainment Speakers","he-speakers"},
                {"Scanners","scanners"},
                {"Adapters","adapters"},
                {"Laptop Batteries","laptop-batteries"},
                {"External Hard Disks","external-hard-disks"},
                {"Keyboards","keyboards"},
                {"Mice","mice"},
                {"Pen Drives","pen-drives"},
                {"All Speakers","all-speakers"},
                {"All Headphones","all-headphones"},
                {"All Headsets","all-headsets"},
                {"Laptop Headphones","laptop-headphones"},
                {"Laptop Headsets","laptop-headsets"},
                {"Laptop Speakers","laptop-speakers"},
                {"Refrigerators","refrigerators"},
                {"Washing Machines","washing-machines"},
                {"Irons","irons"},
                {"Landlines","landlines"},
                {"Water Purifiers","water-purifiers"},
                {"Stabilizers","stabilizers"},
                {"Vacuum Cleaners","vacuum-cleaners"},
                {"Air Conditioners","air-conditioners"},
                {"Epilators","epilators"},
                {"Hair Curlers","hair-curlers"},
                {"Hair Dryers","hair-dryers"},
                {"Hair Straighteners","hair-straighteners"},
                {"Hair Stylers","hair-stylers"},
                {"Shavers","shavers"},
                {"Trimmers","trimmers"},
                {"Induction Cooktops","induction-cooktops"},
                {"Juicer Mixer Grinders","juicer-mixer-grinders"},
                {"Sandwich and Roti Makers","sandwich-roti-makers"},
                {"Electric Cookers","electric-cookers"},
                {"Food Processors","food-processors"},
                {"Pop-up Toasters","pop-up-toasters"},
                {"Hand Blenders","hand-blenders"},
                {"Coffee Makers","coffee-makers"},
                {"Electric Jugs / Travel Kettles","electric-jugs"},
                {"SLR Cameras","cameras/slr"},
                {"Point & Shoot Cameras","cameras/point-n-shoot"},
                {"Mirrorless Cameras","cameras/mirrorless"},
                {"Camcorders","cameras/camcorder"}};

        public static final String[][][] mousesTags = {
                {{}, {}, {"mobile-batteries", "mobile-cables", "mobile-chargers", "mobile-headphones", "mobile-headsets", "mobile-memory-cards", "mobile-speakers", "mobile-car-chargers", "mobile-car-cradles", "mobile-car-kits"}},
                {{}, {}, {}, {}, {"adapters", "laptop-batteries", "external-hard-disks", "keyboards", "mice", "pen-drives", "laptop-headphones", "laptop-headsets", "laptop-speakers"}},
                {{}, {}, {}, {}},
                {{}, {}, {}, {}, {}},
                {{"hair-curlers", "hair-dryers", "hair-straighteners", "hair-stylers", "shavers", "epilators", "trimmers"}, {"refrigerators", "washing-machines", "irons", "landlines", "water-purifiers", "stabilizers", "vacuum-cleaners", "air-conditioners"},{"induction-cooktops", "juicer-mixer-grinders", "sandwich-roti-makers", "electric-cookers", "food-processors", "pop-up-toasters", "hand-blenders", "coffee-makers", "electric-jugs"}}
        };

        public String[][] catsUnused = {{"Batteries","mobile-batteries"},
                {"Cables","mobile-cables"},
                {"Chargers","mobile-chargers"},
                {"Mobile Headsets","mobile-headsets"},
                {"Memory Cards","mobile-memory-cards"},
                {"Mobile Speakers","mobile-speakers"},
                {"Car Chargers","mobile-car-chargers"},
                {"Car Cradles","mobile-car-cradles"},
                {"Car Kits","mobile-car-kits"},
                {"Desktops","desktops"},
                {"Home Theatres","home-theatres"},
                {"Video Players","video-players"},
                {"Monitors","monitors"},
                {"Home Entertainment Speakers","he-speakers"},
                {"Scanners","scanners"},
                {"Adapters","adapters"},
                {"Laptop Batteries","laptop-batteries"},
                {"Keyboards","keyboards"},
                {"Mice","mice"},
                {"Pen Drives","pen-drives"},
                {"All Speakers","all-speakers"},
                {"All Headsets","all-headsets"},
                {"Laptop Headphones","laptop-headphones"},
                {"Laptop Headsets","laptop-headsets"},
                {"Laptop Speakers","laptop-speakers"},
                {"Refrigerators","refrigerators"},
                {"Washing Machines","washing-machines"},
                {"Irons","irons"},
                {"Landlines","landlines"},
                {"Water Purifiers","water-purifiers"},
                {"Stabilizers","stabilizers"},
                {"Vacuum Cleaners","vacuum-cleaners"},
                {"Air Conditioners","air-conditioners"},
                {"Epilators","epilators"},
                {"Hair Curlers","hair-curlers"},
                {"Hair Dryers","hair-dryers"},
                {"Hair Straighteners","hair-straighteners"},
                {"Hair Stylers","hair-stylers"},
                {"Shavers","shavers"},
                {"Induction Cooktops","induction-cooktops"},
                {"Juicer Mixer Grinders","juicer-mixer-grinders"},
                {"Sandwich and Roti Makers","sandwich-roti-makers"},
                {"Electric Cookers","electric-cookers"},
                {"Food Processors","food-processors"},
                {"Pop-up Toasters","pop-up-toasters"},
                {"Hand Blenders","hand-blenders"},
                {"Coffee Makers","coffee-makers"},
                {"Electric Jugs / Travel Kettles","electric-jugs"},
                {"SLR Cameras","cameras/slr"},
                {"Point & Shoot Cameras","cameras/point-n-shoot"},
                {"Mirrorless Cameras","cameras/mirrorless"},
                {"Camcorders","cameras/camcorder"}};
    */
    public static final int[] catIDs = {1,  //mobiles
            4,                                  //tablets
            10,                                 //all-headphones
            2,                                  //Cameras
            3,                                  //laptops
            6,                                  //mp3players
            5,                                  //tvs
            26,                                 //harddisk
            20,                                 //printers
            50,                                 //trimmers
            33};                                //all-headphones

    public static final String[] catNames = {"Mobiles",
            "Tablets",
            "Headphones",
            "Cameras",
            "Laptops",
            "Mp3 Players",
            "TVs",
            "External Hard Disks",
            "Printers",
            "Trimmers"};

    public static final String[] catTags = {"mobiles",
            "tablets",
            "all-headphones",
            "cameras",
            "laptops",
            "mp3-players",
            "tvs",
            "external-hard-disks",
            "printers",
            "trimmers"};

    public static final int[] catImgs = {R.drawable.ic_nav_mobile,
            R.drawable.ic_nav_tablet,
            R.drawable.ic_nav_headphone,
            R.drawable.ic_nav_camera,
            R.drawable.ic_nav_laptop,
            R.drawable.ic_nav_mp3player,
            R.drawable.ic_nav_tv,
            R.drawable.ic_nav_harddisk,
            R.drawable.ic_nav_printer,
            R.drawable.ic_nav_trimmer
    };

    public static final int[] catImgsWhite = {R.drawable.ic_nav_mobile_white,
            R.drawable.ic_nav_tablet_white,
            R.drawable.ic_nav_headphone_white,
            R.drawable.ic_nav_camera_white,
            R.drawable.ic_nav_laptop_white,
            R.drawable.ic_nav_mp3player_white,
            R.drawable.ic_nav_tv_white,
            R.drawable.ic_nav_harddisk_white,
            R.drawable.ic_nav_printer_white,
            R.drawable.ic_nav_trimmer_white
    };

    public static final String[] catColors = {"#ff6e40",
            "#5b5b5b",
            "#00c853",
            "#9575cd",
            "#e06055",
            "#f9a825",
            "#f06292",
            "#62a3f0",
            "#c86100",
            "#4dd0e1"
    };

    public static final Object[][] catsToUse = {
            {"Mobiles", "mobiles", R.drawable.ic_nav_mobile, R.drawable.ic_nav_mobile_white, "#ff6e40"},
            {"Tablets", "tablets", R.drawable.ic_nav_tablet, R.drawable.ic_nav_tablet_white, "#5b5b5b"},
            {"Headphones", "all-headphones", R.drawable.ic_nav_headphone, R.drawable.ic_nav_headphone_white, "#00c853"},
            {"Cameras", "cameras", R.drawable.ic_nav_camera, R.drawable.ic_nav_camera_white, "#9575cd"},
            {"Laptops", "laptops", R.drawable.ic_nav_laptop, R.drawable.ic_nav_laptop_white, "#e06055"},
            {"Mp3 Players", "mp3-players", R.drawable.ic_nav_mp3player, R.drawable.ic_nav_mp3player_white, "#f9a825"},
            {"TVs", "tvs", R.drawable.ic_nav_tv, R.drawable.ic_nav_tv_white, "#f06292"},
            {"External Hard Disks", "external-hard-disks", R.drawable.ic_nav_harddisk, R.drawable.ic_nav_harddisk_white, "#62a3f0"},
            {"Printers", "printers", R.drawable.ic_nav_printer, R.drawable.ic_nav_printer_white, "#c86100"},
            {"Trimmers", "trimmers", R.drawable.ic_nav_trimmer, R.drawable.ic_nav_trimmer_white, "#4dd0e1"}};

}
