package com.buyingiq.app.resources;

import android.content.Context;

import java.io.File;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir = new File(context.getExternalCacheDir(), "imgs");
        else
            cacheDir = new File(context.getCacheDir(), "imgs");
        if (!cacheDir.exists())
            cacheDir.mkdirs();
    }


    public File getFile(String url) {
        //Asumption that names will never be same.
        String filename = String.valueOf(url.hashCode());
        //String filename = URLEncoder.encode(url);
        return new File(cacheDir, filename);

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }

}