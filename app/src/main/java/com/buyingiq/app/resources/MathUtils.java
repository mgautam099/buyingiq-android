package com.buyingiq.app.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by maxx on 4/9/14.
 */
public class MathUtils {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
