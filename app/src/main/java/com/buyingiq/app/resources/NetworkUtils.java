package com.buyingiq.app.resources;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static com.buyingiq.app.resources.Utils.Print.d;
import static com.buyingiq.app.resources.Utils.Print.e;

public class NetworkUtils {

    public final static int GET = 1;
    public final static int POST = 2;
    public final static String TAG = "HTTPResources";
    static String response = null;
    HttpURLConnection con;
    Tracker mTracker;

    public NetworkUtils(Tracker t) {
        mTracker = t;
    }

    public static boolean isNetworkConnected(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            }
        } catch (Exception e) {
            e(TAG, "Exception: " + e.getMessage());
            Utils.logException(e);
            return false;
        }
        return status;
    }

    public static void networkCheck(final Activity activity) {
        if (!isNetworkConnected(activity)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
            dialog.setMessage("Please check network connection and try again");
            dialog.setTitle("Network Error");
            dialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    activity.finish();
                }
            });
            dialog.show();
        }
    }

    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    //Service Call With catching
    public String makeServiceCall(String url, int method, List<Pair<String,String>> params) {
        try {
            Uri.Builder uriB = new Uri.Builder();
            uriB.appendPath(url);
            if (method == GET) {
                if (params != null) {
                    for(Pair<String,String> pair: params){
                        uriB.appendQueryParameter(pair.first,pair.second);
                    }
                    //String paramString = URLEncodedUtils
                      //      .format(params, "utf-8");
                    //url += "?" + paramString;
                }
            }
            uriB.build();
            URL u = new URL(uriB.build().getEncodedPath());
            //Crashlytics.setString("last_url", url);
            con = (HttpURLConnection) u.openConnection();
            //    con.setConnectTimeout(30000);
            //    con.setReadTimeout(30000);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            d(TAG, url);
            if (Thread.interrupted())
                return null;
            try {
                if (method == POST) {
                    con.setRequestMethod("POST");
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    String postString = postData(params);
                    con.setUseCaches(false);
                    OutputStream out = new BufferedOutputStream(con.getOutputStream());
                    writeStream(out, postString);
                    out.flush();
                }
                InputStream in = new BufferedInputStream(con.getInputStream());
                response = readStream(in);
            } catch (IOException e1) {
                InputStream in = new BufferedInputStream(con.getErrorStream());
                response = readStream(in);
                e(TAG, "IOException: " + con.getResponseCode());
                switch (con.getResponseCode()) {
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        response = "ERROR 500: Server Error";
                        break;
                    case HttpURLConnection.HTTP_NOT_FOUND:
                        response = "ERROR 404: Link does not Exist";
                        break;
                    case HttpURLConnection.HTTP_BAD_REQUEST:
                        response = "ERROR::" + response;
                        break;
                }
                e(TAG, "IOException: " + response);
                //Crashlytics.log("IOException: " + url + " : " + response);
                Utils.logException(e1);
            } finally {
                con.disconnect();
            }

        } catch (UnsupportedEncodingException e) {
            e(TAG, "UnsupportedEncodingException: " + e.getMessage());
            Utils.logException(e);
//            Utils.sendException(mTracker, TAG, "UnsupportedEncodingException", url + ":" + e.getMessage());
            return "ERROR";
        } catch (IOException e) {
            e(TAG, "IOException: " + e.getMessage());
            Utils.logException(e);
//            Utils.sendException(mTracker, TAG, "IOException", url + ":" + e.getMessage());
            return "ERROR";
        } catch (JSONException e) {
            e(TAG, "JSONException: " + e.getMessage());
            Utils.logException(e);
//            Utils.sendException(mTracker, TAG, "JSONException", url + ":" + e.getMessage());
            return "ERROR";
        } catch (Exception e) {
            e(TAG, "Exception: " + e.getMessage());
            Utils.logException(e);
//            Utils.sendException(mTracker, TAG, "Exception", url + ":" + e.getMessage());
            return "ERROR";
        }
        return response;

    }

/*    public String makeServiceCall2(String url, int method,
                                   List<Pair<String,String>> params) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity;
            HttpResponse httpResponse = null;

            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                httpResponse = httpClient.execute(httpPost);

            } else if (method == GET) {
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                d("HTTPFunc", url);
                HttpGet httpGet = new HttpGet(url);
                httpResponse = httpClient.execute(httpGet);
            }
            if (httpResponse != null) {
                httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);
            }

        } catch (UnsupportedEncodingException e) {
            e(TAG, "UnsupportedEncodingException: " + e.getMessage());
        } catch (ClientProtocolException e) {
            e(TAG, "ClientProtocolException: " + e.getMessage());
        } catch (IOException e) {
            e(TAG, "IOException: " + e.getMessage());
        }
        return response;

    }*/

    public String readStream(InputStream in) throws IOException {
        String response = "";
        byte[] buffer = new byte[8192];
        int read = in.read(buffer);
        while (read != -1 && !Thread.interrupted()) {
            response += new String(buffer, 0, read);
            read = in.read(buffer);
        }
        return response;
    }

    public void writeStream(OutputStream out, String data) throws IOException {
        BufferedWriter printWriter = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
        printWriter.write(data);
        printWriter.flush();
    }

    public void stopOperation() {
        con.disconnect();
    }

    public String postData(List<Pair<String,String>> pairs) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Pair<String,String> p : pairs) {
            jsonObject.put(p.first, p.second);
        }
        return jsonObject.toString();
    }

    public long getImage(String url, Context context) {
        FileCache fileCache = new FileCache(context);
        File f = fileCache.getFile(url);

        //from SD cache
        if (f.exists())
            return f.length();

        //from web
        try {
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            return f.length();
        } catch (Throwable ex) {
            e(TAG, "Exception: " + ex.getMessage());
            Utils.logException(ex);
//            Utils.sendException(mTracker, TAG, "Exception", url + ":" + ex.getMessage());
            return -1;
        }
    }
}
