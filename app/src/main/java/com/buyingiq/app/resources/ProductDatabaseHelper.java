package com.buyingiq.app.resources;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * A SQLiteDatabaseHelper Class.
 */
public class ProductDatabaseHelper extends SQLiteOpenHelper {

    public final static String DATABASE_NAME = "products.db";
    public final static String RECENT_TABLE_NAME = "recently_visited";
    public final static String BANNER_TABLE_NAME = "banners";
    public final static String KEY_ID = "_id";
    public final static String KEY_NAME = "name";
    public final static String KEY_CAT = "category";
    public final static String KEY_PRICE = "price";
    public final static String KEY_RATING = "rating";
    public final static String KEY_VOTES = "votes";
    public final static String KEY_IMAGE = "image";
    public final static String KEY_IMAGE_S = "image_s";
    public final static String KEY_IMAGE_M = "image_m";
    public final static String KEY_IMAGE_L = "image_l";
    public final static String KEY_IMAGE_XL = "image_xl";
    public final static String KEY_URL = "url";
    public final static String KEY_LAST_VISITED = "last_visited";
    public final static String KEY_VISITED_FROM = "visited_from";
    private final static String RECENT_TABLE_CREATE = "CREATE TABLE " + RECENT_TABLE_NAME + " (" +
            KEY_ID + " TEXT primary key, " +
            KEY_NAME + " TEXT not null, " +
            KEY_CAT + " TEXT, " +
            KEY_PRICE + " TEXT, " +
            KEY_IMAGE_S + " TEXT, " +
            KEY_IMAGE_M + " TEXT, " +
            KEY_IMAGE_L + " TEXT, " +
            KEY_IMAGE_XL + " TEXT, " +
            KEY_URL + " TEXT, " +
            KEY_RATING + " TEXT," +
            KEY_VOTES + " TEXT, " +
            KEY_LAST_VISITED + " DATETIME, " +
            KEY_VISITED_FROM + " TEXT " +
            ");";
    public final static String KEY_RANK = "rank";
    public final static String KEY_LOCATION = "location";
    private final static String BANNER_TABLE_CREATE = "CREATE TABLE " + BANNER_TABLE_NAME + " (" +
            KEY_ID + " TEXT primary key, " +
            KEY_NAME + " TEXT not null, " +
            KEY_IMAGE + " TEXT not null, " +
            KEY_URL + " TEXT, " +
            KEY_RANK + " INTEGER, " +
            KEY_LOCATION + " TEXT " +
            " )";


    private final static int DATABASE_VERSION = 6;

    public ProductDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(RECENT_TABLE_CREATE);
        sqLiteDatabase.execSQL(BANNER_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion < 6) {
            sqLiteDatabase.execSQL("DROP TABLE " + RECENT_TABLE_NAME + ";");
            sqLiteDatabase.execSQL(RECENT_TABLE_CREATE);
        }
        //Banner table introduced in Database version 5
        if (oldVersion < 4) {
            sqLiteDatabase.execSQL(BANNER_TABLE_CREATE);
        }
    }
}
