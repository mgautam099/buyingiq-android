package com.buyingiq.app.resources;

import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.buyingiq.app.BuyingIQApp;
import com.buyingiq.app.SearchActivity;
import com.buyingiq.app.entities.Product;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.IOException;
import java.util.ArrayList;

import static com.buyingiq.app.resources.StringUtils.TAG_PRODUCTS;
import static com.buyingiq.app.resources.StringUtils.TAG_TOTAL;

public class SearchProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "com.buyingiq.app.resources.SearchProvider";
    public static final Uri SEARCH_URI = Uri.parse("content://" + AUTHORITY + "/search_suggest_query?limit=50");
    public static final int MODE = DATABASE_MODE_QUERIES;
    public static final int SEARCH_RESULT_LIMIT = 10;
    private static final String TAG = "SearchProvider";
    public JsonParser jp;
    String apiUrl = "search/";

    public SearchProvider() {
        setupSuggestions(AUTHORITY, MODE);
        apiUrl = StringUtils.BASE_URL + apiUrl;
    }

    /**
     * Parse JSON Using JackSon Parser
     *
     * @param jp A {@link com.fasterxml.jackson.core.JsonParser} object.
     * @throws java.io.IOException
     */
    public static long parseJackSon(JsonParser jp, ArrayList<Product> productList) throws IOException {
        long totalResults = 0;
        jp.nextToken();
        while (jp.nextToken() != JsonToken.END_OBJECT) {
            String currentName = jp.getCurrentName();
            jp.nextToken();
            switch (currentName) {
                case TAG_TOTAL:
                    totalResults = jp.getLongValue();
                    break;
                case TAG_PRODUCTS:
                    Product.populateProductList(jp, productList, true);
                    break;
                default:
                    jp.skipChildren();
                    break;
            }
        }
        return totalResults;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        String query = selectionArgs[0];
        if (query.equals(""))
            cursor = getRecentResults();
        else
            cursor = getSearchResults(query);
        return cursor;
    }

    private Cursor getRecentResults() {
        ProductDatabaseHelper dbHelper = new ProductDatabaseHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor results = db.query(ProductDatabaseHelper.RECENT_TABLE_NAME, null, ProductDatabaseHelper.KEY_VISITED_FROM + "=?",
                new String[]{SearchActivity.TAG}, null, null, ProductDatabaseHelper.KEY_LAST_VISITED + " DESC");
        if (results == null)
            return null;
        else if (!results.moveToNext())
            return null;
        return results;
    }

    public Cursor getSearchResults(String query) {
        String[] columns = {
                BaseColumns._ID,
                ProductDatabaseHelper.KEY_NAME,
                ProductDatabaseHelper.KEY_CAT,
                ProductDatabaseHelper.KEY_PRICE,
                ProductDatabaseHelper.KEY_RATING,
                ProductDatabaseHelper.KEY_VOTES,
                ProductDatabaseHelper.KEY_IMAGE_L,
                ProductDatabaseHelper.KEY_URL
        };
        ArrayList<Product> results = getResults(query);
        MatrixCursor cursor = new MatrixCursor(columns);
        int i = 0;
        for (Product p : results) {
            if (i == SEARCH_RESULT_LIMIT)
                break;
            Object[] tmp = {p.mID, p.name, p.category, p.price,
                    p.rating, p.votes, p.largeImageUrl, p.url};
            cursor.addRow(tmp);
            i++;
        }
        return cursor;
    }

    @NonNull
    @Override
    public String getType(@NonNull Uri uri) {
        return "text";
    }

    public ArrayList<Product> getResults(String query) {
        ArrayList<Product> results = new ArrayList<>();
        NetworkUtils networkUtils = new NetworkUtils(null);
        ArrayList<Pair<String,String>> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new Pair<String,String>("q", query));
        String json = networkUtils.makeServiceCall(apiUrl, NetworkUtils.GET, nameValuePairs);
        if (json != null && !json.contains("ERROR")) {
            try {
                JsonFactory factory = ((BuyingIQApp)getContext()).getFactory();
                JsonParser jp = factory.createParser(json);
                parseJackSon(jp, results);
            } catch (IOException e) {
                Utils.Print.e(TAG, "IOException:" + e.getMessage() + " At line: " + Utils.getLineNumber());
                Utils.logException(e);
            }
        }
        return results;
    }


    /*public Map<String, String> getResults(String query, ArrayList<String> products, ArrayList<String> images, ArrayList<String> category) {
        Map<String, String> results = new LinkedHashMap<>();
        NetworkUtils networkUtils = new NetworkUtils(null);
        ArrayList<Pair<String,String>> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new Pair<String,String>("q", query));
        String json = networkUtils.makeServiceCall(apiUrl, NetworkUtils.GET, nameValuePairs);
        if (json != null) {
            try {
                JSONObject jsonObject = new JSONObject(json);
                JSONArray productArray = jsonObject.getJSONArray(StringUtils.TAG_PRODUCTS);
                for (int i = 0; i < productArray.length(); i++) {
                    JSONObject productInfo = productArray.getJSONObject(i);
                    JSONObject productImages = productInfo.getJSONObject(StringUtils.TAG_IMAGES_O);
                    String ID = productInfo.getString(StringUtils.TAG_ID);
                    String productName = productInfo.getString(StringUtils.TAG_NAME);
                    results.put(ID, productName);
                    images.add(productImages.getString(StringUtils.TAG_SMALL_IMAGE));
                    products.add(productInfo.toString());
                    category.add(productInfo.getString(StringUtils.TAG_CATEGORY));
                }
            } catch (JSONException e) {
                Utils.Print.e("SearchProvider", "JSONException: " + e.getMessage());
                Utils.logException(e);
            }

        }
        return results;
    }

    public Map<String, String> getResults(ArrayList<String> images, ArrayList<String> category) {
        Map<String, String> results = new LinkedHashMap<>();
        ProductDatabaseHelper dbHelper = new ProductDatabaseHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(ProductDatabaseHelper.RECENT_TABLE_NAME, null, null, null, null, null, null);
        while (c.moveToNext()) {
            String ID = c.getString(0);
            String productName = c.getString(1);
            results.put(ID, productName);
            category.add(c.getString(2));
            images.add(c.getString(5));
        }
        db.close();
        return results;
    }*/

}
