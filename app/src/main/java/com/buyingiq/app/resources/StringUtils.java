package com.buyingiq.app.resources;

/**
 * This file stores all the Strings required for the Application to run.
 */
public class StringUtils {

    public static final String APP_STORE_URL = "https://play.google.com/store/apps/details?id=com.buyingiq.app&referrer=utm_source%3Dandroid%26utm_medium%3Dsharing";

    //    public static final String TAG_TIME = "took";
    public static final String TAG_TOTAL = "total";
    //    public static final String TAG_SELECTED_TAGS= "selected_tags";
//    public static final String TAG_SELECTED_FACETS = "selected_facets";
    public static final String TAG_FOLDERS = "folders";
    public static final String TAG_FACETS = "facets";
    public static final String TAG_TAG = "tag";
    public static final String TAG_LABEL = "label";
    public static final String TAG_IS_SELECTED = "is_selected";
    public static final String TAG_PRODUCT = "product";
    public static final String TAG_BRAND = "brand";
    public static final String TAG_CATEGORY = "category";
    public static final String TAG_PRODUCTS = "products";
    public static final String TAG_ID = "id";
    public static final String TAG_IMAGES_O = "images_o";
    public static final String TAG_AVG_RATING = "avg_rating";
    public static final String TAG_RATING_OVERALL = "avg_rating_overall";
    //TODO take all keys of store
    public static final String TAG_REVIEW_COUNT = "review_count";
    public static final String TAG_RATING_COUNT = "rating_count";
    public static final String TAG_STORE_COUNT = "store_count";
    public static final String TAG_MIN_PRICE_STR = "min_price_str";
    public static final String TAG_PREV = "prev";
    public static final String TAG_KEY_FEATURES = "key_features";
    public static final String TAG_SMALL_IMAGE = "s";
    public static final String TAG_MEDIUM_IMAGE = "m";
    public static final String TAG_LARGE_IMAGE = "l";
    public static final String TAG_XLARGE_IMAGE = "xl";

    public static final String TAG_STORE = "store";
    public static final String TAG_HAS_COD = "has_cod";
    public static final String TAG_HAS_EMI = "has_emi";
    public static final String TAG_RETURN_POLICY = "return_policy";
    public static final String TAG_IS_SPONSORED = "is_sponsored";
    public static final String TAG_IN_STOCK = "in_stock";
    public static final String TAG_SPONSORED = "sponsored";
    public static final String TAG_COUPONS = "coupons";
    public static final String TAG_DEALS = "deals";
    public static final String TAG_OFFER = "offer";
    public static final String TAG_STOCK_INFO = "stock_info";
    public static final String TAG_DELIVERY_TIME = "delivery_time";
    public static final String TAG_BASE_PRICE_STR = "base_price_str";
    public static final String TAG_CASHBACK_STR = "cashback_str";
    public static final String TAG_SHIP_COST_STR = "ship_cost_str";
    public static final String TAG_EFF_PRICE_STR = "eff_price_str";
    public static final String TAG_URL = "url";
    public static final String TAG_SELECTED_FACETS = "selected_facets";
    public static final String TAG_SELECTED_TAGS = "selected_tags";
    public static final String TAG_PRODUCT_LIST = "product_list";
    public static final String TAG_FACET_LIST = "facet_list";
    public static final String TAG_TITLE = "title";
    public static final String VALUES_CATEGORIES = "categories";
    public static final String VALUES_BRANDS = "brands";
    public static final String TAG_COMPARISON = "comparison";
    public static final String ARG_PRODUCT_ID = "productID";
    public static final String ARG_NAV_CAT = "navcat";
    public static final String ARG_STORE = "store";
    public static final String BASE_URL = "http://api.buyingiq.com/v1/";
    public static final String DECIMAL_FORMAT = "#.#";
    public static final String TAG_FEATURES = "features";
    public static final String TAG_TEXT = "text";
    public static final String TAG_IMAGE = "image";
    public static final String TAG_LINK = "link";
    public static final String TAG_RANK = "ranks";
    public static final String TAG_BANNERS = "banners";
    public static final String TAG_TRENDING = "trending";
    public static final String TAG_IMAGES = "images";
    public static final String TAG_BIQ_SCORE = "biq_score";

    public static final String TAG_REVIEWS = "reviews";
    public static final String TAG_AVERAGES = "averages";
    public static final String TAG_NEXT = "next";
    public static final String TAG_URI = "uri";
    public static final String TAG_REVIEW = "review";
    public static final String TAG_OVERALL_RATING = "overall_rating";
    public static final String TAG_RATINGS = "ratings";
    public static final String TAG_USER = "user";
    public static final String TAG_USERNAME = "username";
    public static final String TAG_NAME = "name";
    public static final String TAG_IMAGE_1 = "image_1";
    public static final String TAG_IMAGE_2 = "image_2";
    public static final String TAG_IMAGE_3 = "image_3";
    public static final String TAG_POINTS = "points";
    public static final String TAG_LEVEL = "level";
    public static final String TAG_WEBSITE = "website";
    public static final String TAG_LOCATION = "location";
    public static final String TAG_JOINED_ON = "joined_on";
    public static final String TAG_HEAD = "head";
    public static final String TAG_SCORE = "score";
    public static final String TAG_COUNT = "count";
    public static final String TAG_AVERAGE = "average";
    public static final String TAG_UPVOTES = "upvotes";
    public static final String TAG_VIEWS = "views";
    public static final String TAG_ADDED_ON = "added_on";
    public static final String TAG_PUBLISHED_ON = "published_on";
    public static final String TAG_IS_PUBLISHED = "is_published";
    public static final String TAG_IS_FEATURED = "is_featured";
    public static final String TAG_IS_TOP_REVIEW = "is_top_review";
    public static final String TAG_CURATED_REVIEW_COUNT = "curated_review_count";

    public static final String KEY_BACK_ANIMATION = "back_animation";
    public static final String TAG_REDIRECT_HASH = "redirect_hash";
    public static final String KEY_COMPARE_LIST = "compare_group_list";
    public static final String TAG_CATEGORY_ID = "category_id";
    public static final String TAG_NOTIFICATIONS = "notifications";
    public static final String TAG_MESSAGE = "message";
    public static final String KEY_NOTIFICATION_LIST = "notif_list";
}
