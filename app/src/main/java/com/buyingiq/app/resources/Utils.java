package com.buyingiq.app.resources;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.io.InputStream;
import java.io.OutputStream;

public class Utils {
    public final static boolean D = false;


    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
            Utils.Print.e("Utils", "Exception: " + ex.getMessage());
            Utils.logException(ex);
        }
    }

    /**
     * Static method to be used to hide a {@link android.view.View}
     * using some {@link android.view.animation.Animation}
     *
     * @param context   Current {@link android.content.Context} of the {@link android.app.Activity}
     * @param view      {@link android.view.View} to be hidden
     * @param animation {@link android.view.animation.Animation} resource to be used
     */
    public static void hideWithAnimation(Context context, View view, int animation) {
        if (view.getVisibility() == View.VISIBLE) {
            animateView(context, view, animation);
            view.setVisibility(View.GONE);
        }
    }

    /**
     * Static method to be used to show a View
     * using some {@link android.view.animation.Animation}
     *
     * @param context   Current {@link android.content.Context} of the {@link android.app.Activity}
     * @param view      {@link android.view.View} to be shown
     * @param animation {@link android.view.animation.Animation} resource to be used
     */
    public static void showWithAnimation(Context context, View view, int animation) {
        if (view.getVisibility() == View.GONE || view.getVisibility() == View.INVISIBLE) {
            animateView(context, view, animation);
            view.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Static method to animate a {@link android.view.View}
     *
     * @param context   Current {@link android.content.Context} of the {@link android.app.Activity}
     * @param view      {@link android.view.View} to be animated
     * @param animation {@link android.view.animation.Animation} resource to be used
     */
    public static void animateView(Context context, View view, int animation) {
        Animation anim = AnimationUtils.loadAnimation(context, animation);
        view.startAnimation(anim);
    }

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public static void changeUpIcon(ActionBar actionBar, Activity activity, int resid) {
        //To change the up icon of the device.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            actionBar.setHomeAsUpIndicator(resid);
        else {
            int upId = Resources.getSystem().getIdentifier("up", "id", "android");
            if (upId > 0) {
                ImageView up = (ImageView) activity.findViewById(upId);
                up.setImageResource(resid);
                up.setPadding(0, 0, 20, 0);
            }
        }
    }

    public static void sendException(Tracker t, Exception e, boolean fatal) {
        String description = e.getCause() + ":" + e.getStackTrace()[2].getMethodName() + ":" + e.getStackTrace()[2].getLineNumber();
        t.send(new HitBuilders.ExceptionBuilder()
                .setDescription(description)
                .setFatal(fatal)
                .build());
    }

    public static void sendException(Tracker t, String location, String exception, String message) {
        if (t == null)
            return;
        String description = location + ":" + exception + ":" + message;
        t.send(new HitBuilders.ExceptionBuilder()
                .setDescription(description)
                .setFatal(false).build());
    }

    public static void logException(Throwable e) {
        //if (!D)
            //Crashlytics.logException(e);
    }

    public static class Print {
        /**
         * Implementation of Debug log function with a debug flag *
         */
        public static void d(String TAG, String message) {
            if (D)
                Log.d(TAG, message);
        }

        /**
         * Implementation of Debug log function with a debug flag *
         */
        public static void d(String message) {
            d("Default", message);
        }

        /**
         * Implementation of Error log function with a debug flag *
         */
        public static void e(String TAG, String message) {
            if (D)
                Log.e(TAG, message);
        }
    }
}