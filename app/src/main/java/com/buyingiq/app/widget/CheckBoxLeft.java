package com.buyingiq.app.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * This subclass of {@link android.widget.TextView} was only created to
 * capture multiple touch events in a listview and do not reflect parent
 * state.
 */
public class CheckBoxLeft extends TextView {
    public CheckBoxLeft(Context context) {
        super(context);
    }

    public CheckBoxLeft(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckBoxLeft(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setPressed(boolean pressed) {
        if (pressed && ((View) getParent()).isPressed())
            return;
        super.setPressed(pressed);
    }

    //Empty because we want to control the selection behaviour of the TextView

    /**
     * @param selected
     * @deprecated This method does not work, use {@link #setChecked(boolean)} instead.
     * This done in order to control the selection behaviour of the TextView.
     */
    @Override
    public void setSelected(boolean selected) {

    }

    /**
     * A method created to control the Selection behaviour of the {@link android.widget.TextView}.
     * By default TextView responds to touch event of parent but we want it to get selected only
     * when the app wants it to.
     *
     * @param checked to select or not
     */
    public void setChecked(boolean checked) {
        super.setSelected(checked);
    }

    @Override
    public void getHitRect(Rect outRect) {
        outRect.set(getLeft() - 5, getTop() - 5, getRight() + 15, getBottom() + 10);
        super.getHitRect(outRect);
    }
}
