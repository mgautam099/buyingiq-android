package com.buyingiq.app.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.buyingiq.app.R;

/**
 * Created by Mayank on 10-07-2014.
 */
public class FeatureTextView extends RobotoTextView {
    public FeatureTextView(Context context) {
        super(context);
    }

    public FeatureTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FeatureTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }

    public void setText(CharSequence text, int comp) {
        Drawable d = null;
        switch (comp) {
            case 1:
                d = getResources().getDrawable(R.drawable.ic_plus_sign_spec);
                break;
            case 0:
                d = getResources().getDrawable(R.drawable.ic_eq_sign_spec);
                break;
            case -1:
                d = getResources().getDrawable(R.drawable.ic_minus_sign_spec);
                break;
            default:
                d = getResources().getDrawable(R.drawable.bullet);
        }
        if (text.length() > 1)
            setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);
        setText(text);
    }
}
