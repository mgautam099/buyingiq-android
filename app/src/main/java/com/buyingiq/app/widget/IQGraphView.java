package com.buyingiq.app.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.buyingiq.app.R;

public class IQGraphView extends ImageView {

    float caretPos;
    int iqScore = 60;
    int iqScoreColor;
    int count;
    float caretWidth;
    float textSize;
    private Paint paint;
    private Paint textPaint;
    private Typeface mFont;
    private float space;
    private float textY;

    public IQGraphView(Context context) {
        super(context);
        setAttributes(context, null);
    }

    public IQGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttributes(context, attrs);
    }

    public IQGraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setAttributes(context, attrs);
    }


    public void setAttributes(Context context, AttributeSet attrs) {
        setLayerType(LAYER_TYPE_HARDWARE, null);
        paint = new Paint();
        paint.setColor(context.getResources().getColor(R.color.second_grey));
        TypedArray ta = context.getTheme().obtainStyledAttributes(attrs, R.styleable.IQGraphView, 0, 0);
        try {
            caretWidth = ta.getDimension(R.styleable.IQGraphView_caretWidth, context.getResources().getDisplayMetrics().density);
            textSize = ta.getDimensionPixelSize(R.styleable.IQGraphView_textSize, 12);
            mFont = Typeface.create("sans-serif", Typeface.NORMAL);
            space = 5 * getResources().getDisplayMetrics().density;
            textY = 16.0f * getResources().getDisplayMetrics().density;
            //    paint.setAntiAlias(true);
            textPaint = new Paint();
            textPaint.setTextSize(textSize);
            textPaint.setTypeface(mFont);
            textPaint.setTextAlign(Paint.Align.CENTER);
        } finally {
            ta.recycle();
        }
        iqScoreColor = context.getResources().getColor(R.color.iq_score4);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        //Utils.Print.d("paint: " + textSize);
        super.onDraw(canvas);
        canvas.drawRect((int) caretPos, textSize + space, (int) caretPos + caretWidth, getHeight(), paint);
        //    paint.setLinearText(true);
        canvas.drawText("" + iqScore, caretPos, textY, textPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (count == 0) {
            caretPos = ((getMeasuredWidth() - getPaddingRight() - getPaddingLeft()) * (iqScore / 100.0f)) + (getPaddingLeft() - 1.5f);
            caretPos = Math.round(caretPos);
            if (iqScore == 50) {
                caretPos += (1.0f * getResources().getDisplayMetrics().density);
            }
            if (iqScore == 100)
                caretPos -= (1.0f * getResources().getDisplayMetrics().density);
            count++;
        }
    }

    public void setIqScore(int score) {
        this.iqScore = score;
        if (score >= 0 && score < 50) {
            iqScoreColor = R.color.iq_score1;
        } else if (score >= 50 && score < 80) {
            iqScoreColor = R.color.iq_score2;
        } else if (score >= 80 && score < 90) {
            iqScoreColor = R.color.iq_score3;
        } else {
            iqScoreColor = R.color.iq_score4;
        }
        iqScoreColor = getResources().getColor(iqScoreColor);
        textPaint.setColor(iqScoreColor);
    }
}
