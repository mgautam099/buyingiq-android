package com.buyingiq.app.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.buyingiq.app.R;

/**
 * Created by Mayank on 07-07-2014.
 */
public class LevelTextView extends RobotoTextView {
    String[] levels = {"NEWBIE", "TECH ENTHUSIAST", "TECH JUNKIE", "TECH SAVVY", "TECH NINJA", "TECH GURU"};

    public LevelTextView(Context context) {
        super(context);
    }

    public LevelTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LevelTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }

    public void setLevel(int level) {
        int drawable = 0;
        switch (level) {
            case 1:
                drawable = R.drawable.level_bg_0;
                break;
            case 2:
                drawable = R.drawable.level_bg_1;
                break;
            case 3:
                drawable = R.drawable.level_bg_2;
                break;
            case 4:
                drawable = R.drawable.level_bg_3;
                break;
            case 5:
                drawable = R.drawable.level_bg_4;
                break;
            case 6:
                drawable = R.drawable.level_bg_5;
                break;

        }
        setBackgroundResource(drawable);
        setText(levels[level - 1]);
    }

}
