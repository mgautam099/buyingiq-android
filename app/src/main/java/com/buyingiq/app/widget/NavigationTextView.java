package com.buyingiq.app.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.buyingiq.app.R;

/**
 * Created by maxx on 14/7/14.
 */
public class NavigationTextView extends TextView {

    private boolean isExpandable = false;
    private boolean isChild = false;

    public NavigationTextView(Context context) {
        super(context);
    }

    public NavigationTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NavigationTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean isExpandable) {
        this.isExpandable = isExpandable;
        setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(isExpandable ? R.drawable.ic_plus_sign_spec : R.drawable.ic_minus_sign_spec), null);
    }

    public boolean isChild() {
        return isChild;
    }

    public void setChild(boolean isChild) {
        this.isChild = isChild;
        setBackgroundColor(getResources().getColor(isChild ? R.color.filter_background_grey : R.color.white));
    }
}
