package com.buyingiq.app.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.buyingiq.app.R;

public class RatingTextView extends RobotoTextView {

    Context mContext;

    public RatingTextView(Context context) {
        super(context);
        mContext = context;
    }

    public RatingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public RatingTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        int pro = (text.charAt(0) - 48) * 10;
        if (text.length() > 1) {
            if (text.charAt(1) == '.')
                pro += (text.charAt(2) - 48);
            else
                pro = 100;
        }
        pro = (pro) / 10;
        switch (pro) {
            case 10:
                setBackgroundResource(R.drawable.rating_textview_bg_9);
                break;
            case 9:
                setBackgroundResource(R.drawable.rating_textview_bg_9);
                break;
            case 8:
                setBackgroundResource(R.drawable.rating_textview_bg_8);
                break;
            case 7:
                setBackgroundResource(R.drawable.rating_textview_bg_7);
                break;
            case 6:
                setBackgroundResource(R.drawable.rating_textview_bg_6);
                break;
            case 5:
                setBackgroundResource(R.drawable.rating_textview_bg_5);
                break;
            case 4:
                setBackgroundResource(R.drawable.rating_textview_bg_4);
                break;
            case 3:
                setBackgroundResource(R.drawable.rating_textview_bg_3);
                break;
            case 2:
                setBackgroundResource(R.drawable.rating_textview_bg_2);
                break;
            case 1:
                setBackgroundResource(R.drawable.rating_textview_bg_1);
                break;
            default:
                text = "\u2014";
                setBackgroundResource(R.drawable.level_bg_0);
                break;
        }
        super.setText(text, type);
    }

}
