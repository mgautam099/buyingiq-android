package com.buyingiq.app.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.buyingiq.app.R;

/**
 * Rating View used in User Reviews for product, when shown in detail.
 */
public class RatingTextView2 extends RobotoTextView {
    public RatingTextView2(Context context) {
        super(context);
    }

    public RatingTextView2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RatingTextView2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        String[] labels = {"AWFUL", "POOR", "SO-SO", "GOOD", "GREAT"};
        int pro = text.charAt(0) - 49;
        super.setText(labels[pro], type);
        switch (pro) {
            case 4:
                setBackgroundResource(R.drawable.ic_rating_9);
                break;
            case 3:
                setBackgroundResource(R.drawable.ic_rating_7);
                break;
            case 2:
                setBackgroundResource(R.drawable.ic_rating_5);
                break;
            case 1:
                setBackgroundResource(R.drawable.ic_rating_3);
                break;
            case 0:
                setBackgroundResource(R.drawable.ic_rating_1);
                break;
        }
    }

}
