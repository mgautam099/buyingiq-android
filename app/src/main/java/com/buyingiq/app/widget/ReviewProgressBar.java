package com.buyingiq.app.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.buyingiq.app.R;

/**
 * Created by Mayank on 07-07-2014.
 */
public class ReviewProgressBar extends ProgressBar {

    public ReviewProgressBar(Context context) {
        super(context);
    }

    public ReviewProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReviewProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public synchronized void setProgress(int progress) {
        int pro = (progress - 1) / 10;
        switch (pro) {
            case 9:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_9));
                break;
            case 8:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_8));
                break;
            case 7:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_7));
                break;
            case 6:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_6));
                break;
            case 5:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_5));
                break;
            case 4:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_4));
                break;
            case 3:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_3));
                break;
            case 2:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_2));
                break;
            case 1:
                setProgressDrawable(getResources().getDrawable(R.drawable.progress_bar_1));
                break;
        }
        super.setProgress(progress);
    }
}
