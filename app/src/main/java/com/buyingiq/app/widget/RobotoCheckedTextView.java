package com.buyingiq.app.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

import com.buyingiq.app.R;

public class RobotoCheckedTextView extends CheckedTextView {

    private final static int ROBOTO = 0;
    private final static int ROBOTO_MEDIUM = 1;
    private final static int ROBOTO_LIGHT = 2;
    private final static int ROBOTO_BOLD = 3;

    private Context mContext;

    public RobotoCheckedTextView(Context context) {
        super(context);
    }

    public RobotoCheckedTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        parseAttributes(context, attributeSet);

    }

    public RobotoCheckedTextView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        parseAttributes(context, attributeSet);
    }

    private void parseAttributes(Context context, AttributeSet attributeSet) {
        mContext = context;
        TypedArray ta = context.obtainStyledAttributes(attributeSet, R.styleable.RobotoTextView);
        int typeface = ta.getInt(R.styleable.RobotoTextView_typeface, 0);
        switch (typeface) {
            case ROBOTO:
            default:
                Typeface roboto = Typeface.create("sans-serif", Typeface.NORMAL);
                setTypeface(roboto);
                break;
            case ROBOTO_LIGHT:
                Typeface robotoLight = Typeface.create("sans-serif-light", Typeface.NORMAL);
                setTypeface(robotoLight);
                break;
            case ROBOTO_MEDIUM:
                Typeface robotoMedium = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Medium.ttf");
                setTypeface(robotoMedium);
                break;
            case ROBOTO_BOLD:
                Typeface robotoBold = Typeface.create("sans-serif", Typeface.BOLD);
                setTypeface(robotoBold);
                break;
        }
        ta.recycle();
    }

    public void setTypeFaceRoboto(int typeface) {
        switch (typeface) {
            case ROBOTO:
            default:
                Typeface roboto = Typeface.create("sans-serif", Typeface.NORMAL);
                setTypeface(roboto);
                break;
            case ROBOTO_LIGHT:
                Typeface robotoLight = Typeface.create("sans-serif-light", Typeface.NORMAL);
                setTypeface(robotoLight);
                break;
            case ROBOTO_MEDIUM:
                Typeface robotoMedium = Typeface.createFromAsset(mContext.getResources().getAssets(), "fonts/Roboto-Medium.ttf");
                setTypeface(robotoMedium);
                break;
            case ROBOTO_BOLD:
                Typeface robotoBold = Typeface.create("sans-serif", Typeface.BOLD);
                setTypeface(robotoBold);
                break;
        }
    }

}
