package com.buyingiq.app.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.buyingiq.app.R;

/**
 * Image view for displaying user images.
 */
public class TextImageView extends ImageView {

    private final Paint paint = new Paint();
    private float mCornerRadius = 0;
    private float mTextSize;
    private String mText;
    private int mTextColor;
    private int mBackgroundColor;
    private RoundRectShape mRoundRectShape;
    private Bitmap mBitmap;
    private BitmapShader mBitmapShader;
    private Typeface mFont;

    public TextImageView(Context context) {
        super(context);
        setLayerType(LAYER_TYPE_HARDWARE, null);
    }

    public TextImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        setAttributes(context, attrs);
    }

    public TextImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        setAttributes(context, attrs);
    }

    public void setAttributes(Context context, AttributeSet attrs) {
        final TypedArray typedArrayAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.TextImageView);
        mCornerRadius = typedArrayAttributes.getDimensionPixelSize(R.styleable.TextImageView_cornerRadius, 0);
        mText = typedArrayAttributes.getString(R.styleable.TextImageView_text);
        mTextSize = typedArrayAttributes.getDimensionPixelSize(R.styleable.TextImageView_textSize, 32);
        mTextColor = typedArrayAttributes.getColor(R.styleable.TextImageView_textColor, Color.WHITE);
        mBackgroundColor = typedArrayAttributes.getColor(R.styleable.TextImageView_backgroundColor, Color.GRAY);
        mRoundRectShape = new RoundRectShape(new float[]{
                mCornerRadius, mCornerRadius,
                mCornerRadius, mCornerRadius,
                mCornerRadius, mCornerRadius,
                mCornerRadius, mCornerRadius
        }, null, null);

        if (((BitmapDrawable) getDrawable()) != null) {
            mBitmap = ((BitmapDrawable) getDrawable()).getBitmap();
            mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        }
        mFont = Typeface.create("sans-serif", Typeface.NORMAL);
        typedArrayAttributes.recycle();
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (bm != null) {
            int w = getWidth();
            if (w == 0) {
                w = getResources().getDimensionPixelSize(R.dimen.review_dp_width);
            }
            mBitmap = Bitmap.createScaledBitmap(bm, w, w, true);
            mBitmapShader = new BitmapShader(mBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        } else {
            mBitmap = null;
            mBitmapShader = null;
        }
    }

    @Override
    public void setImageResource(int resId) {
        if (resId == 0) {
            mBitmap = null;
            mBitmapShader = null;
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint.setAntiAlias(true);
        if (mBitmapShader != null) {
            paint.setShader(mBitmapShader);
            paint.setColor(Color.WHITE);
            mRoundRectShape.resize(getWidth(), getHeight());
            mRoundRectShape.draw(canvas, paint);
        } else {
            drawText(canvas, paint, mText);
        }
    }

    public void drawText(Canvas canvas, Paint paint, String text) {
        paint.setColor(mBackgroundColor);
        paint.setShader(null);
        paint.setTextSize(mTextSize);
        paint.setTypeface(mFont);
        paint.setTextAlign(Paint.Align.CENTER);
        mRoundRectShape.resize(getWidth(), getHeight());
        mRoundRectShape.draw(canvas, paint);
        paint.setColor(mTextColor);
        float x = canvas.getWidth() / 2;
        float y = (getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2);
        canvas.drawText(mText, x, y, paint);
    }

    public void setText(String text) {
        this.mText = text;
    }

    public void setTextSize(float textSize) {
        mTextSize = textSize;
    }

    @Override
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
    }
}
