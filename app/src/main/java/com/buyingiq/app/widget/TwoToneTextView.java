package com.buyingiq.app.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.widget.TextView;

import com.buyingiq.app.R;

/**
 * This textView sets the Background of color of the left drawable provided.
 * It also provides curved corners if required.
 */
public class TwoToneTextView extends TextView {

    //    Time now = new Time();
    private Paint paint;
    private int mFirstBackground;
    private int mSecondBackground;
    //    private int mOuterPaddingTop;
//    private int mOuterPaddingBottom;
//    private int mOuterPaddingLeft;
//    private int mOuterPaddingRight;
//    private int mInnerPaddingTop;
//    private int mInnerPaddingBottom;
//    private int mInnerPaddingLeft;
//    private int mInnerPaddingRight;
    private int mDivider;
    private float mCornerRadius;
    private RoundRectShape mRoundRectShape;
    private ShapeDrawable mFirstDrawable;
    private ShapeDrawable mSecondDrawable;

    public TwoToneTextView(Context context) {
        super(context);
        setLayerType(LAYER_TYPE_HARDWARE, null);
    }

    public TwoToneTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        setAttributes(context, attrs);
    }

    public TwoToneTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        setAttributes(context, attrs);
    }

    public void setAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TwoToneTextView, 0, 0);
        try {
            mFirstBackground = a.getColor(R.styleable.TwoToneTextView_firstBackground, Color.WHITE);
            mSecondBackground = a.getColor(R.styleable.TwoToneTextView_secondBackground, Color.WHITE);
            mCornerRadius = a.getDimension(R.styleable.TwoToneTextView_cornerRadius, 0);
            //    mOuterPaddingLeft = a.getDimensionPixelSize(R.styleable.TwoToneTextView_outerPaddingLeft, 0);
            //    mOuterPaddingRight = a.getDimensionPixelSize(R.styleable.TwoToneTextView_outerPaddingRight, 0);
            //    mOuterPaddingTop = a.getDimensionPixelSize(R.styleable.TwoToneTextView_outerPaddingTop, 0);
            //    mOuterPaddingBottom = a.getDimensionPixelSize(R.styleable.TwoToneTextView_outerPaddingBottom,0);
            int dright = 0;
            if (getCompoundDrawables()[0] != null)
                dright = getCompoundDrawables()[0].getIntrinsicWidth() + (getPaddingLeft() * 2);
            mDivider = a.getDimensionPixelSize(R.styleable.TwoToneTextView_divider1, dright);
        } finally {
            a.recycle();
        }
        mFirstDrawable = new ShapeDrawable(new RoundRectShape(new float[]{
                mCornerRadius, mCornerRadius,
                0, 0,
                0, 0,
                mCornerRadius, mCornerRadius
        }, null, null));
        mSecondDrawable = new ShapeDrawable(new RoundRectShape(new float[]{
                0, 0,
                mCornerRadius, mCornerRadius,
                mCornerRadius, mCornerRadius,
                0, 0
        }, null, null));
        mFirstDrawable.getPaint().setColor(mFirstBackground);
        mSecondDrawable.getPaint().setColor(mSecondBackground);
        //    mInnerPaddingLeft = getPaddingLeft();
        //    mInnerPaddingRight = getPaddingRight();
        //    mInnerPaddingTop = getPaddingTop();
        //    mInnerPaddingBottom = getPaddingBottom();
        paint = new Paint();
        paint.setColor(mFirstBackground);
        setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
    }

    /*@Override
    public void setPadding(int left, int top, int right, int bottom) {
        updatePadding();
    }

    private void updatePadding() {
        super.setPadding(mInnerPaddingLeft+mOuterPaddingLeft, mInnerPaddingTop+mOuterPaddingTop, mInnerPaddingRight+mOuterPaddingRight, mInnerPaddingBottom+mOuterPaddingBottom);
    }

    public void setInnerPadding(int left,int top,int right,int bottom) {
        mInnerPaddingLeft = left;
        mInnerPaddingTop = top;
        mInnerPaddingRight = right;
        mInnerPaddingBottom = bottom;
        requestLayout();
    }*/

    @Override
    protected void onDraw(Canvas canvas) {
        //    int bright = getWidth() - mOuterPaddingRight;
        //    int bottom = getHeight()- mOuterPaddingBottom;
        //THis is if we need a side padding of white color.
        //    paint.setColor(Color.WHITE);
        //    canvas.drawRect(0, 0, mOuterPaddingLeft+(int)(mCornerRadius*getResources().getDisplayMetrics().density), getHeight(), paint);
        //    canvas.drawRect(bright-(int)(mCornerRadius*getResources().getDisplayMetrics().density), 0, getWidth(), getHeight(), paint);
        //    canvas.drawRect(mOuterPaddingLeft, 0, bright, mOuterPaddingTop, paint);
        //    canvas.drawRect(mOuterPaddingLeft, bottom, bright, getHeight(), paint);
        mFirstDrawable.setBounds(0, 0, mDivider, getHeight());
        mFirstDrawable.draw(canvas);
        mSecondDrawable.setBounds(mDivider, 0, getWidth(), getHeight());
        mSecondDrawable.draw(canvas);
        super.onDraw(canvas);
    }

    public int getFirstBackground() {
        return mFirstBackground;
    }

    public void setFirstBackground(int mFirstBackground) {
        this.mFirstBackground = mFirstBackground;
        mFirstDrawable.getPaint().setColor(mFirstBackground);
        invalidate();
    }

    public int getSecondBackground() {
        return mSecondBackground;
    }

    public void setSecondBackground(int mSecondBackground) {
        this.mSecondBackground = mSecondBackground;
        requestLayout();
    }
/*
    public int getOuterPaddingTop() {
        return mOuterPaddingTop;
    }

    public void setOuterPaddingTop(int mOuterPaddingTop) {
        this.mOuterPaddingTop = (int)(mOuterPaddingTop*getResources().getDisplayMetrics().density);
        updatePadding();
        requestLayout();
    }

    public int getOuterPaddingBottom() {
        return mOuterPaddingBottom;
    }

    public void setOuterPaddingBottom(int mOuterPaddingBottom) {
        this.mOuterPaddingBottom = (int) (mOuterPaddingBottom*getResources().getDisplayMetrics().density);
        updatePadding();
        requestLayout();
    }

    public int getOuterPaddingLeft() {
        return mOuterPaddingLeft;
    }

    public void setOuterPaddingLeft(int mOuterPaddingLeft) {
        this.mOuterPaddingLeft = (int) (mOuterPaddingLeft*getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public int getOuterPaddingRight() {
        return mOuterPaddingRight;
    }

    public void setOuterPaddingRight(int mOuterPaddingRight) {
        this.mOuterPaddingRight = (int) (mOuterPaddingRight*getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public int getInnerPaddingTop() {
        return mInnerPaddingTop;
    }

    public void setInnerPaddingTop(int mInnerPaddingTop) {
        this.mInnerPaddingTop = (int) (mInnerPaddingTop*getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public int getInnerPaddingBottom() {
        return mInnerPaddingBottom;
    }

    public void setInnerPaddingBottom(int mInnerPaddingBottom) {
        this.mInnerPaddingBottom = (int) (mInnerPaddingBottom*getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public int getInnerPaddingLeft() {
        return mInnerPaddingLeft;
    }

    public void setInnerPaddingLeft(int mInnerPaddingLeft) {
        this.mInnerPaddingLeft = (int) (mInnerPaddingLeft*getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public int getInnerPaddingRight() {
        return mInnerPaddingRight;
    }

    public void setInnerPaddingRight(int mInnerPaddingRight) {
        this.mInnerPaddingRight = (int) (mInnerPaddingRight*getResources().getDisplayMetrics().density);
        requestLayout();
    }*/

    public int getDivider() {
        return mDivider;
    }

    public void setDivider(int mDivider) {
        this.mDivider = (int) (mDivider * getResources().getDisplayMetrics().density);
        requestLayout();
    }

    public float getCornerRadius() {
        return mCornerRadius;
    }

    public void setCornerRadius(float mCornerRadius) {
        this.mCornerRadius = mCornerRadius;
        requestLayout();
    }
}
